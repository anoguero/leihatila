/**
 * 
 */

$(document).ready(function() {
	$('button.update_pass_button').click(function () {
		var form = this.form;
		if (form.prev_pass.value == '') {
			alert(missing_prev_password_error_message);
		} else if (form.new_pass1.value == '' || form.new_pass2.value == '') {
			alert(missing_new_password_error_message);
		} else if (form.new_pass1.value != form.new_pass2.value) {
			alert(different_passwords_error_message);
		} else if (confirm(confirm_password_change_message)){
			form.submit();
		}
	});
	
	$('button.update_account_button').click(function () {
		var form = $('form#update_account_form').get(0);
		if (form.name.value == '' || form.surname1.value == '' ||
			(form.email.value == '' && form.phone.value == '')) {
			alert(user_incomplete_error_message);
		} else if (confirm(confirm_account_update_message)){
			form.submit();
		}
	});
	
	$('button.update_commerce_button').click(function () {
		var form = $('form#update_account_form').get(0);
		if (form.name.value == '' || form.responsible.value == '' ||
			(form.email.value == '' && form.phone.value == '')) {
			alert(user_incomplete_error_message);
		} else if (confirm(confirm_account_update_message)){
			form.submit();
		}
	});
	
	$('button.update_association_button').click(function () {
		var form = $('form#update_account_form').get(0);
		if (form.name.value == '' || form.responsible.value == '' ||
			(form.email.value == '' && form.phone.value == '')) {
			alert(user_incomplete_error_message);
		} else if (confirm(confirm_account_update_message)){
			form.submit();
		}
	});
	
	$('select.assoc_combo').change(function() {
		var sel = $(this).children("option:selected")[0].value;
		if (sel != '0') {
			$('button.update_assoc_button').prop('disabled', false);
		} else {
			$('button.update_assoc_button').prop('disabled', true);
		}
	});
	
	$('button.update_assoc_button').click(function() {
		var form = this.form;
		if (confirm(change_supported_assoc_confirm)) {
			form.submit();
		}
	});
});

function showMessage(message) {
	if (message != null && message != '') {
		alert(message);
	}
}

function text2NotesValue(text) {
	var barcodes = text.replace(' ','').split('\n');
	var value = 0;
	var error = false;
	for (var i = 0; i < barcodes.length; i++) {
		switch(barcodes[i].charAt(0)) {
		case '0':
		case '5':
			value += 1;
			break;
		case '1':
		case '6':
			value += 2;
			break;
		case '2':
		case '7':
			value += 5;
			break;
		case '3':
		case '8':
			value += 10;
			break;
		case '4':
		case '9':
			value += 20;
			break;
		default:
			error = true;
			break;
		}
	}
	
	if (error) {
		return -1;
	} else {
		return value;
	}
}