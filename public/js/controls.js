/**
 *  JQuery functions
 */

var notesMap = {};
$(document).ready(function() {
	// Prevent the ENTER key to submit forms unwantedly
	$(document).keypress(function(e) {
		var evt = (e) ? e : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		if (evt.keyCode == 13 && node.type=="text" && $(node).hasClass('barcode_reader'))  {return false;} else {return true;} 
	});
	
	// Note barcode reader control functions
	$("input.barcode_reader").keypress(function(e) {
		if (e.keyCode == 13) {
			var reader = $(this);
			var barcode = reader.val();
			reader.val('');
			if (barcode.length == 6) {
				var current_codes = $($(reader).parent().parent().children('.hidden_barcodes')[0]).val();
				if (current_codes.indexOf(barcode) > -1) {
					alert(note_repeated_note);
				} else {
					$.get("/getnote?barcode="+barcode, function(data, status) {
						if (status == 'success') {
							if (!data.barcode) {
								alert(note_not_in_the_system_message);
							} else {
								notesMap[data.barcode] = data;
								if (reader.hasClass('stored_reader')) {
									if (data.status == 'STORED') {
										$($(reader.parent().siblings('.list_div')[0]).children('.barcode_list')[0]).trigger('add_barcode', data);
									} else {
										alert(note_not_stored_message);
									}
								} else if (reader.hasClass('non_expired_reader')) {
									if (data.status == 'CIRCULATING') {
										if (data.has_expired) {
											alert(note_expired_message);
										} else {
											$($(reader.parent().siblings('.list_div')[0]).children('.barcode_list')[0]).trigger('add_barcode', data);
										}
									} else {
										alert(note_not_circulating_message);
									}
								} else if (reader.hasClass('expiration_reader')) {
									if (data.status == 'CIRCULATING') {
										if (data.has_expired) {
											data['exp_rate'] = config_expiration_rate;
											$($(reader.parent().siblings('.list_div')[0]).children('.barcode_list')[0]).trigger('add_barcode', data);
										} else {
											alert(note_not_expired_message);
										}
									} else {
										alert(note_not_circulating_message);
									}
								}
							}
						}
					});
				}
			} else if (barcode.length > 0) {
				alert(invalid_barcode);
			}
		}
	});
	$("select.barcode_list").bind('add_barcode', function(e, data) {
		$(this).append($('<option ></option>').val(data.barcode).html(data.barcode));
		$($(this).parent().parent().children('.hidden_barcodes')[0]).trigger('update_barcodes');
		$($(this).parent().parent().children('.hidden_value')[0]).trigger('update_value');
		$($(this).parent().parent().children('.hidden_expcount')[0]).trigger('update_value');
		$($($(this).parent().siblings('.total_div')[0]).children('.total_span')[0]).trigger('update_value');
	});
	$("select.barcode_list").bind('remove_barcode', function(e, data) {
		$(this).children().each(function(i,n) {
			if (n.selected) {
				$(n).remove();
			}
		});
		$($(this).parent().siblings('.reader_div')[0]).children('.remove_button')[0].disabled = true;
		$($(this).parent().parent().children('.hidden_barcodes')[0]).trigger('update_barcodes');
		$($(this).parent().parent().children('.hidden_value')[0]).trigger('update_value');
		$($(this).parent().parent().children('.hidden_expcount')[0]).trigger('update_value');
		$($($(this).parent().siblings('.total_div')[0]).children('.total_span')[0]).trigger('update_value');
	});
	$("select.barcode_list").change(function() {
		if ($(this).val()) {
			$($(this).parent().siblings('.reader_div')[0]).children('.remove_button')[0].disabled = false;
		} else {	
			$("button.remove_button").disabled = true;
			$($(this).parent().siblings('.reader_div')[0]).children('.remove_button')[0].disabled = true;
		}
	});
	
	$("button.remove_button").bind('click', function() {
		$($($(this).parent().siblings('.list_div')[0]).children('.barcode_list')[0]).trigger('remove_barcode');
	});
	
	$('input.hidden_barcodes').bind('update_barcodes', function(e) {
		var hidden = $(this);
		$(hidden).val('');
		$($($(this).parent().children('.list_div')[0]).children('.barcode_list')[0]).children().each(function(i,n) {
			$(hidden).val($(hidden).val() + $(n).val() + '\n');
		});
	});
	
	$('input.hidden_value').bind('update_value', function(e) {
		$(this).val('');
		var total = 0;
		var hidden = $(this);
		var exp = $(this).hasClass('expiration_reader');
		var len = $($($(this).parent().children('.list_div')[0]).children('.barcode_list')[0]).children().length;
		if (len == 0) {
			$(this).val(0);
		}
		$($($(this).parent().children('.list_div')[0]).children('.barcode_list')[0]).children().each(function(i,n) {
			if (exp) {
				total += parseInt(notesMap[$(n).val()].value) * parseInt(notesMap[$(n).val()].expiration_count) * config_expiration_rate;
			} else {
				total += parseInt(notesMap[$(n).val()].value);
			}
			
			if (i == len-1) {
				$(hidden).val(total);
			}
		});
	});
	
	$('input.hidden_expcount').bind('update_value', function(e) {
		$(this).val('');
		var total = 0;
		var hidden = $(this);
		var exp = $(this).hasClass('expiration_reader');
		var len = $($($(this).parent().children('.list_div')[0]).children('.barcode_list')[0]).children().length;
		if (len == 0) {
			$(this).val(0);
		}
		$($($(this).parent().children('.list_div')[0]).children('.barcode_list')[0]).children().each(function(i,n) {
			if (exp) {
				total += parseInt(notesMap[$(n).val()].value) * parseInt(notesMap[$(n).val()].expiration_count) * config_expiration_rate;
			} else {
				total += parseInt(notesMap[$(n).val()].value);
			}
			
			if (i == len-1) {
				$(hidden).val(total);
			}
		});
	});
	
	$('span.total_span').bind('update_value', function(e) {
		var total = 0;
		var span = $(this);
		var exp = $(this).hasClass('expiration_reader');
		var len = $($($(this).parent().siblings('.list_div')[0]).children('.barcode_list')[0]).children().length;
		if (len == 0) {
			$(this).html(0);
		}
		$($($(this).parent().siblings('.list_div')[0]).children('.barcode_list')[0]).children().each(function(i,n) {
			if (exp) {
				total += parseInt(notesMap[$(n).val()].value) * parseInt(notesMap[$(n).val()].expiration_count) * config_expiration_rate;
			} else {
				total += parseInt(notesMap[$(n).val()].value);
			}
			
			if (i == len-1) {
				$(span).html(total);
			}
		});
	});
	
	// Dual currency reader controls
	$('input.official_button').change(function() {
		var official = $('input.official_input', $(this).parent().siblings('.official_currency_div')[0])[0];
		var local = $('input.barcode_reader', $(this).parent().siblings('.local_currency_div')[0])[0];
		if (this.checked) {
			official.disabled = false;
			local.disabled = true;
			
			var list = $('select.barcode_list', $(this).parent().siblings('.local_currency_div')[0])[0];
			$(list).empty();
			$('input.hidden_barcodes', $(this).parent().siblings('.local_currency_div')[0]).trigger('update_barcodes');
			$('input.hidden_value', $(this).parent().siblings('.local_currency_div')[0]).trigger('update_value');
			$('span.total_span', $(this).parent().siblings('.local_currency_div')[0]).trigger('update_value');
		}
	});
	$('input.local_button').change(function() {
		var official = $('input.official_input', $(this).parent().siblings('.official_currency_div')[0])[0];
		var local = $('input.barcode_reader', $(this).parent().siblings('.local_currency_div')[0])[0];
		if (this.checked) {
			local.disabled = false;
			official.disabled = true;
			$(official).val('');
		}
	});
	
});
