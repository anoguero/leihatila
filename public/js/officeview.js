/**
 * 
 */
$(document).ready(function() {
	$('button.operation_proceed_button').click(function() {
		var form = this.form;
		if (form.operation.value == 'buy') {
			if (confirm(office_about_2_buy + ": " + form.notes_value.value + " " + local_money+ " " + office_for + " " + form.notes_value.value + " " + official_money +"\n"+office_are_you_sure)) {
				form.submit();
			}
 		} else if (form.operation.value == 'sell') {
 			if (confirm(office_about_2_sell + ": " + form.notes_value.value + " " + local_money+ " " + office_for + " " + form.notes_value.value*(1-config_exchange_rate) + " " + official_money +"\n"+office_are_you_sure)) {
				form.submit();
			}
		} else if (form.operation.value == 'exp') {
			if (confirm(office_about_2_exp + ": " + form.notes_value.value + " " + local_money+ " " + office_for + " " + form.notes_expiration_count.value + " " + official_money +"\n"+office_are_you_sure)) {
				form.submit();
			}
		} else if (form.operation.value == 'change') {
			if (form.received_notes_value.value != form.given_notes_value.value) {
				alert(office_different_change_values);
			} else {
				if (confirm(office_about_2_change + ": " + form.received_notes_value.value + " " + local_money+ "\n"+office_are_you_sure)) {
					form.submit();
				}
			}
		}
	});
	
	$('button.pay_quota_button').click(function() {
		var form = this.form;
		var quota = 0;
		if (form.currency.value == 'official') {
			quota = form.quota_official_value.value;
		} else if (form.currency.value == 'local') {
			quota = form.quota_local_value.value;
		}
	
		var msg = office_quota_confirm.replace(':value:', quota).replace(':curr:', form.currency.value == 'official'? official_money : local_money);
		if (quota >= form.to_be_paid.value) {
			if (confirm(msg)) {
				form.submit();
			}
		} else {
			alert(office_insufficient_quota);
		}
	});
});


var active = 'tab1';

function refresh() {
	document.getElementById("Display_content").innerHTML = document.getElementById(active).innerHTML;
}

function toggleQuotaBox(form) {
	if (form.currency.value == 'official') {
		form.quotanotes.value = '';
		form.quotanotes.disabled = true;
	} else {
		form.quotanotes.disabled = false;
		form.quotanotes.focus();
	}
}

function change(tab) {
	var menuItems = new Array();
	menuItems[0] = document.getElementById('t1');
	menuItems[1] = document.getElementById('t2');
	menuItems[2] = document.getElementById('t3');
	menuItems[3] = document.getElementById('t4');
	menuItems[4] = document.getElementById('t5');
	menuItems[5] = document.getElementById('t6');
	menuItems[6] = document.getElementById('t7');
	
	document.getElementById("Display_content").innerHTML = document.getElementById(tab).innerHTML;
	active = tab;
	
	var i;
	var id = "t" + tab[tab.length-1];
	for (i = 0; i < menuItems.length; i++) {
		if (menuItems[i].id == id) {
			menuItems[i].setAttribute("class", "lhactive");
		}
		else {
			menuItems[i].setAttribute("class", "lh");
		}
	}
}

function confirmUserCreate(form) {
	if (isNaN(Number(form.donation.value)) || Number(form.donation.value) < 5) {
		alert("Para crear un nuevo socio es necesario un donativo de 5 Euro.");
		return false;
	}
	return confirm("Se crear� un nuevo usuario con una donaci�n de "+ form.donation.value +" Euros.\n�Est� seguro de a�adir el nuevo usuario?");
}

function confirmCommerceCreate() {
	return confirm("Se crear� un nuevo comercio. �Desea continuar?");
}

function confirmUserUpdate() {
	return confirm("�Est� seguro de querer guardar los cambios?");
}

function confirmUserDelete() {
	return confirm("Sen enviar� un email a Ekhi Txanpona para dar de baja al usuario. �Est� seguro?");
}

function processBuy() {
	document.getElementById("leihatila-operation").value = document.getElementById("ekhi-op-combo").value;
}

function processSell() {
	document.getElementById("leihatila-operation").value = document.getElementById("ekhi-op-combo").value;
}

function processExp() {
	document.getElementById("leihatila-operation").value = document.getElementById("ekhi-op-combo").value;
}

function testSelectChange(form) {
	if (form.last.value != "") {
		if (form.operation.value != form.last.value) {
			form.submit();
		}
	}
}

function confirmBuyAndSubmit(confirmation) {
	if (confirm(confirmation)) {
		document.getElementById("tab4-buy-confirm-form").submit();
	}
	else {
		document.getElementById("tab4-cancel-form").submit();
	}
}

function confirmSellAndSubmit(confirmation) {
	if (confirm(confirmation)) {
		document.getElementById("tab4-sell-confirm-form").submit();
	}
	else {
		document.getElementById("tab4-cancel-form").submit();
	}
}

function confirmExpAndSubmit(confirmation) {
	if (confirm(confirmation)) {
		document.getElementById("tab4-exp-confirm-form").submit();
	}
	else {
		document.getElementById("tab4-cancel-form").submit();
	}
}

function confirmChangeAndSubmit(confirmation) {
	if (confirm(confirmation)) {
		document.getElementById("tab4-change-confirm-form").submit();
	}
	else {
		document.getElementById("tab4-cancel-form").submit();
	}
}

function tellErrorAndSubmit(error) {
	alert("Ocurri� un error:\n" + error);
	document.getElementById("tab4-cancel-form").submit();
}

function showErrorAndCancel2(error) {
	alert("Ocurri� un error:\n" + error);
	document.getElementById("tab2-cancel-form").submit();
}

function showErrorAndCancel3(error) {
	alert("Ocurri� un error:\n" + error);
	document.getElementById("tab3-cancel-form").submit();
}

function showResultsDetails(url){
	window.open(url,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=1076,height=768,directories=no,location=no'); 
}

function confirmPartnerCreation(form, conf_message) {
	if (form.create.value) {
		// Validate the input
		var error = false;
		var errorMsg = office_missing_fields_error_message;
		if (form.name.value == null || form.name.value == '') {
			errorMsg += '\n\t - ' + office_name_field;
			error = true;
		}
		if (form.surname1.value == null || form.surname1.value == '') {
			errorMsg += '\n\t - ' + office_surname1_field;
		}
		if (form.association.value == '0') {
			errorMsg += '\n\t - ' + office_supported_association_field;
		}
		if ((form.phone.value == null || form.phone.value == '') && 
			(form.email.value == null || form.email.value == '')) {
			errorMsg += '\n\t - ' + office_phone_or_email_field;
		}
		
		if (error) {
			alert(errorMsg);
			return false;
		}
		
		if (form.currency.value == 'official') {
			var quotaVal = parseInt(form.quota_official_value.value);
			var msg = office_confirm_partner_create_official_message.replace(':value:',quotaVal);
			if (quotaVal < 5) {
				msg += '\n' + office_lower_quota_warning;
			}
			return confirm(msg);
		} else {
			var quotaVal = parseInt(form.quota_local_value.value);
			var msg = office_confirm_partner_create_local_message.replace(':value:',quotaVal);
			if (quotaVal < 5) {
				msg += '\n' + office_lower_quota_warning;
			}
			return confirm(msg);
		}
		
	}
	return confirm(office_confirm_partner_update);
}

function confirmOperationCreation(form, buymessage, sellmessage, expmessage,changemessage) {
	if (form.operation.value == 'buy') {
		var value = text2NotesValue(form['note-barcodes'].value);
		return confirm(buymessage.replace(':#value#:', value));
	} else if (form.operation.value == 'sell') {
		var value = text2NotesValue(form['note-barcodes'].value);
		return confirm(sellmessage.replace(':#value#:', value));
	} else if (form.operation.value == 'exp') {
		var value = text2NotesValue(form['note-barcodes'].value);
		return confirm(expmessage.replace(':#value#:', value));
	} else if (form.operation.value == 'change') {
		var val1 = text2NotesValue(form['received-notes'].value);
		var val2 = text2NotesValue(form['given-notes'].value);
		if (val1 != val2) {
			return false;
		}
		return confirm(changemessage.replace(':#value#:', value));
	}
	return false;
}
