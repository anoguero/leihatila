$(document).ready(function() {
	$('li.tab_item, li.active_tab_item').click(function () {
		var id = this.id;
		if (!$(this).hasClass('active_tab_item')) {
			$(this).removeClass('tab_item');
			$(this).addClass('active_tab_item');
			$(this).siblings().each(function(i,elem) {
				$(elem).addClass('tab_item');
				$(elem).removeClass('active_tab_item');
			});
			
			var sel = $('div#'+id+'.tab_content'); 
			sel.toggle();
			sel.siblings('.tab_content').each(function(i, elem) {
				if ($(elem).css('display') != 'none') {
					$(elem).toggle();
				}
			});
		}
	});
	
	$('div#'+tabbedmenu_active+'.tab_content').toggle();
});