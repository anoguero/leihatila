
$(document).ready(function() {
	$('button.show_transactions_button').click(function() {
		var url = 'system-transactions?fromDate=' + this.form.fromDate.value + '&toDate=' + this.form.toDate.value;
		window.open(url,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=1076,height=768,directories=no,location=no');
	});
	
	$('button.config_refresh_button').click(function() {
		var form = this.form;
		$(form.action).val('tab2-refresh-form');
		form.submit();
	});
	
	$('button.change_config_button').click(function() {
		var form = this.form;
		if (!confirm(admin_config_confirm_message)) {
			return false;
		}
		else if (!confirm(admin_config_change_warning_message)) {
			return false;
		}
		form.submit();
	});
	
	$('button.create_transaction_button').click(function() {
		var form = this.form;
		if (confirm(admin_transaction_create_confirm_message)) {
			form.submit();
		}
	});
	
	$('button.create_notes_button').click(function() {
		var form = this.form;
		var text = form.tab4NewNotesText.value;
		var barcodes = text.trim().replace(/\s+/g, " ").split(" ");
		var error = false;
		for (var i = 0; i < barcodes.length; i++) {
			if (isNaN(parseInt(barcodes[i])) || barcodes[i].length != 6) {
				error = true;
				break;
			}
		}
		if (error) {
			alert(admin_erroneous_barcodes);
		} else {
			var mess = admin_notes_create_confirm + '\n';
			for (var i = 0; i < barcodes.length; i++) {
				mess += barcodes[i] + " ";
			}
			if (confirm(mess)) {
				form.submit();
			}
		}
		
	});
	
	$('button.create_notes_range_button').click(function() {
		var form = this.form;
		var start = form.rangeStart.value;
		var end = form.rangeEnd.value;
		if (start.length != 6 || end.length != 6 || isNaN(parseInt(start)) || isNaN(parseInt(end))) {
			alert(admin_invalid_note_barcode);
			$(form.rangeStart).val("");
			$(form.rangeEnd).val("");
		} else if (parseInt(end) <= parseInt(start)) {
			alert(admin_range_start_before_end);
			$(form.rangeStart).val($(form.rangeEnd).val());
			$(form.rangeEnd).val($(form.rangeEnd).val());
		} else if (confirm(admin_notesrange_create_confirm.replace(":start:", start).replace(":end:",end))) {
			form.submit();
		}
		
	});
	
	$('button.send_mail_button').click(function() {
		var form = this.form;
		if (confirm(admin_send_email_confirm)) {
			form.submit();
		}
	}); 
}); 


