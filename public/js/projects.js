$(document).ready(function() {
	$('.new_project_button').click(function() {
		var form = $('form#project_form').get(0);
		// Hide the panel if visible
		if($(form).parent('div').css('display') != 'none') {
			$(form).parent('div').toggle();
		}
		
		// Update the info
		$(form.project_id).val('-1');
		$(form.name).val('Nuevo Proyecto');
		$(form.description).val('');
		$(form.requested_budget).val('0.00');
		$(form.granted_budget).val('0.00');
		$('img#project_photo').attr('src', host_name+no_image_src);
		
		// Show the panel again
		$(form).parent('div').toggle();
	});
	
	$('.edit_project_button').click(function() {
		var list = $('.project_list').get(0);
		$.get("/project?id="+$(list).val(), function(data, status) {
			if (status == 'success') {
				var form = $('form#project_form').get(0);
				// Hide the current project panel
				if($(form).parent('div').css('display') != 'none') {
					$(form).parent('div').toggle();
				}
				// Update the project info
				$(form.project_id).val(data.id);
				$(form.name).val(data.name);
				$(form.description).val(data.description);
				$(form.requested_budget).val(data.requested_funding);
				$(form.granted_budget).val(data.granted_funding);
				
				if (data.image != null) {
					$('img#project_photo').attr('src', data.image);
				} else {
					$('img#project_photo').attr('src', host_name+no_image_src);
				}
				
				// Show the panel again
				$(form).parent('div').toggle();
			} else {
				alert(no_such_project_error);
			}
		});
	});
	
	$('.delete_project_button').click(function() {
		
	});
	
	$('.project_list').change(function() {
		if ($(this).val()) {
			$("button.edit_project_button").prop( "disabled", false );
			$("button.delete_project_button").prop( "disabled", false );
		} else {
			$("button.edit_project_button").prop( "disabled", true );
			$("button.delete_project_button").prop( "disabled", true );
		}
	});
	
	$('.save_project_button').click(function() {
		var form = this.form;
		var status = $(form.status).val();
		
		if (status == '0') {
			form.submit();
		} else {
			
		}
	});
});