<?php

class Commerce extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'commerces';

	
	public function updateQuota($quotaPaid, $numQuotas) {
		$date = new DateTime($this->yearly_quota_paid_until);
		$date->add(new DateInterval('P'.$numQuotas.'Y'));
		$this->yearly_quota_paid_until = $date->format('Y-m-d');
		$this->total_quota += $quotaPaid;
	}
	
	public function isQuotaUpdated() {
		$start_ts = mktime();
		$end_ts = strtotime($this->yearly_quota_paid_until);
		$diff = $end_ts - $start_ts;
		return round($diff / 86400) >= 0;
	}
	
}