<?php

class Note extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notes';
	protected $appends = array('has_expired', 'expiration_count');
	
	/**
	 * Sets the note value according to its barcode
	 */
	public function setNoteValue() {
		if ($this->barcode[0] == '0' || $this->barcode[0] == '5') {
			$this->value = 1;
		}
		else if ($this->barcode[0] == '1' || $this->barcode[0] == '6') {
			$this->value = 2;
		}
		else if ($this->barcode[0] == '2' || $this->barcode[0] == '7') {
			$this->value = 5;
		}
		else if ($this->barcode[0] == '3' || $this->barcode[0] == '8') {
			$this->value = 10;
		}
		else if ($this->barcode[0] == '4' || $this->barcode[0] == '9') {
			$this->value = 20;
		}
		else {
			$this->value = 0;
		}
	}
	
	public function setToStored() {
		$this->status = 'STORED';
		$this->next_expiration_date = null;
	}
	
	public function setToCirculating() {
		$this->status = 'CIRCULATING';
		$this->next_expiration_date = Leihatila::getNextExpirationDate();
	}
	
	public function updateExpiration() {
		$this->next_expiration_date = Leihatila::getNextExpirationDate();
	}
	
	public function getHasExpiredAttribute() {
		return $this->next_expiration_date != null && Leihatila::getExpirationCount($this) > 0;
	}
	
	public function getExpirationCountAttribute() {
		return $this->next_expiration_date != null ? Leihatila::getExpirationCount($this) : 0;
	}

}