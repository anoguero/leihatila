<?php

class ExchangeOffice extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'exchange_offices';

}