<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create system_transactions table
		Schema::create('system_transactions', function (Blueprint $table) {
		$table->increments('id');
		$table->double('local_money_amount');
		$table->double('official_money_amount');
		$table->integer('office_id')->nullable();
		$table->text('concept');
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop system_transactions table
		Schema::drop('system_transactions');
	}

}
