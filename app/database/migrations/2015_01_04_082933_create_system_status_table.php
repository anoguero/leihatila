<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemStatusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create system_status table
		Schema::create ( 'system_status' , function (Blueprint $table) {
		$table->increments ( 'id' );
		$table->double('total_official_money');
		$table->double('total_physical_local_money');
		$table->double('total_digital_local_money');
		$table->double('exchange_collected_money');
		$table->double('expiration_collected_money');
		$table->double('quota_collected_money');
		$table->double('circulating_local_money');
		$table->timestamps ();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		// Drop commerces table
		Schema::drop ( 'system_status' );
	}

}
