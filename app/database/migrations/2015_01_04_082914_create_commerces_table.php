<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create commerces
		Schema::create('commerces', function (Blueprint $table) {
		$table->increments('id');
		$table->string('barcode',10)->unique();
		$table->string('name', 45);
		$table->string('id_card', 12)->nullable();
		$table->string('address')->nullable();
		$table->double('latitude')->nullable();
		$table->double('longitude')->nullable();
		$table->text('description');
		$table->string('responsible');
		$table->string('email', 100)->nullable();
		$table->string('phone', 15)->nullable();
		$table->string('webpage', 100)->nullable();
		$table->integer('office_id');
		$table->string('image')->nullable();
		$table->integer('supported_association_id')->nullable();
		$table->date('yearly_quota_paid_until')->default('2014-03-01');
		$table->double('total_quota')->default(0.00);
		$table->double('digital_local_money')->default(0);
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop commerces table
		Schema::drop('commerces');
	}

}
