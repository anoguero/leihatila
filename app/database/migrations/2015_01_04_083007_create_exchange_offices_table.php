<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeOfficesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create offices
		Schema::create('exchange_offices', function (Blueprint $table) {
		$table->increments('id');
		$table->string('name', 45);
		$table->string('address')->nullable();
		$table->double('total_local_money')->default(0.0);
		$table->double('total_official_money')->default(0.0);
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop offices
		Schema::drop('exchange_offices');
	}

}
