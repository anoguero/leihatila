<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create category_sets table
		Schema::create('category_sets', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('commerce_id');
		$table->integer('category_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop category_sets table
		Schema::drop('category_sets');
	}

}
