<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create Users Table
		Schema::create ( 'users', function (Blueprint $table) {
			$table->increments ( 'id' );
			$table->string ( 'username', 45 )->unique ();
			$table->string ( 'password', 64 );
			$table->integer ( 'usertype' );
			$table->integer ( 'related_user_id' )->nullable ();
			$table->string ( 'remember_token', 64 );
			$table->timestamps ();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop users table
		Schema::drop('users');
	}

}
