<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create Partners Table
		Schema::create('partners', function (Blueprint $table) {
		$table->increments('id');
		$table->string('barcode',10)->unique();
		$table->string('name', 45);
		$table->string('surname1', 45);
		$table->string('surname2', 45)->nullable();
		$table->string('id_card', 12)->nullable();
		$table->string('address')->nullable();
		$table->string('email', 100)->nullable();
		$table->string('phone', 15)->nullable();
		$table->integer('office_id');
		$table->double('initial_quota');
		$table->integer('supported_association_id')->nullable();
		$table->double('digital_local_money')->default(0);
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop partners table
		Schema::drop('partners');
	}

}
