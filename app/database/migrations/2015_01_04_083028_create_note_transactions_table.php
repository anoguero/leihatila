<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create note_transactions table
		Schema::create('note_transactions', function (Blueprint $table) {
		$table->increments('id');
		$table->enum('type', ['BUY','REDEEM','EXPIRATION','CHANGE', 'QUOTA']);
		$table->double('local_money_amount')->default(0.0);
		$table->double('official_money_amount')->default(0.0);
		$table->string('user_barcode', 10);
		$table->integer('office_id');
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop note_transactions table
		Schema::drop('note_transactions');
	}

}
