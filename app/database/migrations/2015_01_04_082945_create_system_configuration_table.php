<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Fluent;
use Illuminate\Database\Schema\Grammars\MySqlGrammar;

/**
 * Extended version of MySqlGrammar with
 * support of 'set' data type
 */
class ExtendedMySqlGrammar extends MySqlGrammar {

	/**
	 * Create the column definition for an 'set' type.
	 *
	 * @param  \Illuminate\Support\Fluent  $column
	 * @return string
	 */
	protected function typeSet(Fluent $column)
	{
		return "set('".implode("', '", $column->allowed)."')";
	}

}

/**
 * Extended version of Blueprint with
 * support of 'set' data type
 */
class ExtendedBlueprint extends Blueprint {

	/**
	 * Create a new 'set' column on the table.
	 *
	 * @param  string  $column
	 * @param  array   $allowed
	 * @return \Illuminate\Support\Fluent
	 */
	public function set($column, array $allowed)
	{
		return $this->addColumn('set', $column, compact('allowed'));
	}

}

class CreateSystemConfigurationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// register new grammar class
		DB::connection()->setSchemaGrammar(new ExtendedMySqlGrammar());
		$schema = DB::connection()->getSchemaBuilder();
		
		// replace blueprint
		$schema->blueprintResolver(function($table, $callback) {
			return new ExtendedBlueprint($table, $callback);
		});
		
		// Create system_status table
		$schema->create ( 'system_configuration' , function (ExtendedBlueprint $table) {
		$table->increments ( 'id' );
		$table->double('expiration_rate');
		$table->double('exchange_rate');
		$table->integer('expiration_day');
		$table->set('expiration_months', range(1,12));
		$table->integer('free_expiration_days');
		$table->timestamps ();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		// Drop commerces table
		Schema::drop ( 'system_configuration' );
	}

}
