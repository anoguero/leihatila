<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create projects table
		Schema::create('projects', function (Blueprint $table) {
		$table->increments('id');
		$table->string('name');
		$table->text('description');
		$table->integer('association_id');
		$table->string('image')->nullable();
		$table->enum('status', ['DRAFT', 'READY', 'ACTIVE', 'FUNDED']);
		$table->double('requested_funding');
		$table->double('granted_funding')->default(0.0);
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop projects table
		Schema::drop('projects');
	}

}
