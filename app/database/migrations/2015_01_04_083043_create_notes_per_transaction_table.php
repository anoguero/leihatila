<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesPerTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create note_transactions table
		Schema::create('notes_per_transaction', function (Blueprint $table) {
		$table->increments('id');
		$table->integer('transaction_id');
		$table->integer('note_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop note_transactions table
		Schema::drop('notes_per_transaction');
	}

}
