<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create notes table
		Schema::create('notes', function (Blueprint $table) {
		$table->increments('id');
		$table->string('barcode', 10);
		$table->enum('value', ['1','2','5','10','20']);
		$table->enum('status', ['CIRCULATING','STORED','DESTROYED']);
		$table->date('next_expiration_date')->nullable();
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop notes table
		Schema::drop('notes');
	}

}
