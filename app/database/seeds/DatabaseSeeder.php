<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CategoriesSeeder');
		$this->call('SystemSeeder');
		$this->call('OfficesSeeder');
		$this->call('AssociationsSeeder');
		$this->call('UserTableSeeder');
	}

}

class OfficesSeeder extends Seeder {

	public function run()
	{
		DB::table('exchange_offices')->delete();

		ExchangeOffice::create(array('name' => 'Oficina Online',
		'address' => 'Muelle de Ibeni 1, Bilbao'));

	}

}

class AssociationsSeeder extends Seeder {

	public function run()
	{
		DB::table('associations')->delete();

		Association::create(
		array('barcode' => '5000000001',
		'name' => 'Ekhi Elkartea',
		'id_card' => 'G95729224',
		'address' => 'Muelle de Ibeni 1, Bilbao',
		'description' => 'Asociación para la promoción de la economía local desde los valores del decrecimiento. Promotora de Ekhi Txanpona.',
		'responsible' => 'Esther Jimenez Redal',
		'email' => 'ekhi.txanpona@gmail.com',
		'webpage' => 'www.ekhitxanpona.org',
		'office_id' => '1',
		'supported_association_id' => '1',
		));
	}

}

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create(array('username' => 'ekhi.system',
		                   'password' => Hash::make('Silvio.Gessel_00'),
		                   'usertype' => 0));
		
		User::create(array('username' => 'ekhi.oficina',
		'password' => Hash::make('Silvio.Gessel_00'),
		'related_user_id' => 1,
		'usertype' => 1));
		
		User::create(array('username' => '5000000001',
		'password' => Hash::make('Silvio.Gessel_00'),
		'related_user_id' => 1,
		'usertype' => 4));
	}

}

class CategoriesSeeder extends Seeder {
	public function run()
	{
		DB::table('categories')->delete();
	
		Category::create(array('category_name' => '<lang=es>Bares y Restaurantes</lang><lang=eus>Tabernak eta Jatetxeak</lang>'));
		Category::create(array('category_name' => '<lang=es>Ropa y Calzado</lang><lang=eus>Arropa eta Zapatak</lang>'));
		Category::create(array('category_name' => '<lang=es>Alimentación</lang><lang=eus>Elikadura</lang>'));
		Category::create(array('category_name' => '<lang=es>Libros</lang><lang=eus>Liburuak</lang>'));
		Category::create(array('category_name' => '<lang=es>Complementos, Bisutería y Joyería</lang><lang=eus>Konplementuak eta Bitxiak</lang>'));
		Category::create(array('category_name' => '<lang=es>Peluquería</lang><lang=eus>Ileapaindegia</lang>'));
		Category::create(array('category_name' => '<lang=es>Arte</lang><lang=eus>Arte</lang>'));
		Category::create(array('category_name' => '<lang=es>Herboristería/lang><lang=eus>Belar-Denda</lang>'));
		Category::create(array('category_name' => '<lang=es>Juguetería</lang><lang=eus>Jostailuak</lang>'));
		Category::create(array('category_name' => '<lang=es>Bebé</lang><lang=eus>Haurtxoak</lang>'));
		Category::create(array('category_name' => '<lang=es>Servicios</lang><lang=eus>Serbitzuak</lang>'));
		Category::create(array('category_name' => '<lang=es>Electricidad</lang><lang=eus>Elektrizitatea</lang>'));
	}
}

class SystemSeeder extends Seeder {

	public function run()
	{
		DB::table('system_status')->delete();
		DB::table('system_configuration')->delete();

		SystemStatus::create(array(
		'total_official_money'=>0.00,
		'total_physical_local_money'=>0.00,
		'total_digital_local_money'=>0.00,
		'exchange_collected_money'=>0.00,
		'expiration_collected_money'=>0.00,
		'quota_collected_money'=>0.00,
		'circulating_local_money'=>0.00));
		
		SystemConfiguration::create(array(
		'expiration_rate' => 0.02,
		'exchange_rate' => 0.05,
		'expiration_day' => 1,
		'expiration_months' => '1,4,7,10',
		'free_expiration_days' => 15));
	}

}
