<?php

class LeihatilaController extends BaseController {
	
	public function getnote() {
		$barcode = Input::get('barcode');
		$note = Note::select()->where('barcode', '=', $barcode)->get()->first();
		if (!$note) {
			return new Note();
		}
		return $note;
	}
	
	public function project() {
		$id = Input::get('id');
		$project = Project::select()->where('id','=',$id)->get()->first();
		if (!$project) {
			return null;
		}
		if ($project->image != null) {
			$project->image = Request::root().'/'.$project->image;
		}
		return $project;
	}
}