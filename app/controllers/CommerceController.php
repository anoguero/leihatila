<?php

class CommerceController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 3) {
			$commerce = Commerce::find(Auth::user()->related_user_id);
			$info = array('active' => 'tab1', 'commerce' => $commerce);
			return View::make('commerce', $info);
		} else {
			return Lang::get("messages.notallowed");
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::guest() || Auth::user()->usertype != 3) {
			return Lang::get("messages.notallowed");
		}
		$action = Input::get('action');
		$active = Input::get('active');
		$commerce = Commerce::find(Auth::user()->related_user_id);
		
		// Error management variables
		$error = false;
		$error_messages = array();
		$warn_messages = array();
		$confirmation = '';
		
		switch ($action) {
			case 'tab1-updateuser-form':
				$commerce->name = Input::get('name');
				$commerce->description = Input::get('description');
				$commerce->responsible = Input::get('responsible');
				$commerce->email = Input::get('email');
				$commerce->address = Input::get('address');
				$commerce->phone = Input::get('phone');
				$commerce->webpage = Input::get('webpage');
				$commerce->id_card = Input::get('id_card');
				
				// Geocode the address using GoogleMaps
				$latlng = Leihatila::geocodeAddress($commerce->address);
				if (!$latlng) {
					$warn_messages[] = Lang::get('messages.geocoding-failed');
				} else {
					$commerce->latitude = $latlng['lat'];
					$commerce->longitude = $latlng['lng'];
				}
				
				// Validate the new partner before continuing
				if (!Leihatila::validateCommerce($commerce, $error_messages)) {
					$info['commerce'] = $commerce;
					break;
				}
				
				if (!$commerce->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					$confirmation = Lang::get('messages.commerce-update-success');
				}
				
				break;
			case 'tab1-deleteuser-form':
				break;
			case 'tab2-updatepass-form':
				$previous = Input::get('prev_pass');
				$new1 = Input::get('new_pass1');
				$new2 = Input::get('new_pass2');
				$error = !$this->updatePasswordAction($previous, $new1, $new2, $confirmation, $error_messages, $warn_messages);
				break;
			case 'tab3-updateassoc-form':
				$assoc_id = Input::get('association');
				$assoc = Association::find($assoc_id);
				if (!$assoc) {
					$error_messages[] = Lang::get('messages.invalid-association-id');
				} else {
					$user->supported_association_id = $assoc_id;
					if (!$user->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else {
						$confirmation = Lang::get('messages.supported-association-update-success');
					}
				}
				break;
		}
		
		$info = array('active' => $active, 'commerce' => $commerce);
		$info['message'] = Leihatila::createMessage($confirmation, $error_messages, $warn_messages);
		return View::make('commerce', $info);
	}

	private function updatePasswordAction($previous, $new1, $new2, &$confirmation, &$error_messages, &$warn_messages) {
		if (!Hash::check($previous, Auth::user()->password)) {
			$error_messages[] = Lang::get('messages.wrong-password');
			return false;
		}
		
		if ($new1 != $new2) {
			$error_messages[] = Lang::get('messages.different-passwords-error');
			return false;
		}
		
		$user = Auth::user();
		$user->password = Hash::make($new1);
		if (!$user->save()) {
			$error_messages[] = Lang::get('messages.db-save-error');
			return false;
		}
		
		$confirmation = Lang::get('messages.pass-change-confirm');
		return true;
	}
}
