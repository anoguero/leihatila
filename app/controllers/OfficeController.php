<?php

class OfficeController extends BaseController {


	public function index()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 1) {
			$office = ExchangeOffice::find(Auth::user()->related_user_id);
			$config = Leihatila::getSystemConfiguration();
			$status = Leihatila::getSystemStatus();
			$info = array('active' => 'tab1', 'config' => $config,
					'status' => $status, 'office' => $office,
					'user' => null, 'commerce' => null, 'association' => null
			);
			// Show Exchange Office View
			return View::make('office', $info);
		} else {
			return Lang::get("messages.notallowed");
		}
	}
	
	public function store()
	{
		if (Auth::guest() || Auth::user()->usertype != 1) {
			return Lang::get("messages.notallowed");
		} else {
			// Info to be passed
			$info = array();
			$info['user'] = null;
			$info['commerce'] = null;
			$info['association'] = null;
			
			// Configuration variables
			$action = Input::get('action');
			$config = Leihatila::getSystemConfiguration();
			$status = Leihatila::getSystemStatus();
			$office = ExchangeOffice::find(Auth::user()->related_user_id);
			
			// Error management variables
			$error = false;
			$error_messages = array();
			$warn_messages = array();
			$confirmation = '';
			
			// Controller logic
			switch ($action) {
				case 'tab2-getuser-form':
					$barcode = (string)Input::get('userbarcode');
					$user = Partner::select()->where('barcode', '=', $barcode)->get()->first();
					if (!$user) {
						if (!Leihatila::validateUserBarcode($barcode, 'partner')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$user = new Partner();
						$user->barcode = $barcode;
					}
					$info['user'] = $user;
					break;
				case 'tab3-getuser-form':
				case 'tab6-getuser-form':
					$barcode = (string)Input::get('userbarcode');
					$commerce = Commerce::select()->where('barcode', '=', $barcode)->get()->first();
					if (!$commerce) {
						if (!Leihatila::validateUserBarcode($barcode, 'commerce')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$commerce = new Commerce();
						$commerce->barcode = $barcode;
					}
					$info['commerce'] = $commerce;
					break;
				case 'tab4-getuser-form':
					$barcode = (string)Input::get('userbarcode');
					$association = Association::select()->where('barcode', '=', $barcode)->get()->first();
					if (!$association) {
						if (!Leihatila::validateUserBarcode($barcode, 'association')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$association = new Association();
						$association->barcode = $barcode;
					}
					$info['association'] = $association;
					break;
				case 'tab2-setuser-form':
					$barcode = Input::get('barcode');
					DB::beginTransaction();
					$user = Partner::select()->where('barcode', '=', $barcode)->get()->first();
					$isUpdate = true;
					if (!$user) {
						if (!Leihatila::validateUserBarcode($barcode, 'partner')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$isUpdate = false;
						$user = new Partner();
						$user->barcode = $barcode;
						$user->office_id = $office->id;
					}
					$user->name = Input::get('name');
					$user->surname1 = Input::get('surname1');
					$user->surname2 = Input::get('surname2');
					$user->email = Input::get('email');
					$user->address = Input::get('address');
					$user->phone = Input::get('phone');
					$user->id_card = Input::get('id_card');
					$assoc_id = Input::get('association');
					$assoc = Association::find($assoc_id);
					if (!$assoc) {
						$warn_messages[] = Lang::get('messages.invalid-association-id');
					} else {
						$user->supported_association_id = $assoc_id;
					}
					
					$curr = Input::get('currency');
					$initial = $curr == 'official' ? Input::get('quota_official_value') : Input::get('quota_local_value'); 
					
					if (!$isUpdate) {
						$user->initial_quota = $initial;
						if (!$user->save()) {
							$error_messages[] = Lang::get('messages.db-save-error');
						}
						$this->handleInitialQuota($user, $curr, $user->initial_quota, Input::get('quota_local_barcodes'), $office, $config, $status, $error_messages);
					}
					
					// Validate the new partner before continuing
					if (!Leihatila::validatePartner($user, $error_messages)) {
						$info['user'] = $user;
						DB::rollBack();
						break;
					}
					
					if (!$user->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else {
						if ($isUpdate) {
							$confirmation = Lang::get('messages.partner-update-success');
						} else {
							$confirmation = Lang::get('messages.partner-create-success');
							
							if ($user->email) {
								$login = Leihatila::createNewUser($user->barcode, 2, $user->id);
								Leihatila::sendWelcomeEMail($user->email, $user, $login['pass']);
							}
						}
					}
					
					if (count($error_messages) > 0) {
						DB::rollBack();
					} else {
						DB::commit();
					}
					break;
				case 'tab2-updateuser-form' :
					break;
				case 'tab2-deleteuser-form' :
					break;
				
				case 'tab3-setuser-form' :
					$barcode = Input::get('barcode');
					DB::beginTransaction();
					$commerce = Commerce::select()->where('barcode', '=', $barcode)->get()->first();
					$isUpdate = true;
					if (!$commerce) {
						if (!Leihatila::validateUserBarcode($barcode, 'commerce')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$isUpdate = false;
						$commerce = new Commerce();
						$commerce->barcode = $barcode;
						
						$date = new DateTime('NOW');
						$date->sub(new DateInterval('P1D'));
						
						$commerce->yearly_quota_paid_until = $date->format('Y-m-d');
						$commerce->office_id = $office->id;
					}
					$commerce->name = Input::get('name');
					$commerce->description = Input::get('description');
					$commerce->responsible = Input::get('responsible');
					$commerce->email = Input::get('email');
					$commerce->address = Input::get('address');
					$commerce->phone = Input::get('phone');
					$commerce->webpage = Input::get('webpage');
					$commerce->id_card = Input::get('id_card');
					
					$assoc_id = Input::get('association');
					$assoc = Association::find($assoc_id);
					if (!$assoc) {
						$warn_messages[] = Lang::get('messages.invalid-association-id');
					} else {
						$commerce->supported_association_id = $assoc_id;
					}
					
					// Validate the new partner before continuing
					if (!Leihatila::validateCommerce($commerce, $error_messages)) {
						$info['commerce'] = $commerce;
						break;
					}
					
					// Geocode the address using GoogleMaps
					$latlng = Leihatila::geocodeAddress($commerce->address);
					if (!$latlng) {
						$warn_messages[] = Lang::get('messages.geocoding-failed');
					} else {
						$commerce->latitude = $latlng['lat'];
						$commerce->longitude = $latlng['lng'];
					}
					
					
					if (!$commerce->save()) {
						$error_messages[] = Lang::get('messages.commerce-set-error');
					} else {
						if ($isUpdate) {
							$confirmation = Lang::get('messages.commerce-update-success');
						} else {
							$confirmation = Lang::get('messages.commerce-create-success');
							
							if ($commerce->email) {
								$login = Leihatila::createNewUser($commerce->barcode, 3, $commerce->id);
								Leihatila::sendWelcomeEMail($commerce->email, $commerce, $login['pass']);
							}
						}
					}
					
					if (count($error_messages) > 0) {
						DB::rollBack();
					} else {
						DB::commit();
					}
					
					break;
				case 'tab3-updateuser-form' :
					break;
				case 'tab3-deleteuser-form' :
					break;
				
				case 'tab4-setuser-form' :
					$barcode = Input::get('barcode');
					DB::beginTransaction();
					$association = Association::select()->where('barcode', '=', $barcode)->get()->first();
					$isUpdate = true;
					if (!$association) {
						if (!Leihatila::validateUserBarcode($barcode, 'association')) {
							$error_messages[] = Lang::get('messages.invalid-user-barcode');
							break;
						}
						$isUpdate = false;
						$association = new Association();
						$association->barcode = $barcode;
						$association->office_id = $office->id;
					}
					$association->name = Input::get('name');
					$association->description = Input::get('description');
					$association->responsible = Input::get('responsible');
					$association->email = Input::get('email');
					$association->address = Input::get('address');
					$association->phone = Input::get('phone');
					$association->webpage = Input::get('webpage');
					$association->id_card = Input::get('id_card');
					
					$curr = Input::get('currency');
					$initial = $curr == 'official' ? Input::get('quota_official_value') : Input::get('quota_local_value');
					
					if (!$isUpdate) {
						$association->initial_quota = $initial;
						$this->handleInitialQuota($user, $curr, $association->initial_quota, Input::get('quotanotes'), $office, $config, $status, $error_messages);
					}
					
					// Validate the new association before continuing
					if (!Leihatila::validateAssociation($association, $error_messages)) {
						$info['association'] = $association;
						break;
					}
						
					// Geocode the address using Google Maps
					$latlng = Leihatila::geocodeAddress($association->address);
					if (!$latlng) {
						$warn_messages[] = Lang::get('messages.geocoding-failed');
					} else {
						$association->latitude = $latlng['lat'];
						$association->longitude = $latlng['lng'];
					}
						
					if (!$association->save()) {
						$error_messages[] = Lang::get('messages.association-set-error');
					} else {
						$association->supported_association_id = $association->id;
						if ($isUpdate) {
							$confirmation = Lang::get('messages.association-update-success');
						} else {
							$confirmation = Lang::get('messages.association-create-success');
							
							if ($association->email) {
								$login = Leihatila::createNewUser($association->barcode, 4, $association->id);
								Leihatila::sendWelcomeEMail($association->email, $association, $login['pass']);
							}
						}
					}
					
					if (count($error_messages) > 0) {
						DB::rollBack();
					} else {
						DB::commit();
					}
					break;
				case 'tab4-updateuser-form' :
					break;
				case 'tab4-deleteuser-form' :
					break;
				case 'tab5-user-operation-form':
					$opSel = array();
					$opSel['barcode'] = Input::get('userbarcode');
					$opSel['operation'] = Input::get('operation');
					$user = Leihatila::getUser(Input::get('userbarcode'));
					$opSel['user'] = $user;
					if (!$user) {
						$error_messages[] = Lang::get('messages.invalid-user-barcode');
					}
					$opSel['name'] = Leihatila::getUserName(Input::get('userbarcode'));
					$info['opSel'] = $opSel;
					break;
				case 'tab5-perform-operation-form':
					$op = Input::get('operation');
					$code = Input::get('barcode');
					$expected = Input::get('notes_value') ? Input::get('notes_value') : Input::get('received_notes_value');
					if ($op != 'change') {
						$notestext1 = Input::get('notes_barcodes');
						$notestext2 = null;
					} else {
						$notestext1 = Input::get('received_notes_barcodes');
						$notestext2 = Input::get('given_notes_barcodes');;
					}
					DB::beginTransaction();
					$this->executeExchangeOperation($op, $code, $notestext1, $notestext2, $expected, $office, $config, $status, $error_messages, $warn_messages);
					
					if (count($error_messages) > 0) {
						DB::rollBack();
					} else {
						DB::commit();
						$confirmation = Lang::get('messages.operation-completed-sucessfully');
					}
					break;
				case 'tab6-pay-quota-form':
					$barcode = Input::get('barcode');
					$toBePaid = Input::get('to_be_paid');
					$pendingQuotas = Input::get('pending_quotas');
					$curr = Input::get('currency');
					
					$commerce = Commerce::select()->where('barcode', '=', $barcode)->get()->first();
					
					if ($curr == 'official') {
						$official = Input::get('quota_official_value');
						if ($official < $toBePaid) {
							$info['commerce'] = $commerce;
							$error_messages[] = Lang::get('messages.insufficient-quota');
						} else {
							DB::beginTransaction();
							// Update the commerce
							$commerce->updateQuota($official, $pendingQuotas);
							if (!$commerce->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							// Update the account of the office
							$office->total_official_money += $official;
							if (!$office->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							// Create a transaction
							$t = LeihatilaDataModel::createNoteTransaction('QUOTA', $official, 0, $commerce, $office);
							if (!$t->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							// Update system status
							$status->quota_collected_money += $official;
							if (!$status->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							if (count($error_messages) > 0) {
								DB::rollBack();
							} else {
								DB::commit();
								$confirmation = Lang::get('messages.operation-completed-sucessfully');
							}
						}
					} else if ($curr == 'local') {
						$local = Input::get('quota_local_value');
						$notestext = Input::get('quota_local_barcodes');
						$notes = Leihatila::text2Barcodes($notestext, true);
						$notesValue = 0;
						$validNotes = array();
						for ($i=0; $i < count($notes); $i++) {
							$note = Note::select()->where('barcode', '=', $notes[$i])->get()->first();
							if (!$note) {
								$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
							} else if ($note->status != 'CIRCULATING') {
								$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-circulating'), 'actual' => Lang::get('messages.status-stored')]);
							} else if (Leihatila::getExpirationCount($note, $config) > 0) {
								$error_messages[] = Lang::get('messages.expirated-note', ['number' => ''.$i]);
							} else {
								$validNotes[] = $note;
							}
						}
						
						if (count($notes) != count($validNotes)) {
							$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
						} else if (Leihatila::getTotalValue($validNotes) != $local) {
							$error_messages[] = Lang::get('messages.invalid-expected-local-amount', ['expected' => $quota, 'actual' => ''.Leihatila::getTotalValue($validNotes)]);
						} else {
							DB::beginTransaction();
							// Deactivate Notes & Update the status of the office
							foreach($validNotes as $note) {
								$note->setToStored();
								if (!$note->save()) {
									$error_messages[] = Lang::get('messages.db-save-error');
								}
							}
							
							$office->total_local_money += $local;
							if (!$office->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							// Update the commerce
							$commerce->updateQuota($local, $pendingQuotas);
							if (!$commerce->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
						
							// Create a transaction associated to the payment
							$trans = LeihatilaDataModel::createNoteTransaction('QUOTA', 0, $local, $commerce, $office);
							if (!$trans->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							} else {
								if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes)) {
									$error_messages[] = Lang::get('messages.db-save-error');
								}
							}
						
							// Update system status
							$status->quota_collected_money += $local * (1-$config->exchange_rate);
							if (!$status->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							}
							
							if (count($error_messages) > 0) {
								DB::rollBack();
							} else {
								DB::commit();
								$confirmation = Lang::get('messages.operation-completed-sucessfully');
							}
						}
					}
					break;
				case 'tab7-getdates-form':
					$fromDate = Input::get('fromDate');
					$toDate = Input::get('toDate');
					$transactions = NoteTransaction::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
					$localMoney = 0;
					$officialMoney = 0;
					foreach ($transactions as $t) {
						$localMoney += $t->local_money_amount;
						$officialMoney += $t->official_money_amount;
					}
					$info['lastFromDate'] = $fromDate;
					$info['lastToDate'] = $toDate;
					$info['localMoneyStatus'] = $localMoney;
					$info['officialMoneyStatus'] = $officialMoney;
					break;
			}
			
			
			$info['active'] = Input::get('active');
			$info['config'] = $config;
			$info['status'] = $status;
			$info['office'] = $office;
			$info['message'] = Leihatila::createMessage($confirmation, $error_messages, $warn_messages);
			return View::make('office', $info);
		}
	}
	
	public function transactions() {
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 1) {
			// Show Transactions View
			$fromDate = Input::get('fromDate');
			$toDate = Input::get('toDate');;
			
			// Get the required information
			$office = ExchangeOffice::find(Input::get('officeid'));
			if (!$office) {
				return Lang::get("messages.notallowed");
			}
			$results = NoteTransaction::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
			$users = Partner::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->where('office_id', '=', $office->id)->get();
			return View::make('notetransactions', array('office' => $office, 'results' => $results,
					'fromDate' => $fromDate, 'toDate' => $toDate, 'users' => $users));
		} else {
			return Lang::get("messages.notallowed");
		}
	}
	
	private function handleInitialQuota($user, $curr, $quota, $quotanotes, $office, &$error_messages) {
		$config = Leihatila::getSystemConfiguration();
		$status = Leihatila::getSystemStatus();
		if ($curr == 'local') {
			$notestext = $quotanotes;
			$notes = Leihatila::text2Barcodes($notestext, true);
			$notesValue = 0;
			$validNotes = array();
			for ($i=0; $i < count($notes); $i++) {
				$note = Note::select()->where('barcode', '=', $notes[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
				} else if ($note->status != 'CIRCULATING') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-circulating'), 'actual' => Lang::get('messages.status-stored')]);
				} else if (Leihatila::getExpirationCount($note, $config) > 0) {
					$error_messages[] = Lang::get('messages.expirated-note', ['number' => ''.$i]);
				} else {
					$validNotes[] = $note;
				}
			}
				
			if (count($notes) != count($validNotes)) {
				$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
			} else if (Leihatila::getTotalValue($validNotes) != $quota) {
				$error_messages[] = Lang::get('messages.invalid-expected-local-amount', ['expected' => $quota, 'actual' => ''.Leihatila::getTotalValue($validNotes)]);
			} else {
				// Deactivate Notes & Update the status of the office
				foreach($validNotes as $note) {
					$note->setToStored();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
				$office->total_local_money += $quota;
				if (!$office->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				}
				

				// Create a transaction associated to the payment
				$trans = LeihatilaDataModel::createNoteTransaction('QUOTA', 0, $quota, $user, $office);
				if (!$trans->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
				
				// Update system status
				$status->quota_collected_money += $quota * (1-$config->exchange_rate);
				if (!$status->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				}
			}
		} else {
			// Currency is official
			$office->total_official_money += $quota;
			if (!$office->save()) {
				$error_messages[] = Lang::get('messages.db-save-error');
			}
			
			// Create a transaction associated to the payment
			$trans = LeihatilaDataModel::createNoteTransaction('QUOTA', $quota, 0, $user, $office);
			if (!$trans->save()) {
				$error_messages[] = Lang::get('messages.db-save-error');
			} 
			
			// Update system status
			$status->quota_collected_money += $quota;
			if (!$status->save()) {
				$error_messages[] = Lang::get('messages.db-save-error');
			}
		}
	}
	
	private function executeExchangeOperation($op, $code, $notestext1, $notestext2, $expected_amount, $office, &$error_messages, &$warn_messages) {
		// Get the user associated to the transaction
		$config = Leihatila::getSystemConfiguration();
		$status = Leihatila::getSystemStatus();
		$user = Leihatila::getUser($code);
		if (!$user) {
			$error_messages[] = Lang::get('messages.invalid-user-barcode');
			return;
		} else if (property_exists($user, 'yearly_quota_paid_until') && !$user->isQuotaUpdated()) {
			$error_messages[] = Lang::get('messages.quota-outdated');
			return;
		}
		
		$notecodes1 = Leihatila::text2Barcodes($notestext1, true);
		$notecodes2 = Leihatila::text2Barcodes($notestext2, true);
		
		// Perform the operation
		if ($op == 'buy') {
			// Check all the note barcodes
			$validNotes = array();
			for ($i=0; $i < count($notecodes1); $i++) {
				$note = Note::select()->where('barcode', '=', $notecodes1[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
				} else if ($note->status != 'STORED') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-stored'), 'actual' => Lang::get('messages.status-'.strtolower($note->status))]);
				} else {
					$validNotes[] = $note;
				}
			}
			
			$total = Leihatila::getTotalValue($validNotes);
			if (count($notecodes1) != count($validNotes)) {
				$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
			} else if ($total != $expected_amount) {
				$error_messages[] = Lang::get('messages.invalid-expected-local-amount', ['expected' => $expected_amount, 'actual' => ''.$total]);
			} else {
				// All are valid notes and according to what was told to the user. Perform the operation.
				// 1. Change the status of the note to circulating
				$nextExpDate = null;
				foreach ($validNotes as $note) {
					$note->setToCirculating();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else if ($nextExpDate == null) {
						$nextExpDate = $note->next_expiration_date;
					}
					
				}
				
				// 2. Update the status of the exchange office
				$office->total_local_money -= $total;
				$office->total_official_money += $total;
				
				// 3. Create a transaction
				$trans = LeihatilaDataModel::createNoteTransaction('BUY', $total, -$total, $user, $office);
				if (!$trans->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
				
				// 4. Update the status of the system
				$status->exchange_collected_money += $total * (1-$config->exchange_rate);
				$status->circulating_local_money += $total;
				if (!$status->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				}
				
				if (count($error_messages) == 0) {
					$warn_messages[] = Lang::get('messages.total-local-bought', ['amount'=>$total]);
					$warn_messages[] = Lang::get('messages.notes-expiration-date', ['date' => $nextExpDate]);
				}
			}
		} else if ($op == 'sell') {
			// Check all the note barcodes
			$validNotes = array();
			for ($i=0; $i < count($notecodes1); $i++) {
				$note = Note::select()->where('barcode', '=', $notecodes1[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
				} else if ($note->status != 'CIRCULATING') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-circulating'), 'actual' => Lang::get('messages.status-'.strtolower($note->status))]);
				} else if (Leihatila::getExpirationCount($note, $config) > 0) {
					$error_messages[] = Lang::get('messages.expirated-note', ['number' => ''.$i]);
				} else {
					$validNotes[] = $note;
				}
			}
				
			$total = Leihatila::getTotalValue($validNotes);
			$official = $total * (1-$config->exchange_rate);
			if (count($notecodes1) != count($validNotes)) {
				$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
			} else if ($total != $expected_amount) {
				$error_messages[] = Lang::get('messages.invalid-expected-local-amount', ['expected' => $expected_amount, 'actual' => ''.$total]);
			} else {
				// All are valid notes and according to what was told to the user. Perform the operation.
				// 1. Change the status of the note to stored
				foreach ($validNotes as $note) {
					$note->setToStored();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
			
				// 2. Update the status of the exchange office
				$office->total_local_money += $total;
				$office->total_official_money -= $official;
			
				// 3. Create a transaction
				$trans = LeihatilaDataModel::createNoteTransaction('REDEEM', -$official, $total, $user, $office);
				if (!$trans->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
			
				// 4. Update the status of the system
				$status->circulating_local_money -= $total;
				if (!$status->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				}
				
				if (count($error_messages) == 0) {
					$warn_messages[] = Lang::get('messages.total-local-sold', ['amount'=>$total, 'official'=>$official]);
				}
			}
		} else if ($op == 'exp') {
			// Check all the note barcodes
			$validNotes = array();
			$invalidBarcodes = false;
			for ($i=0; $i < count($notecodes1); $i++) {
				$note = Note::select()->where('barcode', '=', $notecodes1[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
					$invalidBarcodes = true;
				} else if ($note->status != 'CIRCULATING') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-circulating'), 'actual' => Lang::get('messages.status-'.strtolower($note->status))]);
					$invalidBarcodes = true;
				} else if (Leihatila::getExpirationCount($note, $config) == 0) {
					$warn_messages[] = Lang::get('messages.not-expirated-note', ['number' => ''.$i]);
				} else {
					$validNotes[] = $note;
				}
			}
			
			$total = Leihatila::getTotalValue($validNotes);
			$expiration = $total * $config->expiration_rate;
			if ($invalidBarcodes) {
				$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
			} else {
				// All are valid notes and according to what was told to the user. Perform the operation.
				// 1. Update the expiration day of the notes
				$nextExpDate = null;
				foreach ($validNotes as $note) {
					$note->updateExpiration();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else if ($nextExpDate == null) {
						$nextExpDate = $note->next_expiration_date;
					}
				}
					
				// 2. Update the status of the exchange office
				$office->total_official_money += $expiration;
					
				// 3. Create a transaction
				$trans = LeihatilaDataModel::createNoteTransaction('EXPIRATION', $expiration, 0, $user, $office);
				if (!$trans->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
					
				// 4. Update the status of the system
				$status->expiration_collected_money += $expiration;
				if (!$status->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				}
			}
		} else if ($op == 'change') {
			// Check all the note barcodes
			$validNotes1 = array();
			$validNotes2 = array();
			$invalidBarcodes = false;
			for ($i=0; $i < count($notecodes1); $i++) {
				$note = Note::select()->where('barcode', '=', $notecodes1[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
					$invalidBarcodes = true;
				} else if ($note->status != 'CIRCULATING') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-circulating'), 'actual' => Lang::get('messages.status-'.strtolower($note->status))]);
					$invalidBarcodes = true;
				} else if (Leihatila::getExpirationCount($note, $config) > 0) {
					$error_messages[] = Lang::get('messages.expirated-note', ['number' => ''.$i]);
					$invalidBarcodes = true;
				} else {
					$validNotes1[] = $note;
				}
			}
			
			for ($i=0; $i < count($notecodes2); $i++) {
				$note = Note::select()->where('barcode', '=', $notecodes2[$i])->get()->first();
				if (!$note) {
					$error_messages[] = Lang::get('messages.invalid-barcode', ['number' => ''.$i]);
					$invalidBarcodes = true;
				} else if ($note->status != 'STORED') {
					$error_messages[] = Lang::get('messages.invalid-status', ['number' => ''.$i, 'expected' => Lang::get('messages.status-stored'), 'actual' => Lang::get('messages.status-'.strtolower($note->status))]);
					$invalidBarcodes = true;
				} else {
					$validNotes2[] = $note;
				}
			}
				
			$total1 = Leihatila::getTotalValue($validNotes1);
			$total2 = Leihatila::getTotalValue($validNotes2);
			if ($invalidBarcodes) {
				$error_messages[] = Lang::get('messages.invalid-barcodes-detected');
			} else if ($total1 != $total2 || $total1 != $expected_amount) {
				$error_messages[] = Lang::get('messages.different-amounts-detected');
			} else {
				// All are valid notes and according to what was told to the user. Perform the operation.
				// 1. Set changed notes to stored
				$nextExpDate = null;
				foreach ($validNotes1 as $note) {
					$note->setToStored();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} 
				}
					
				// 2. Set given noted to circulating
				foreach ($validNotes2 as $note) {
					$note->setToCirculating();
					if (!$note->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else if ($nextExpDate == null) {
						$nextExpDate = $note->next_expiration_date;
					}
				}
									
				// 3. Create a transaction
				$trans = LeihatilaDataModel::createNoteTransaction('CHANGE', 0, 0, $user, $office);
				if (!$trans->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes1)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
					if (!LeihatilaDataModel::addNotesPerTransaction($trans, $validNotes2)) {
						$error_messages[] = Lang::get('messages.db-save-error');
					}
				}
			}
		} else {
			$error_messages[] = Lang::get('messages.invalid-operation');
		}
	}

}
