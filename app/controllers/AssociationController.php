<?php

class AssociationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 4) {
			$association = Association::find(Auth::user()->related_user_id);
			$projects = Project::select()->where('association_id','=',$association->id)->get();
			$info = array('active' => 'tab1', 'association' => $association, 'projects'=>$projects);
			return View::make('association', $info);
		} else {
			return Lang::get("messages.notallowed");
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::guest() || Auth::user()->usertype != 4) {
			return Lang::get("messages.notallowed");
		}
		$action = Input::get('action');
		$active = Input::get('active');
		$association = Association::find(Auth::user()->related_user_id);
		
		// Error management variables
		$error = false;
		$error_messages = array();
		$warn_messages = array();
		$confirmation = '';
		
		switch ($action) {
			case 'tab1-updateuser-form':
				$association->name = Input::get('name');
				$association->description = Input::get('description');
				$association->responsible = Input::get('responsible');
				$association->email = Input::get('email');
				$association->address = Input::get('address');
				$association->phone = Input::get('phone');
				$association->webpage = Input::get('webpage');
				$association->id_card = Input::get('id_card');
				
				// Geocode the address using Google Maps
				$latlng = Leihatila::geocodeAddress($association->address);
				if (!$latlng) {
					$warn_messages[] = Lang::get('messages.geocoding-failed');
				} else {
					$association->latitude = $latlng['lat'];
					$association->longitude = $latlng['lng'];
				}
				
				// Validate the new association before continuing
				if (!Leihatila::validateAssociation($association, $error_messages)) {
					$info['association'] = $association;
					break;
				}
				
				if (!$association->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					$confirmation = Lang::get('messages.association-update-success');
				}
				
				break;
			case 'tab1-deleteuser-form':
				break;
			case 'tab2-updatepass-form':
				$previous = Input::get('prev_pass');
				$new1 = Input::get('new_pass1');
				$new2 = Input::get('new_pass2');
				$error = !$this->updatePasswordAction($previous, $new1, $new2, $confirmation, $error_messages, $warn_messages);
				break;
			case 'tab3-editproject-form':
				$project_id = Input::get('project_id');
				if ($project_id > 0) {
					// Edit an existing project
					$project = Project::select()->where('id','=',$project_id)->get()->first();
					if (!$project) {
						$error_messages[] = Lang::get('messages.no-such-project-error');
					} else {
						$project->name = Input::get('name');
						$project->description = Input::get('description');
						$project->status = $this->convertStatus(Input::get('status'));
						$project->requested_funding = Input::get('requested_budget');
						
						$photo = Input::file('photo');
						if ($photo) {
							// Delete previous photo (if required) & add the new one
							if ($project->image != null) {
								File::delete(__DIR__.'/../../public/'.$project->image);
							}
							
							$photo->move(__DIR__.'/../../public/storage/'.$association->barcode.'/', $photo->getClientOriginalName());
							$project->image = 'storage/'.$association->barcode.'/'.$photo->getClientOriginalName();
							if (!$project->save()) {
								File::delete(__DIR__.'/../../public/storage/'.$association->barcode.'/'.$photo->getClientOriginalName());
								$error_messages[] = Lang::get('messages.db-save-error');
							} else {
								$confirmation = Lang::get('messages.project-edit-confirm');
							}
						} else {
							if (!$project->save()) {
								$error_messages[] = Lang::get('messages.db-save-error');
							} else {
								$confirmation = Lang::get('messages.project-edit-confirm');
							}
						}
					}
				} else {
					// Create a new project
					$project = new Project();
					$project->name = Input::get('name');
					$project->description = Input::get('description');
					$project->association_id = $association->id;
					$project->status = $this->convertStatus(Input::get('status'));
					$project->requested_funding = Input::get('requested_budget');
					
					$photo = Input::file('photo');
					
					$project->image = 'storage/'.$association->barcode.'/'.$photo->getClientOriginalName();
					$photo->move(__DIR__.'/../../public/storage/'.$association->barcode.'/', $photo->getClientOriginalName());
					
					if (Leihatila::validateProject($project, $error_messages)) {
						if (!$project->save()) {
							File::delete(__DIR__.'/../../public/storage/'.$association->barcode.'/'.$photo->getClientOriginalName());
							$error_messages[] = Lang::get('messages.db-save-error');
						} else {
							$confirmation = Lang::get('messages.project-create-confirm');
						}
					}
				}
				break;
		}
		$projects = Project::select()->where('association_id','=',$association->id)->get();
		$info = array('active' => $active, 'association' => $association, 'projects'=>$projects);
		$info['message'] = Leihatila::createMessage($confirmation, $error_messages, $warn_messages);
		return View::make('association', $info);
	}

	private function updatePasswordAction($previous, $new1, $new2, &$confirmation, &$error_messages, &$warn_messages) {
		if (!Hash::check($previous, Auth::user()->password)) {
			$error_messages[] = Lang::get('messages.wrong-password');
			return false;
		}
		
		if ($new1 != $new2) {
			$error_messages[] = Lang::get('messages.different-passwords-error');
			return false;
		}
		
		$user = Auth::user();
		$user->password = Hash::make($new1);
		if (!$user->save()) {
			$error_messages[] = Lang::get('messages.db-save-error');
			return false;
		}
		
		$confirmation = Lang::get('messages.pass-change-confirm');
		return true;
	}
	
	private function convertStatus($id) {
		switch($id) {
			case '0':
				return 'DRAFT';
			case '1':
				return 'READY';
			case '2':
				return 'ACTIVE';
			case '3':
				return 'FUNDED';
		}
	}


}
