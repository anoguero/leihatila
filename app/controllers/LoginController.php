<?php

class LoginController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) {
			return $this->redirectUser();
		} else {
			return View::make('login', ['message'=>Session::get('message')]);
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function login()
	{
		// Try to authenticate
		if (Auth::attempt ( Input::only ( 'username', 'password' ) )) {
			return $this->redirectUser();
		} else {
			return Redirect::back()->withInput ()->with('message',Lang::get('messages.login-failed'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function logout()
	{
		if (Auth::guest()) {
			return Redirect::to('/login')->with('message', Lang::get('messages.not-logged'));
		}
		Auth::logout();
		$messages = array();
		return Redirect::to('/login')->with('message', Lang::get('messages.logout-success'));
	}

	
	private function redirectUser() {
		if (Auth::user ()->usertype == 0) {
			// Admin
			return Redirect::to ( '/admin' );
		} else if (Auth::user ()->usertype == 1) {
			// Office
			return Redirect::to ( '/exchangeoffice' );
		} else if (Auth::user ()->usertype == 2) {
			// Commerce
			return Redirect::to ( '/user' );
		} else if (Auth::user ()->usertype == 3) {
			// Partner
			return Redirect::to ( '/commerce' );
		} else if (Auth::user ()->usertype == 4) {
			// Association
			return Redirect::to ( '/association' );
		} else {
			return Lang::get ( 'messages.internalerror' );
		}
	}

}
