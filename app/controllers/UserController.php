<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 2) {
			$user = Partner::find(Auth::user()->related_user_id);
			$info = array('active' => 'tab1', 'user' => $user);
			return View::make('partner', $info);
		} else {
			return Lang::get("messages.notallowed");
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::guest() || Auth::user()->usertype != 2) {
			return Lang::get("messages.notallowed");
		}
		$action = Input::get('action');
		$active = Input::get('active');
		$user = Partner::find(Auth::user()->related_user_id);
		
		// Error management variables
		$error = false;
		$error_messages = array();
		$warn_messages = array();
		$confirmation = '';
		
		switch ($action) {
			case 'tab1-updateuser-form':
				$user->name = Input::get('name');
				$user->surname1 = Input::get('surname1');
				$user->surname2 = Input::get('surname2');
				$user->email = Input::get('email');
				$user->address = Input::get('address');
				$user->phone = Input::get('phone');
				$user->id_card = Input::get('id_card');
				
				if (!Leihatila::validatePartner($user, $error_messages)) {
					break;
				}
				
				if (!$user->save()) {
					$error_messages[] = Lang::get('messages.db-save-error');
				} else {
					$confirmation = Lang::get('messages.partner-update-success');
				}
				
				break;
			case 'tab1-deleteuser-form':
				break;
			case 'tab2-updatepass-form':
				$previous = Input::get('prev_pass');
				$new1 = Input::get('new_pass1');
				$new2 = Input::get('new_pass2');
				$error = !$this->updatePasswordAction($previous, $new1, $new2, $confirmation, $error_messages, $warn_messages);
				break;
			case 'tab3-updateassoc-form':
				$assoc_id = Input::get('association');
				$assoc = Association::find($assoc_id);
				if (!$assoc) {
					$error_messages[] = Lang::get('messages.invalid-association-id');
				} else {
					$user->supported_association_id = $assoc_id;
					if (!$user->save()) {
						$error_messages[] = Lang::get('messages.db-save-error');
					} else {
						$confirmation = Lang::get('messages.supported-association-update-success');
					}
				}
				break;
		}
		
		$info = array('active' => $active, 'user' => $user);
		$info['message'] = Leihatila::createMessage($confirmation, $error_messages, $warn_messages);
		return View::make('partner', $info);
	}

	private function updatePasswordAction($previous, $new1, $new2, &$confirmation, &$error_messages, &$warn_messages) {
		if (!Hash::check($previous, Auth::user()->password)) {
			$error_messages[] = Lang::get('messages.wrong-password');
			return false;
		}
		
		if ($new1 != $new2) {
			$error_messages[] = Lang::get('messages.different-passwords-error');
			return false;
		}
		
		$user = Auth::user();
		$user->password = Hash::make($new1);
		if (!$user->save()) {
			$error_messages[] = Lang::get('messages.db-save-error');
			return false;
		}
		
		$confirmation = Lang::get('messages.pass-change-confirm');
		return true;
	}

}
