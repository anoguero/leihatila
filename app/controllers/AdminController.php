<?php

class AdminController extends BaseController {

	public function transactions()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 0) {
			// Show Transactions View
			$fromDate = Input::get('fromDate');
			$toDate = Input::get('toDate');;
						
			// Get the required information
			$offices = ExchangeOffice::all();
			$results = SystemTransaction::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
			
			$fromDateObj = date_create($fromDate);
			$toDateObj = date_create($toDate);
			return View::make('transactions', array('offices' => $offices, 'results' => $results,
													'fromDate' => $fromDate, 'toDate' => $toDate,
			                                        'fromDateObj' => $fromDateObj, 'toDateObj' => $toDateObj));
		} else {
			return Lang::get("messages.notallowed");
		}
	}

	public function index()
	{
		if (Auth::guest()) {
			return Redirect::to('/login');
		} else if (Auth::user()->usertype == 0) {
			// Show Admin View
			$config = Leihatila::getSystemConfiguration();
			$status = Leihatila::getSystemStatus();
			$offices = ExchangeOffice::all();
			$info = array('active' => 'tab1','config' => $config,
					'status' => $status, 'offices' => $offices,
					'showResults' => false, 'users' => null,
					'commerces' => null, 'associations' => null);
			
			return View::make('admin', $info);
		} else {
			return Lang::get("messages.notallowed");
		}
	}
	
	public function store()
	{
		if (Auth::guest() || Auth::user()->usertype != 0) {
			return Lang::get("messages.notallowed");
		} else {
			// Responses
			$info['showResults'] = false;
			$info['users'] = array();
			$info['commerces'] = array();
			$info['associations'] = array();
			
			
			// PROCESS THE FORMS
			$action = Input::get('action');
			$error = false;
			$error_messages = array();
			$warn_messages = array();
			$confirmation = '';
			
			
			// Configuration variables
			$config = Leihatila::getSystemConfiguration();
			$status = Leihatila::getSystemStatus();
			$offices = ExchangeOffice::all();
			
			
			// System status variables
			$systemLocalMoney = $status->total_physical_local_money;
			$systemOfficialMoney = $status->total_official_money;
			$funding = 0.0;
			$reserve = 0.0;
			$circulating = 0.0;
	
			foreach ($offices as $office) {
				$systemLocalMoney += $office->total_local_money;
				$systemOfficialMoney += $office->total_official_money;
			}
			
			
			// Variables for user management
			$users = null;
			$commerces = null;
			$showResults = false;
			$queried = false;
			
			switch ($action) {
				case 'tab6-sendmail-form':
					$toPartners = Input::get('partners');
					$toCommerces = Input::get('commerces');
					$toAssociations = Input::get('associations');
					$topic = Input::get('topic');
					$text = Input::get('text');
					$this->performMailingOperation($toPartners, $toCommerces, $toAssociations, $topic, $text);
					break;
				case 'tab5-getdates-form':
					$fromDate = Input::get('fromDate');
					$toDate = Input::get('toDate');
					$users = Partner::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
					$commerces = Commerce::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
					$associations = Association::select()->where('created_at', '>=', $fromDate.' 00:00:00')->where('created_at', '<=', $toDate.' 23:59:59')->get();
					$info['showResults'] = true;
					$info['users'] = $users;
					$info['commerces'] = $commerces;
					$info['associations'] = $associations;
					break;
				case 'tab4-notesrange-form':
					$createTransaction = Input::get('createTransaction');
					$rangeStart = Input::get('rangeStart');
					$rangeEnd = Input::get('rangeEnd');
					$totalAmountCreated = 0;
					$error = $this->createNotesRange($rangeStart, $rangeEnd, $totalAmountCreated, $warn_messages, $error_messages);
					if (!$error && $createTransaction) {
						if ($totalAmountCreated > 0) {
							$this->createMoneyCreationTransaction($totalAmountCreated, $error_messages);
							$confirmation = Lang::get('adminview.money-create-success');
						} else {
							$error_messages[] = Lang::get('adminview.empty-transaction-error');
						}
					} else if (!$error) {
						$confirmation = Lang::get('adminview.money-create-success');
						$warn_messages[] = Lang::get('adminview.no-transaction-warning');
					}
					break;
				case 'tab4-newnotes-form':
					$barcodes = Input::get('tab4NewNotesText');
					$createTransaction = Input::get('createTransaction');
					$totalAmountCreated = 0;
					$error = $this->createNotesFromText($barcodes, $totalAmountCreated, $warn_messages, $error_messages);
					if (!$error && $createTransaction) {
						if ($totalAmountCreated > 0) {
							$this->createMoneyCreationTransaction($totalAmountCreated, $error_messages);
							$confirmation = Lang::get('adminview.money-create-success');
						} else {
							$error_messages[] = Lang::get('adminview.empty-transaction-error');
						}
					} else if (!$error) {
						$confirmation = Lang::get('adminview.money-create-success');
						$warn_messages[] = Lang::get('adminview.no-transaction-warning');
					}
					break;
				case 'tab3-newsystemop-form':
					$selectedOffice = Input::get('office-combo');
					$localMoney = Input::get('localMoney');
					$officialMoney = Input::get('officialMoney');
					$description = Input::get('concept');
					$error = $this->createSystemTransaction($selectedOffice, $localMoney, $officialMoney, $description, $error_messages);
					$reloadStatus = true;
					if (!$error) {
						$confirmation = Lang::get('adminview.transaction-create-success');
					}
					break;
			
				case 'tab2-changestatus-form':
					// Gather information & update
					$config->expiration_rate = Input::get('expRate');
					$config->exchange_rate = Input::get('redeemRate');
					$config->expiration_day = Input::get('expDay');
					$config->expiration_months = Input::get('expMonths');
					$config->free_expiration_days = Input::get('freeExpDays');
					if (Leihatila::validateSystemConfiguration($config)) {
						if (!$config->save()) {
							$error = true;
							$error_messages[] = Lang::get('adminview.config-change-failed');
							$config = Leihatila::getSystemConfiguration(true);
						} else {
							$confirmation = Lang::get('adminview.config-change-success');
						}
					} else {
						$error_messages[] = Lang::get('adminview.invalid-configuration');
						$config = Leihatila::getSystemConfiguration(true);
					}
					
			}
			
			$info['active'] = Input::get('active');
			$info['config'] = $config;
			$info['status'] = $status;
			$info['offices'] = $offices;
			$info['message'] = Leihatila::createMessage($confirmation, $error_messages, $warn_messages);
			return View::make('admin', $info);
		}
		
	}
	
	
	/**************** Logic and validation functions ****************/

	private function createNotesFromText($notesText, &$totalAmountCreated, &$warn_messages, &$error_messages) {
		DB::beginTransaction ();
		$barcodes = Leihatila::text2Barcodes ( $notesText );
		
		if (!Leihatila::validateNoteBarcodes($barcodes)) {
			$error_messages [] = Lang::get('messages.invalid-note-barcodes');
			return true;
		}
		
		$count = count ( $barcodes );
		$barcodes = Leihatila::removeDuplicateBarcodes($barcodes);
		if ($count != count ( $barcodes )) {
			$warn_messages [] = Lang::get('adminview.removed-duplicates-warning');
		}
		
		$newNotes = array ();
		foreach ( $barcodes as $barcode ) {
			$newNote = new Note ();
			$newNote->barcode = $barcode;
			$newNote->status = 'STORED';
			$newNote->setNoteValue ();
			
			if ($newNote->value == 0) {
				$error_messages [] = Lang::get ( 'adminview.erroneous-value', [ 
						'barcode' => $barcode 
				] );
				return true;
			}
			
			if (Note::where ( 'barcode', '=', $barcode )->count () > 0) {
				$existingNotes [] = $newNote;
				$warn_messages [] = Lang::get ( 'adminview.existing-note-barcode', [ 
						'barcode' => $barcode 
				] );
			} else {
				$newNotes [] = $newNote;
				
				if (! $newNote->save ()) {
					$error_messages [] = Lang::get ( 'adminview.error-saving-note', [ 
							'barcode' => $barcode 
					] );
					DB::rollBack ();
					return true;
				}
			}
		}
		
		$totalAmountCreated = 0;
		foreach ( $newNotes as $note ) {
			$totalAmountCreated += $note->value;
		}
		
		DB::commit ();
		return false;
	}
	
	private function createNotesRange($firstNote, $lastNote, &$totalAmountCreated, &$warnings, &$error_messages) {
		DB::beginTransaction ();
		if (! is_numeric ( $firstNote ) || ! is_numeric ( $lastNote )) {
			$error_messages [] = Lang::get ( 'adminview.non-numeric-ranges' );
			return true;
		}
		
		$newNotes = array ();
		$existingNotes = array ();
		for($i = intval ( $firstNote ); $i <= intval ( $lastNote ); $i ++) {
			$barcode = null;
			if ($i < 100000) {
				$barcode = sprintf ( "%06d", $i );
			} else {
				$barcode = sprintf ( "%d", $i );
			}
			$newNote = new Note ();
			$newNote->barcode = $barcode;
			$newNote->status = 'STORED';
			$newNote->setNoteValue ();
			
			if ($newNote->value == 0) {
				$error_messages [] = Lang::get ( 'adminview.erroneous-value', [ 
						'barcode' => $barcode 
				] );
				return true;
			}
			
			if (Note::where ( 'barcode', '=', $barcode )->count () > 0) {
				$existingNotes [] = $newNote;
				$warnings [] = Lang::get ( 'adminview.existing-note-barcode', [ 
						'barcode' => $barcode 
				] );
			} else {
				$newNotes [] = $newNote;
				
				if (! $newNote->save ()) {
					$error_messages [] = Lang::get ( 'adminview.error-saving-note', [ 
							'barcode' => $barcode 
					] );
					DB::rollBack ();
					return true;
				}
			}
		}
		
		$totalAmountCreated = 0;
		foreach ( $newNotes as $note ) {
			$totalAmountCreated += $note->value;
		}
		
		DB::commit ();
		return false;
	}
	
	private function createMoneyCreationTransaction($totalAmountCreated, &$error_messages) {
		DB::beginTransaction();
		$status = Leihatila::getSystemStatus();
		$st = new SystemTransaction();
		$st->local_money_amount = $totalAmountCreated;
		$st->official_money_amount = 0;
		$st->concept = str_replace(':created:', $totalAmountCreated, Lang::get('adminview.transaction-description'));
	
		// Create the transaction
		if (!$st->save()) {
			$error_messages[] = Lang::get('adminview.transaction-creation-error');
			DB::rollBack();
			return true;
		}
	
		// Update the staus of the system
		$status->total_physical_local_money += $totalAmountCreated;
		if (!$status->save()) {
			$error_messages[] = Lang::get('adminview.status-update-error');
			DB::rollBack();
			return true;
		}
	
		// Commit the transaction if no errors occurred
		DB::commit();
		return false;
	}
	
	private function createSystemTransaction($selectedOffice, $localMoney, $officialMoney, $description, &$error_messages) {
		// Gather information & insert operation
		DB::beginTransaction();
		$status = Leihatila::getSystemStatus();
		$st = new SystemTransaction();

		if ($selectedOffice != 'none') {
			$st->office_id = $selectedOffice;
		}
		
		$st->local_money_amount = $localMoney;
		$st->official_money_amount = $officialMoney;
		$st->concept = $description;
		
		if (! Leihatila::validateSystemTransaction ( $st )) {
			$error_messages [] = Lang::get('adminview.invalid-transaction-error');
			return true;
		} else {
			if (!$st->save()) {
				DB::rollBack();
				$error_messages [] = Lang::get('adminview.transaction-creation-error');
				return true;
			}
		}
		
		$status->total_physical_local_money += $localMoney;
		$status->total_official_money += $officialMoney;
		if (!$status->save()) {
			$error_messages [] = Lang::get('adminview.status-update-error');
			DB::rollBack();
			return true;
		}
		
		// Update the status of the affected office if required
		if ($st->office_id != null) {
			$affectedOffice = ExchangeOffice::find($st->office_id);
			if (! $affectedOffice) {
				// Should never happen!!
				$error_messages [] = Lang::get('adminview.office-find-error');
				DB::rollBack();
				return true;
			} else {
				$affectedOffice->total_local_money -= $st->local_money_amount;
				$affectedOffice->total_official_money -= $st->official_money_amount;
				if (!$affectedOffice->save()) {
					$error_messages [] = Lang::get('adminview.office-update-error');
					DB::rollBack();
					return true;
				}
			}
		}
		
		// Commit if no problems occurred
		DB::commit();
		return false;
	}
	
	private function performMailingOperation($toPartners, $toCommerces, $toAssociations, $topic, $text) {
		// Gather all the required recipients
		$tos = array();
		if ($toPartners) {
			foreach (Partner::all() as $partner) {
				if ($partner->email != null && $partner->email != '') {
					$tos[] = $partner->email;
				}
			}
		}
		
		if ($toCommerces) {
			foreach (Commerce::all() as $commerce) {
				if ($commerce->email != null && $commerce->email != '') {
					$tos[] = $commerce->email;
				}
			}
		}
		
		if ($toAssociations) {
			foreach (Association::all() as $assoc) {
				if ($assoc->email != null && $assoc->email != '') {
					$tos[] = $assoc->email;
				}
			}
		}
		
		// Send the mails
		foreach ($tos as $to) {
			Leihatila::sendEMail($to, $topic, $topic, $text);
		}
	}
	
}
