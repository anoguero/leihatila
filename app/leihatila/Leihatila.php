<?php

/**
 * A class with static functions associated to the Leihatila application
 * 
 * @author adrian
 *
 */
class Leihatila {
	
	private static $version = '2.0.0';
	private static $config = null;
	private static $status = null;
	
	
	/**
	 * Create a Leihatila user associated to a partner, commerce or association. 
	 * 
	 * @param unknown $username the username
	 * @param unknown $usertype the user type: 2 (partner), 3 (commerce) or 4 (association)
	 * @param unknown $related_user_id the id of the associated partner, commerce or association.
	 * @param string $pass (optional) a specific password
	 * @return NULL|multitype:User string an array with 'user' and 'pass' fields
	 */
	public static function createNewUser($username, $usertype, $related_user_id, $pass = null ) {
		if ($usertype != 2 && $usertype != 3 && $usertype != 4) {
			return null;
		}
		if (User::select()->where('username','=',$username)->count() > 0) {
			return null;
		}
		$user = new User();
		$user->username = $username;
		$user->usertype = $usertype;
		$user->related_user_id = $related_user_id;
		if ($pass) {
			$clear_pass = $pass;
		} else {
			// Generate a random pass
			$clear_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
			
		}
		$user->password = Hash::make($clear_pass);
		
		if (!$user->save()) {
			return null;
		} else {
			$ret = ['user' => $user, 'pass'=>$clear_pass];
			return $ret;
		}
	}
	
	/**
	 * Get the current system configuration.
	 *
	 * @param boolean $reload whether to force reloading the system configuration from the database.
	 * @return SystemConfiguration 
	 */
	public static function getSystemConfiguration($reload=false) {
		if (Leihatila::$config == null || $reload) {
			Leihatila::$config = SystemConfiguration::all()->first();
		}
		return Leihatila::$config;
	}
	
	/**
	 * Get the current system status.
	 * 
	 * @param boolean $reload whether to force reloading the system status from the database.
	 * @return SystemStatus 
	 */
	public static function getSystemStatus($reload=false) {
		if (Leihatila::$status == null || $reload) {
			Leihatila::$status = SystemStatus::all()->first();
		}
		return Leihatila::$status;
	}
	
	/**
	 * Check if an array of string represents a set of valid Note barcodes
	 * 
	 * @param array $barcodes
	 * @return boolean true if the set is valid
	 */
	public static function validateNoteBarcodes(array $barcodes) {
		foreach ( $barcodes as $barcode ) {
			if (! is_numeric ( $barcode ) || strlen ( $barcode ) != 6) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks if the provided SystemConfiguration object defines a valid configuration
	 * 
	 * @param SystemConfiguration $config
	 * @return boolean true if configuration is valid
	 */
	public static function validateSystemConfiguration(SystemConfiguration $config) {
	if ($config->exchange_rate < 0 || $config->exchange_rate >= 1 || $config->expiration_rate < 0 || $config->expiration_rate >= 1) {
		return false;
	}
	
// 	if ($config->fundingRate < 0 || $config->fundingRate > $config->redeemRate) {
// 		return false;
// 	}
	
	if ($config->free_expiration_days < 0) {
		return false;
	} 
	
	if ($config->expiration_day < 1 || $config->expiration_day > 31) {
		return false;
	}
	
	$months = explode(",", $config->expiration_months);
	foreach ($months as $m) {
		if (intval($m) < 1 || intval($m) > 12) {
			return false;
		}
		
		if ((intval($m) == 4 || intval($m) == 6 || intval($m) == 9 || intval($m) == 11) && $config->expiration_day > 30) {
			return false;
		}
		
		if (intval($m) == 2 && $config->expiration_day > 28) {
			return false;
		}
	}
	
	return true;
}
	
	/**
	 * Check is a SystemTransaction object is valid
	 * 
	 * @param SystemTransaction $st
	 * @return boolean true if valid
	 */
	public static function validateSystemTransaction(SystemTransaction $st) {
		if (!is_numeric($st->local_money_amount) || !is_numeric($st->official_money_amount)) {
			return false;
		}
		
		if (intval($st->local_money_amount) == 0 && intval($st->official_money_amount) == 0) {
			return false;
		}
		
		if ($st->office_id != null) {
			if (ExchangeOffice::find($st->office_id)->count() != 1) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Removes the duplicates from an array of string representing barcodes
	 * 
	 * @param array $barcodes
	 * @return array
	 */
	public static function removeDuplicateBarcodes(array $barcodes) {
		$res = array ();
		foreach ( $barcodes as $code ) {
			$found = false;
			foreach ( $res as $existing ) {
				if ($existing == $code) {
					$found = true;
					break;
				}
			}
			if (! $found) {
				$res [] = $code;
			}
		}
		return $res;
	}
	
	/**
	 * Extracts from a raw text string read from a textarea in Leihatila an array of barcodes
	 * 
	 * @param string $text
	 * @return array
	 */
	public static function text2Barcodes($text, $removeDuplicates = false) {
		if (!$text) {
			return null;
		}
		$string = trim ( preg_replace ( '/\s+/', ' ', $text ) );
		$noteCodes = explode ( ' ', $string );
		for($i = 0; $i < count ( $noteCodes ); $i ++) {
			$noteCodes [$i] = trim ( $noteCodes [$i] );
		}
		if ($removeDuplicates) {
			return Leihatila::removeDuplicateBarcodes($noteCodes);
		} else {
			return $noteCodes;
		}
	}
	
	/**
	 * Get the name of an exchange office given its id
	 * @param int $id
	 */
	public static function getOfficeName($id) {
		if (is_null($id)) {
			return Lang::get('messages.none');
		}
		$office = ExchangeOffice::find($id);
		return $office->name;
	}
	
	/**
	 * Get the name of a user from its barcode.
	 * @param string $barcode
	 * @return NULL|string
	 */
	public static function getUserName($barcode) {
		if (is_null($barcode) || !is_numeric($barcode) || strlen($barcode) != 10) {
			return null;
		}
		
		if ($barcode[0] == '0') {
			$user = Partner::select()->where('barcode','=',$barcode)->get()[0];
			return $user->name.' '.$user->surname1.' '.$user->surname2;
		} else if ($barcode[0] == '5') {
			$user = Association::select()->where('barcode','=',$barcode)->get()[0];
			return $user->name;
		} else if ($barcode[0] == '7') {
			$user = Commerce::select()->where('barcode','=',$barcode)->get()[0];
			return $user->name;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the name of a user from an object.
	 * @param $user the user object
	 * @return NULL|string
	 */
	public static function getUserObjectName($user) {
		if ($user->barcode[0] == '0') {
			return $user->name.' '.$user->surname1.' '.$user->surname2;
		} else if ($user->barcode[0] == '5' || $user->barcode[0] == '7') {
			return $user->name;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the user instance associated to a barcode.
	 * @param string $barcode
	 * @return NULL|Partner|Association|Commerce
	 */
	public static function getUser($barcode) {
		if (is_null($barcode) || !is_numeric($barcode) || strlen($barcode) != 10) {
			return null;
		}
	
		if ($barcode[0] == '0') {
			$user = Partner::select()->where('barcode','=',$barcode)->get()[0];
			return $user;
		} else if ($barcode[0] == '5') {
			$user = Association::select()->where('barcode','=',$barcode)->get()[0];
			return $user;
		} else if ($barcode[0] == '7') {
			$user = Commerce::select()->where('barcode','=',$barcode)->get()[0];
			return $user;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the number of expiration time slots passed since a note expired.
	 * 
	 * @param Note $expNote
	 * @param SystemConfiguration $config
	 * @return number
	 */
	public static function getExpirationCount($expNote) {
		$nextExp = $expNote->next_expiration_date;
		$expCount = 0;
		while (Leihatila::dateDiff($nextExp, date("Y-m-d")) >= 0) {
			$expCount++;
			$nextExp = Leihatila::getNextExpirationFromDate($nextExp);
		}
		return $expCount;
	}
	
	/**
	 * Get the total value of an array of Notes
	 * 
	 * @param array $notes an array of Note elements
	 * @return number
	 */
	public static function getTotalValue($notes) {
		$value = 0;
		foreach ($notes as $n) {
			$value += $n->value;
		}
		return $value;
	}
	
	public static function getNextExpirationDate() {
		$config = SystemConfiguration::all()->first();
		// Compute the expiration date
		$curMonth = date('m');
		$curDay = date('d');
		$curYear = date('Y');
		
// 		echo "Today is ", $curDay, "-",$curMonth,"-",$curYear,"<br>";
		
		$expDay = $config->expiration_day;
		//echo "Expiration months: ", $config->expirationMonths, "<br>";
		$expMonths = explode(',', $config->expiration_months);
		//echo count($expMonths),"<br>";
		$expMonth = null;
		
		foreach ($expMonths as $m) {
			if (intval($m) > intval($curMonth)) {
				if ($m < 10) {
					$expMonth = '0'.$m;
				}
				else {
					$expMonth = $m;
				}
				break;
			}
		}
		
		if ($expMonth == null) {
			$expYear = strval(intval($curYear)+1);
			if ($expMonths[0] < 10) {
				$expMonth = '0' . $expMonths[0];
			}
			else {
				$expMonth = $expMonths[0];
			}
		}
		else {
			$expYear = $curYear;
		}
		
		$expDate = $expYear . '-' . $expMonth . '-' . $expDay;
		$curDate = $curYear . '-' . $curMonth . '-' . $curDay;
		//echo "Today is: ", $curDate, " and next expiration is ", $expDate, "<br>";
		//echo "Distance between dates is ", dateDiff($curDate, $expDate), " days<br>";
		
		// Check if the distance in days is < 30. If so, switch to the next expiration date.
		if (Leihatila::dateDiff($curDate, $expDate) < 30) {
			$expDate = Leihatila::getNextExpirationFromDate($expDate, $config);
			//echo "Corrected expiration date is ", $expDate, "<br>";
		}
		
		return $expDate;
	}
	
	/**
	 * 
	 * @param string $fromDate
	 * @param SystemConfiguration $config
	 * @return string
	 */
	public static function getNextExpirationFromDate($fromDate) {
		// Compute the expiration date
		$fdexp = explode('-', $fromDate);
		$curMonth = $fdexp[1];
		$curDay = $fdexp[2];
		$curYear = $fdexp[0];
		$config = Leihatila::getSystemConfiguration();
		
		$expDay = $config->expiration_day;
		$expMonths = explode(',', $config->expiration_months);
		$expMonth = null;
		
		foreach ($expMonths as $m) {
			if (intval($m) > intval($curMonth)) {
				if ($m < 10) {
					$expMonth = '0'.$m;
				}
				else {
					$expMonth = $m;
				}
				break;
			}
		}
		
		if ($expMonth == null) {
			$expYear = strval(intval($curYear)+1);
			if ($expMonths[0] < 10) {
				$expMonth = '0' . $expMonths[0];
			}
			else {
				$expMonth = $expMonths[0];
			}
		}
		else {
			$expYear = $curYear;
		}
		
		$expDate = $expYear . '-' . $expMonth . '-' . $expDay;
		
		return $expDate;
	}
	
	/**
	 * 
	 * @param unknown $expNote
	 */
	public static function getDaysSinceExpiration($expNote) {
		return Leihatila::dateDiff($expNote->next_expiration_date, getCurrentDate());
	}
	
	/**
	 * Returns the version of the Leihatila system
	 * @return string the current version of the application
	 */
	public static function getVersion() {
		return Leihatila::$version;
	}
	
	/**
	 * Creates a message from a confirmation, warning and error message sets.
	 * 
	 * @param string $confirm a confirmation message
	 * @param array $errors an array of error messages
	 * @param array $warnings an array of warning messages
	 * @return string
	 */
	public static function createMessage($confirm, $errors, $warnings) {
		$mess = '';
		if (count($errors) > 0) {
			foreach ($errors as $e) {
				$mess .= $e.'\\n';
			}
			if (count($warnings) > 0) {
				$mess .= '----------------------\\n';
				foreach ($warnings as $w) {
					$mess .= $w.'\\n';
				}
			}
		} else {
			$mess = $confirm;
			if (count($warnings) > 0) {
				$mess .= '\\n----------------------\\n';
				foreach ($warnings as $w) {
					$mess .= $w.'\\n';
				}
			}
		}
		return $mess;
	}
	
	/**
	 * Checks whether a user barcode is valid
	 * @param $barcode the barcode string
	 * @param $userkind the user kind: partner, commerce or association
	 * @return boolean true if the barcode is valid
	 */
	public static function validateUserBarcode($barcode, $userkind) {
		if ( strlen($barcode) == 10 && is_numeric($barcode) ) {
			if (($userkind == 'partner' && $barcode[0] != '0') ||
				($userkind == 'commerce' && $barcode[0] != '7') ||
				($userkind == 'association' && $barcode[0] != 5)) {
				return false;
			} else {
				return true;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Check is a partner object is correct
	 * 
	 * @param Partner $partner the partner object
	 * @param array $error_messages the error messages array
	 * @return boolean true of ok
	 */
	public static function validatePartner($partner, &$error_messages) {
		$ok = true;
		if ($partner->name == null || strlen($partner->name) == 0) {
			$ok = false;
			$error_messages[] = Lang::get('messages.no-name-error');
		}
		if ($partner->surname1 == null || strlen($partner->surname1) == 0) {
			$ok = false;
			$error_messages[] = Lang::get('messages.no-surname-error');
		}
		if (($partner->email == null || strlen($partner->email) == 0) &&
			($partner->phone == null || strlen($partner->phone) == 0)) {
			$ok = false;
			$error_messages[] = Lang::get('messages.no-phone-email-error');
		}
		return $ok;
	}
	
	/**
	 * Check if a commerce object is correct
	 * 
	 * @param Commerce $commerce the commerce object
	 * @param array $error_messages the error messages array
	 * @return boolean true if ok
	 */
	public static function validateCommerce($commerce, &$error_messages) {
		$ok = true;
		if ($commerce->name == null || strlen($commerce->name) == 0) {
			$ok = false;
			$error_messages[] = Lang::get('messages.no-name-error');
		}
		if ($commerce->description == null || strlen($commerce->description) == 0) {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.no-description-error' );
		}
		if (($commerce->email == null || strlen ( $commerce->email ) == 0) && 
			($commerce->phone == null || strlen ( $commerce->phone ) == 0)) {
			$ok = false;
			$error_messages [] = Lang::get ( 'messages.no-phone-email-error' );
		}
		if ($commerce->responsible == null || strlen($commerce->responsible) == 0) {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.no-responsible-error' );
		}
		return $ok;
	}
	
	/**
	 * Check if an association object is correct
	 *
	 * @param Association $association the association object
	 * @param array $error_messages the error messages array
	 * @return boolean true if ok
	 */
	public static function validateAssociation($association, &$error_messages) {
		$ok = true;
		if ($association->name == null || strlen($association->name) == 0) {
			$ok = false;
			$error_messages [] = Lang::get ( 'messages.no-name-error' );
		}
		if ($association->description == null || strlen($association->description) == 0) {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.no-description-error' );
		}
		if (($association->email == null || strlen ( $association->email ) == 0) && 
			($association->phone == null || strlen ( $association->phone ) == 0)) {
			$ok = false;
			$error_messages [] = Lang::get ( 'messages.no-phone-email-error' );
		}
		if ($association->responsible == null || strlen($association->responsible) == 0) {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.no-responsible-error' );
		}
		return $ok;
	}
	
	/**
	 * Check if a project is correct.
	 * 
	 * @param Project $project the project object
	 * @param array $error_messages the array of error messages
	 * @return boolean true if ok
	 */
	public static function validateProject($project, &$error_messages) {
		$ok = true;
		if ($project->name == null || strlen($project->name) == 0) {
			$ok = false;
			$error_messages [] = Lang::get ( 'messages.no-name-error' );
		}
		
		if ($project->description == null || strlen($project->description) == 0) {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.no-description-error' );
		}
		
		if (floatval($project->requested_funding) <= 0)  {
			$ok = false;
			$error_messages[] = Lang::get ( 'messages.requested-budget-must-be-positive' );
		}
		
		return $ok;
	}
	
	/**
	 * Get the number of quotas pending for a commerce given a particular date
	 *  
	 * @param string $fromDate the date when the last quota was paid
	 * @param string $toDate (optional) current date
	 * @return number the number of quotas pending to be paid
	 */
	public static function getPendingQuotas($fromDate, $toDate = null) {
		if ($toDate == null) {
			$toDate = date('Y-m-d');
		}
		$start_ts = strtotime($fromDate);
		$end_ts = strtotime($toDate);
		$diff = $end_ts - $start_ts;
		if ($diff > 0) {
			return ceil($diff / (86400 * 365));
		} else {
			return 0;
		}
	}
	
	/**
	 * Queues an email to be sent to a recipient. The mail will use the emails.ekhi template.
	 * 
	 * @param string $to the email of the recipient.
	 * @param string $subject the email subject.
	 * @param string $title a title for the email.
	 * @param string $body the body of the email text.
	 */
	public static function sendEMail($to, $subject, $title, $body) {
		Mail::queue('emails.ekhi', array('title'=>$title, 'body'=>$body), function($message) use ($to, $subject){
			$message->to($to)->subject($subject);
		});
	}
	
	public static function sendWelcomeEMail($to, $user, $pass) {
		Mail::queue('emails.welcome', array('name'=>Leihatila::getUserObjectName($user), 'username'=>$user->barcode, 'password'=>$pass), function($message) use ($to){
			$message->to($to)->subject('Ongi etorri Ekhi Txanponara / Bienvenida a Ekhi Txanpona');
		});
	}
	
	public static function geocodeAddress($address) {
		$client = new GuzzleHttp\Client();
		//add the user entered location to the Google Maps API url query string
		$gmapsApiAdd = "http://maps.googleapis.com/maps/api/geocode/json?address=".Leihatila::preprocessAddress($address)."&sensor=false";
	
		$res = $client->get($gmapsApiAdd);
	
		//Open the Google Maps API and send it the above url containing user entered address
		//Google Maps will return a JSON file (Javascript multidimensional array)
		if($res->getStatusCode() == 200){
			//convert the json file to PHP array
			$response = json_decode($res->getBody(), true);
			//If the user entered address matched a Google Maps API address, it will return 'OK' in the status field.
			if($response["status"] == "OK"){
				//If okay, find the lat and lng values and assign them to local array
				$latLng = array("lat"=>$response["results"][0]["geometry"]["location"]["lat"],
						"lng" =>$response["results"][0]["geometry"]["location"]["lng"]);
				return $latLng;
			}
			else {
				echo 'Error: Bad request';
				return null;
			}
		}
		else{
			echo 'Error: Address not found';
			return null;
		}
	}
	
	private static function preprocessAddress($address) {
		$res = str_replace("á", "a", $address);
		$res = str_replace("Á", "A", $res);
		$res = str_replace("é", "e", $res);
		$res = str_replace("É", "E", $res);
		$res = str_replace("í", "i", $res);
		$res = str_replace("Í", "I", $res);
		$res = str_replace("ó", "o", $res);
		$res = str_replace("Ó", "O", $res);
		$res = str_replace("ú", "u", $res);
		$res = str_replace("Ú", "U", $res);
		$res = str_replace(",", "", $res);
		$res = str_replace(";", "", $res);
		$res = str_replace(".", "", $res);
		$res = str_replace(":", "", $res);
		$res = str_replace("º", "", $res);
		$res = str_replace("ª", "", $res);
	
		$res = str_replace(" ", "+", $res);
		return $res;
	}
	
	private static function dateDiff($start, $end) {
		$start_ts = strtotime($start);
		$end_ts = strtotime($end);
		$diff = $end_ts - $start_ts;
		return round($diff / 86400);
	}
	
	
	
}





class LeihatilaDataModel {

	/**
	 * Creates a new NoteTransaction from a given set of parameters.
	 *
	 * @param string $kind
	 * @param number $official
	 * @param number $local
	 * @param Eloquent $user
	 * @param ExchangeOffice $office
	 * @return NoteTransaction
	 */
	public static function createNoteTransaction($kind, $official, $local, $user, $office) {
		$trans = new NoteTransaction();
		$trans->type = $kind;
		$trans->local_money_amount = $local;
		$trans->official_money_amount = $official;
		$trans->user_barcode = $user->barcode;
		$trans->office_id = $office->id;
		return $trans;
	}
	
	/**
	 * Add all note_per_transaction entries
	 * @param NoteTransaction $trans
	 * @param array $notes
	 */
	public static function addNotesPerTransaction($trans, $notes) {
		foreach ($notes as $n) {
			$npt = new NotePerTransaction();
			$npt->transaction_id = $trans->id;
			$npt->note_id = $n->id;
			if (!$npt->save(['timestamps' => false])) {
				return false;
			}
		}
		return true;
	}

}