<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('/login');
});

Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::get('/system-transactions', 'AdminController@transactions');
Route::get('/office_transaction_details', 'OfficeController@transactions');

Route::get('/admin', 'AdminController@index');
Route::post('/admin', 'AdminController@store');
Route::get('/exchangeoffice', 'OfficeController@index');
Route::post('/exchangeoffice', 'OfficeController@store');
Route::resource('/commerce', 'CommerceController');
Route::resource('/association', 'AssociationController');
Route::resource('/user', 'UserController');

// AJAX Calls
Route::get('/getnote', 'LeihatilaController@getnote');
Route::get('/project', 'LeihatilaController@project');

Route::get('/test', 'TestController@test'); // Test controller added to try the HTML in blades

