<?php
// Prepare the variables

$menu_items = array("tab1"=>htmlentities(Lang::get('messages.account-info')),
					"tab2"=>htmlentities(Lang::get('messages.change-password')),
					"tab3"=>htmlentities(Lang::get('messages.projects')),
					"tab4"=>htmlentities(Lang::get('messages.statistics')),
);

$project_opt = array();
foreach ($projects as $project) {
	$project_opt[$project->id] = '['.$project->status.'] '.$project->name.', '.Lang::get('messages.funding-status').': '.$project->granted_funding.Lang::get('messages.local-money').'/'.$project->requested_funding.Lang::get('messages.local-money');
}

$status_opts = array('0'=>Lang::get('messages.draft'),
					 '1'=>Lang::get('messages.ready'),
					 '2'=>Lang::get('messages.active'),
);


?>

@extends('layouts.master') 

@section('title') 
{{ htmlentities(Lang::get('messages.welcome')) }} {{ Auth::user()->username }}
@stop 

@section('header') 
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}

{{ HTML::script('js/menu.js') }} 
{{ HTML::script('js/partnerview.js') }} 
{{ HTML::script('js/projects.js') }}
{{ HTML::script('js/leihatila-common.js') }}

{{ HTML::style('css/leihatila.css') }}
{{ HTML::style('css/tabbedpanel.css') }}
{{ HTML::style('css/controls.css') }}   
{{ HTML::style('css/mainpage.css') }}  

<script type="text/javascript">
<!--
	var host_name = 'http://localhost:8000/';
	var no_image_src = 'img/noimage.jpeg';
-->
</script>
@include('messages')

@stop 

@section('content')


<div id="tabbed_section" class="leihatila">
	<!-- *** MENU DIV ELEMENT *** -->
	@include('controls.tabbedpanel', ['items'=>$menu_items, 'selected'=>$active])
	<!-- *** END OF MENU DIV ELEMENT *** -->
	
	<!-- *** TAB1 VIEW *** -->
	<div id="tab1" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.account-data')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.account-data-explanation')) }}</p>
    	<hr/>
    	{{ Form::open(['id'=>'update_account_form']) }}
    	{{ Form::hidden('action', 'tab1-updateuser-form') }}
		{{ Form::hidden('active', 'tab1') }}
		<table border="" style="width: 100%;border: none;">
		<tr> 
			<td class="form-left">
				{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('name', $association->name, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('address', htmlentities(Lang::get('messages.address'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('address', $association->address, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('description', htmlentities(Lang::get('messages.description'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('description', $association->description, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('responsible', htmlentities(Lang::get('messages.responsible-person'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('responsible', $association->responsible, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('email', htmlentities(Lang::get('messages.email'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('email', $association->email, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('phone', htmlentities(Lang::get('messages.phone'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('phone', $association->phone, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('webpage', htmlentities(Lang::get('messages.webpage'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('webpage', $association->webpage, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('id_card', htmlentities(Lang::get('messages.id_card'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('id_card', $association->id_card, ['class' => 'long-text']) }}
			</td> 
		</tr>
		</table>
    	{{ Form::close() }}
    	{{ Form::open(['id'=>'delete_account_form']) }}
	    	{{ Form::hidden('action', 'tab1-updateuser-form') }}
			{{ Form::hidden('active', 'tab1') }}
		{{ Form::close() }}
    	<p style="text-align: right;padding-right: 10px;">
			{{ Form::button(Lang::get('messages.update-account'), ['class'=>'update_association_button']) }}
			<!-- {{ Form::button(Lang::get('messages.delete-account'), ['class'=>'delete_account_button']) }} -->
		</p>
    </div>
    <!-- *** END OF TAB1 VIEW *** -->
    
    <!-- *** TAB2 VIEW *** -->
    <div id="tab2" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.change-password')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.change-pass-explanation')) }}</p>
    	<hr/>
    	{{ Form::open() }}
    	{{ Form::hidden('action', 'tab2-updatepass-form') }}
		{{ Form::hidden('active', 'tab2') }}
		<table border="" style="width: 100%;border: none;">
			<tr> 
				<td class="form-left">
					{{ Form::label('prev_pass', htmlentities(Lang::get('messages.previous-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('prev_pass', ['class' => 'long-text']) }}
				</td> 
			</tr>
			<tr> 
				<td class="form-left">
					{{ Form::label('new_pass1', htmlentities(Lang::get('messages.new-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('new_pass1', ['class' => 'long-text']) }}
				</td> 
			</tr>
			<tr> 
				<td class="form-left">
					{{ Form::label('new_pass2', htmlentities(Lang::get('messages.repeat-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('new_pass2', ['class' => 'long-text']) }}
				</td> 
			</tr>
		</table>
		<p style="text-align: right;padding-right: 10px;">
		{{ Form::button(Lang::get('messages.update-password'), ['class'=>'update_pass_button']) }}
		</p>
		{{ Form::close() }}
    </div>
	<!-- *** END OF TAB2 VIEW *** -->
    
	
	<!-- *** TAB3 VIEW *** -->
    <div id="tab3" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.view-projects')) }}:</h1>
    	<div>
    	{{ Form::select('projects_combo', $project_opt, null, ['class'=>'project_list','size'=>'5']) }}
    	</div>
    	<div class='project_form_line'>
	    	<div style="width: 35%; float: right; padding-top: 5px;">
	    	{{ Form::button(Lang::get('messages.new'), ['class'=>'new_project_button']) }}
	    	{{ Form::button(Lang::get('messages.edit'), ['class'=>'edit_project_button', 'disabled'=>'disabled']) }}
	    	{{ Form::button(Lang::get('messages.delete'), ['class'=>'delete_project_button', 'disabled'=>'disabled']) }}
	    	</div>
    	</div>
    	<hr/>
    	<div class="project_form_div">
    	{{ Form::open(['id'=>'project_form', 'files'=> true]) }}
    	{{ Form::hidden('action', 'tab3-editproject-form') }}
		{{ Form::hidden('active', 'tab3') }}
		{{ Form::hidden('project_id', '-1') }}
		<div class='project_form_line'>
			<div class="project_form_left">
				{{ Form::label('name', Lang::get('messages.name')) }}
			</div>
			<div class="project_form_right">
				{{ Form::text('name') }}
			</div>
		</div>
		<div class='project_form_line'>
			<div class="project_form_half_left">
				{{ Form::label('requested_budget', Lang::get('messages.requested-budget')) }}
				{{ Form::text('requested_budget') }} 
				{{ Lang::get('messages.local-money') }}
			</div>
			<div class="project_form_half_right">
				{{ Form::label('granted_budget', Lang::get('messages.granted-budget')) }}
				{{ Form::text('granted_budget', '', ['readonly'=>'readonly']) }} 
				{{ Lang::get('messages.local-money') }}
			</div>
		</div>
		<div class='project_form_line'>
			<div class="project_form_half_left">
				{{ Form::label('photo', Lang::get('messages.photo')) }} 
				{{ Form::file('photo', ['class'=>'browse_photo_button', 'accept'=>'image/*']) }}
			</div>
			<div class="project_form_half_right">
				{{ Form::label('description', Lang::get('messages.description')) }}
			</div>
		</div>
		<div class='project_wide_form_line'>
			<div class="photo_frame project_form_half_left">
				<img id="project_photo" class="project_photo" src="">
			</div>
			<div class="project_form_half_right">
				{{ Form::textarea('description') }}
			</div>
		</div>
		<div class="project_form_line">
			<div class="project_form_left">
				{{ Form::label('status', Lang::get('messages.project-status')) }}
			</div>
			<div class="project_form_right">
				{{ Form::select('status', $status_opts, null) }}
			</div>
		</div>
		<div class="project_form_line">
			<div class="project_form_button">
				{{ Form::button('messages.save-changes', ['class'=>'save_project_button']) }}
			</div>
		</div>
    	
    	{{ Form::close() }}
    	</div>
    </div>
    
	<!-- *** END OF TAB3 VIEW *** -->
	
	<!-- *** TAB4 VIEW *** -->
    <div id="tab4" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.account-statistics')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.coming-soon')) }}</p>
    </div>
	<!-- *** END OF TAB4 VIEW *** -->
	
</div>

@stop

@section('footer')
	<div class="logout_section">
	{{ Form::open(array('action' => 'LoginController@logout', 'method' => 'get')) }}
	{{ Form::submit(Lang::get('adminview.exit')) }}
	{{ Form::close() }}
	</div>
	@if(isset($mustConfirm) && $mustConfirm)
	<script type="text/javascript"> askConfirmationMessage('{{ $message or '' }}') </script>
	@else
	<script type="text/javascript"> showMessage('{{ $message or '' }}') </script>
	@endif
@stop