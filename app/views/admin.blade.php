<?php 
// Prepare data
$options = array();
$options['none'] = Lang::get('adminview.none');
foreach ($offices as $office) {
	$options[$office->id] = $office->name;
}

$menu_items = array("tab1"=>htmlentities(Lang::get('messages.systemstatus')),
		"tab2"=>htmlentities(Lang::get('messages.changeconfig')),
		"tab3"=>htmlentities(Lang::get('messages.systemtransactions')),
		"tab4"=>htmlentities(Lang::get('messages.notemanagement')),
		"tab5"=>htmlentities(Lang::get('messages.partnermanagement')),
		"tab6"=>htmlentities(Lang::get('messages.mailing')),);

?>

@extends('layouts.master') 

@section('title') 
{{ htmlentities(Lang::get('messages.welcome')) }} {{ Auth::user()->username }}
@stop 

@section('header') 
{{ HTML::script('js/jquery.min.js') }}

{{ HTML::script('js/menu.js') }} 
{{ HTML::script('js/adminview.js') }}

{{ HTML::style('css/leihatila.css') }}
{{ HTML::style('css/tabbedpanel.css') }}
{{ HTML::style('css/mainpage.css') }} 

@include('messages')

@stop 

@section('content')

<div id="tabbed_section" class="leihatila">
	<!-- *** MENU DIV ELEMENT *** -->
	@include('controls.tabbedpanel', ['items'=>$menu_items, 'selected'=>$active])
	<!-- *** END OF MENU DIV ELEMENT *** -->


	<!-- *** TAB1 VIEW *** -->
	<div id="tab1" class="tab_content">
		{{ Form::open() }} 
		{{ Form::hidden('action', 'tab1-refreshstatus-form') }} 
		{{ Form::hidden('active', 'tab1') }}
		<h2 style="text-align: left;">
			{{ htmlentities(Lang::get('adminview.system-status')) }}</h2>
		<table border="" style="width: 100%; border: none;">
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.central-local')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('localMoney', $status->total_physical_local_money, ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.central-official')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('officialMoney', $status->total_official_money, ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
		</table>
		<hr align="center" width="90%">
		<!-- <p style="text-align: left; padding-left: 10px;">Estado del sistema:</p> -->
		<table border="" style="width: 100%; border: none;">
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.total-local')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('localMoney', '', ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.total-official')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('officialMoney', '', ['class' => 'long-text',	'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.circulating')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('circulating', '', ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.funding')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('funding', '', ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.quotas')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('reserve','', ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left" style="width: 40%">
					{{ htmlentities(Lang::get('adminview.backing')) }}
				</td>
				<td class="form-right" style="width: 60%">
					{{ Form::text('backing','', ['class' => 'long-text', 'readonly']) }}
				</td>
			</tr>
		</table>
		<p style="text-align: right; padding-right: 10px;">
			{{ Form::submit(Lang::get('adminview.refresh')) }}
		</p>
		{{ Form::close() }}
		
		<hr align="center" width="90%">
		{{ Form::open() }}
		
		<p>
			{{ htmlentities(Lang::get('adminview.show-transactions-between')) }} 
			{{ Form::text('fromDate', date('Y-m-d')) }} 
			{{ htmlentities(Lang::get('adminview.and')) }} 
			{{ Form::text('toDate',	date('Y-m-d')) }} 
			{{ Form::button(Lang::get('adminview.details'),	['class' => "show_transactions_button" ])	}}
		</p>
		{{ Form::close() }}
	</div>
	<!-- *** END OF TAB1 VIEW *** -->

	<!-- *** TAB2 VIEW *** -->
	<div id="tab2" class="tab_content">
		<h2 style="text-align: left;">
			{{ htmlentities(Lang::get('adminview.system-config')) }}
		</h2>
		{{ Form::open() }} 
		{{ Form::hidden('action', 'tab2-changestatus-form') }} 
		{{ Form::hidden('active', 'tab2') }} {{ Form::hidden('systemid','') }}
		<table border="" style="width: 100%; border: none;">
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.exchange-rate')) }}
				</td>
				<td class="form-right">
					{{ Form::text('redeemRate', $config->exchange_rate, ['class' =>	'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.expiration-rate')) }}
				</td>
				<td class="form-right">
					{{ Form::text('expRate', $config->expiration_rate, ['class' => 'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.free-days')) }}
				</td>
				<td class="form-right">
					{{ Form::text('freeExpDays', $config->free_expiration_days, ['class' => 'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.expiration-day')) }}
				</td>
				<td class="form-right">
					{{ Form::text('expDay', $config->expiration_day, ['class' =>	'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.expiration-months')) }}
				</td>
				<td class="form-right">
					{{ Form::text('expMonths', $config->expiration_months, ['class' => 'long-text']) }}
				</td>
			</tr>
		</table>

		<p style="text-align: right; padding-right: 10px;">
			{{ Form::button(Lang::get('adminview.refresh'), ['class'=>'config_refresh_button']) }} 
			{{ Form::button(Lang::get('adminview.change-config'), ['class'=>'change_config_button']) }}
		</p>
		{{ Form::close() }}
	</div>
	<!-- *** END OF TAB2 VIEW *** -->

	<!-- *** TAB3 VIEW *** -->
	<div id="tab3" class="tab_content">
		{{ Form::open() }} 
		{{ Form::hidden('action', 'tab3-newsystemop-form') }} 
		{{ Form::hidden('active', 'tab3') }}

		<p style="text-align: left">
			{{ Form::label('office-combo', htmlentities(Lang::get('adminview.select-office'))) }} 
			{{ Form::select('office-combo', $options, 'none') }}
		</p>
		<hr align="center" width="90%">
		<p style="text-align: left; font-family: sans-serif; font-size: 12">
			<b>{{ Lang::get('adminview.warning') }} </b> 
			{{ htmlentities(Lang::get('adminview.warning-message')) }}
		</p>
		<table border="" style="width: 100%; border: none;">
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.local-money')) }}
				</td>
				<td class="form-right">
					{{ Form::text('localMoney', '0.00', ['class' => 'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.official-money')) }}
				</td>
				<td class="form-right">
					{{ Form::text('officialMoney', '0.00', ['class' => 'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.description')) }}
				</td>
				<td class="form-right">
					{{ Form::text('concept', Lang::get('adminview.nodescription'), ['class' => 'long-text']) }}
				</td>
			</tr>
		</table>
		<p style="text-align: right; padding-right: 10px;">
			{{ Form::button(Lang::get('adminview.create-transaction'), ['class'=>'create_transaction_button']) }}
		</p>
		{{ Form::close() }}

	</div>
	<!-- *** END OF TAB3 VIEW *** -->


	<!-- *** TAB4 VIEW *** -->
	<div id="tab4" class="tab_content">
		<p style="text-align: left">
			{{ htmlentities(Lang::get('adminview.scan-explanation')) }}
		</p>
		{{ Form::open() }} 
			{{ Form::hidden('action', 'tab4-newnotes-form') }}
			{{ Form::hidden('active', 'tab4') }} 
			{{ Form::textarea('tab4NewNotesText', '', ['rows'=>'10','cols'=>'100']) }}
			
		<p style="text-align: right; padding-right: 10px;">
			{{ Form::checkbox('createTransaction', 'createTransaction', true) }}
			{{ Lang::get('adminview.create-transaction') }}
			<br>
			{{ Form::button(Lang::get('adminview.create-notes'),['class'=>'create_notes_button']) }}
		</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		<p style="text-align: left">
			{{ htmlentities(Lang::get('adminview.range-explanation')) }}
		</p>
		{{ Form::open() }} 
		{{ Form::hidden('action', 'tab4-notesrange-form') }} 
		{{ Form::hidden('active', 'tab4') }}
		<table border="" style="width: 100%; border: none;">
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.first-code')) }}
				</td>
				<td class="form-right">
					{{ Form::text('rangeStart', '', ['class' =>	'long-text']) }}
				</td>
			</tr>
			<tr>
				<td class="form-left">
					{{ htmlentities(Lang::get('adminview.last-code')) }}
				</td>
				<td class="form-right">
					{{ Form::text('rangeEnd', '', ['class' => 'long-text']) }}
				</td>
			</tr>
		</table>
		<p style="text-align: right; padding-right: 10px;">
			{{ Form::checkbox('createTransaction', 'createTransaction', true) }}
			{{ Lang::get('adminview.create-transaction') }}
			<br>
			{{Form::button(Lang::get('adminview.create-notes'),['class'=>'create_notes_range_button'])}}
		</p>
		{{ Form::close() }}
	</div>
	<!-- *** END OF TAB4 VIEW *** -->

	<!-- *** TAB5 VIEW *** -->
	<div id="tab5" class="tab_content" style="overflow: scroll;">
		<p>
			{{ htmlentities(Lang::get('adminview.find-users-explanation')) }}
		</p>
		<p style="font-family: sans-serif; font-style: italic; font-size: smaller;">
			{{ htmlentities(Lang::get('adminview.use-date-format')) }}
		</p>
		{{ Form::open() }}
			{{ Form::hidden('action', 'tab5-getdates-form') }}
			{{ Form::hidden('active', 'tab5') }} 
			{{ Form::label('fromDate', Lang::get('adminview.from')) }}
			{{ Form::text('fromDate', date('Y-m-d')) }}
			{{ Form::label('toDate', Lang::get('adminview.to')) }}
			{{ Form::text('toDate', date('Y-m-d')) }}
			{{ Form::submit(Lang::get('adminview.calculate')) }}
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if ((count($users) + count($commerces) + count($associations) > 0))
		<div id="tab5-results-div" style="height: 365px; overflow-y: scroll;">
		@if (count($users) > 0)
		<b>Socios y socias</b>
		<table border="1" style="width: 100%; text-align: center;">
			<tr>
				<th>{{Lang::get('messages.date-time')}}</th>
				<th>{{Lang::get('messages.partner')}}</th>
				<th>{{Lang::get('messages.barcode')}}</th>
				<th>{{Lang::get('messages.phone')}}</th>
				<th>{{Lang::get('messages.email')}}</th>
				<th>{{Lang::get('messages.office-id')}}</th>
			</tr>
			@foreach($users as $user)
			<tr>
				<td>{{$user->created_at}}</td>
				<td>{{$user->name}} {{$user->surname1}} {{$user->surname2}}</td>
				<td>{{$user->barcode}}</td>
				<td>{{$user->phone}}</td>
				<td>{{$user->email}}</td>
				<td>{{Leihatila::getOfficeName($user->office_id)}}</td>
			</tr>
			@endforeach
		</table>
		@endif
		@if (count($commerces) > 0)
		<b>Comercios</b>
		<table border="1" style="width: 100%; text-align: center;">
			<tr>
				<th>{{Lang::get('messages.date-time')}}</th>
				<th>{{Lang::get('messages.commerce')}}</th>
				<th>{{Lang::get('messages.barcode')}}</th>
				<th>{{Lang::get('messages.phone')}}</th>
				<th>{{Lang::get('messages.email')}}</th>
				<th>{{Lang::get('messages.office-id')}}</th>
			</tr>
			@foreach($commerces as $commerce)
			<tr>
				<td>{{$commerce->created_at}}</td>
				<td>{{$commerce->name}}</td>
				<td>{{$commerce->barcode}}</td>
				<td>{{$commerce->phone}}</td>
				<td>{{$commerce->email}}</td>
				<td>{{Leihatila::getOfficeName($commerce->office_id)}}</td>
			</tr>
			@endforeach
		</table>
		@endif
		@if (count($associations) > 0)
		<b>Asociaciones</b>
		<table border="1" style="width: 100%; text-align: center;">
			<tr>
				<th>{{Lang::get('messages.date-time')}}</th>
				<th>{{Lang::get('messages.association')}}</th>
				<th>{{Lang::get('messages.barcode')}}</th>
				<th>{{Lang::get('messages.phone')}}</th>
				<th>{{Lang::get('messages.email')}}</th>
				<th>{{Lang::get('messages.office-id')}}</th>
			</tr>
			@foreach($associations as $assocation)
			<tr>
				<td>{{$assocation->created_at}}</td>
				<td>{{$assocation->name}}</td>
				<td>{{$assocation->barcode}}</td>
				<td>{{$assocation->phone}}</td>
				<td>{{$assocation->email}}</td>
				<td>{{Leihatila::getOfficeName($assocation->office_id)}}</td>
			</tr>
			@endforeach
		</table>
		@endif
		</div>
		@endif
	</div>
	<!-- *** END OF TAB5 VIEW *** -->

	<!-- *** TAB6 VIEW *** -->
	<div id="tab6" class="tab_content">
  	<p style="text-align: left; padding-left: 30px;">
  		{{ htmlentities(Lang::get('adminview.email-to-explanation')) }}
  	</p>
  	{{ Form::open() }}
  	{{ Form::hidden('action', 'tab6-sendmail-form') }}
	{{ Form::hidden('active', 'tab6') }} 
  	<p style="text-align: center;">
		{{ Form::checkbox('partners', 'partners', false) }}
		{{ htmlentities(Lang::get('adminview.partners-only')) }}
		{{ Form::checkbox('commerces', 'commerces', false) }}
		{{ htmlentities(Lang::get('adminview.commerces-only')) }}
		{{ Form::checkbox('associations', 'associations', false) }}
		{{ htmlentities(Lang::get('adminview.associations-only')) }}
  	</p>
  	<hr align="center" width="90%">
  	<table style="width: 100%;border: 0px;">
  		<tr>
  			<td class="form-left" style="width: 20%;padding-left: 2%;text-align: right;">
  				{{ htmlentities(Lang::get('adminview.topic')) }}
  			</td> 
  			<td class="form-right" style="width: 80%;">
  				{{ Form::text('topic','', ['style'=>'width: 90%;']) }}
  			</td>
  		</tr>
  		<tr>
  			<td colspan="2">
  				{{ Form::textarea('text', '', ['rows'=>'25', 'cols'=>'70', 'style'=>'width: 95%;']) }}
  			</td>
  		</tr>
  	</table>
  	{{ Form::button(Lang::get('adminview.send'), ['class'=>'send_mail_button']) }}
  	
  	{{ Form::close() }}
  	</div>
	<!-- *** END OF TAB6 VIEW *** -->

	<!-- *** TAB7 VIEW *** -->
	<!-- *** END OF TAB7 VIEW *** -->
	
	
</div>

@stop

@section('footer')
	<div class="logout_section">
	{{ Form::open(array('action' => 'LoginController@logout', 'method' => 'get')) }}
	{{ Form::submit(Lang::get('adminview.exit')) }}
	{{ Form::close() }}
	</div>
	<script type="text/javascript"> showMessage('{{ $message or '' }}') </script>
@stop


