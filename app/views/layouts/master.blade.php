<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
@yield('title')
</title>
@yield('header')
</head>
<body>
@yield('content')
@yield('footer')
</body>
</html>