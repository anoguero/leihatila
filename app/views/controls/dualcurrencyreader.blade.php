<?php

?>

<div class="dual_currency_reader">
	<div class="currency_selector_div">
		{{ Form::radio('currency','official',true, ['class'=>'official_button']) }}
		{{ Lang::get('messages.official-money') }}
		{{ Form::radio('currency','local', false, ['class'=>'local_button']) }}
		{{ Lang::get('messages.local-money') }}
	</div>
	<hr/>
	<div class="official_currency_div">
		<p style="text-align: right;">
		{{ Form::text($input_name.'_official_value','',['class'=>'official_input']) }}
		{{ Lang::get('messages.official-money') }}
		</p>
	</div>
	<div class="local_currency_div">
		@include('controls.notebarcode',['input_name'=>$input_name.'_local', 'reader_kind'=>'non_expired_reader', 'enabled'=>false])
	</div>
</div>