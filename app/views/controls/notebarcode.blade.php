<?php 
// Allow only valid reader kinds
if (!isset($reader_kind) || ($reader_kind != 'expiration_reader' && $reader_kind != 'non_expired_reader')) {
	$reader_kind = 'stored_reader';
}

if (!isset($input_name)) {
	$input_name = 'notereader';
}

if (!isset($enabled)) {
	$enabled = 'true';
}
?>
<div class="barcode_control">
	{{ Form::hidden($input_name.'_barcodes', '', ['class'=>'hidden_barcodes']) }}
	{{ Form::hidden($input_name.'_value', '', ['class'=>'hidden_value']) }}
	@if($reader_kind == 'expiration_reader')
	{{ Form::hidden($input_name.'_expiration_count', '', ['class'=>'hidden_expcount expiration_reader']) }}
	@endif
	<div class="reader_div">
		@if($enabled)
		{{ Form::text('reader', '', ['class'=>'barcode_reader '.$reader_kind]) }}
		@else
		{{ Form::text('reader', '', ['class'=>'barcode_reader '.$reader_kind, 'disabled'=>'disabled']) }}
		@endif
		{{ Form::button(Lang::get('messages.remove-note'), ['class'=>'remove_button', 'disabled'=>'disabled']) }}
	</div>
	<div class="list_div">
		{{ Form::select('list', array(), null, ['class'=>'barcode_list','size'=>'10', 'multiple'=>'multiple']) }}
	</div>
	<div class="total_div">
		@if($reader_kind == 'expiration_reader')
		<span>{{ Lang::get('messages.expiration-total') }}:</span> <span class="total_span {{ $reader_kind }}">0</span><span> {{ Lang::get('messages.official-money') }}</span>
		@else
		<span>{{ Lang::get('messages.total') }}:</span> <span class="total_span {{ $reader_kind }}">0</span><span> {{ Lang::get('messages.local-money') }}</span>
		@endif
	</div>
</div>
