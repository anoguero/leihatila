<?php

?>


<script type="text/javascript">
<!--
var tabbedmenu_active = '{{$selected}}';
//-->
</script>
<!-- *** MENU DIV ELEMENT *** -->
<div class="tab_menu">
	<ul class="tab_list">
		@foreach($items as $key => $value)
			@if($key == $selected)
			<li class="active_tab_item" id="{{ $key }}">
			@else
			<li class="tab_item" id="{{ $key }}">
			@endif
				<span class="tab_name_span">{{ $value }}</span>
			</li>
		@endforeach
	</ul>
</div>
