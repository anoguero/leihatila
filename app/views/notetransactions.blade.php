
@extends('layouts.master')

@section('title')
{{ htmlentities(Lang::get('messages.office-transactions')) }} {{ Lang::get('messages.from') }} {{ $fromDate }} {{ Lang::get('messages.to') }} {{ $toDate }}
@stop

@section('content')

@if(count($results) > 0 || count($users) > 0)

	@if (count($results) > 0)
	<p>{{ Lang::get('messages.transactions-between') }} {{$fromDate}} {{ Lang::get('messages.and') }} {{$toDate}}:</p>
	<table border="1" style="width: 100%;text-align: center;">
	<tr> <th>{{Lang::get('messages.date-time')}}</th> <th>{{ Lang::get('messages.transaction-kind') }}</th> <th>{{Lang::get('messages.official-money')}}</th> <th>{{Lang::get('messages.local-money')}}</th></tr>
	@foreach($results as $st)
	<tr>
		<td>{{ $st->created_at }}</td>
		@if($st->type == 'BUY')
		<td>{{ Lang::get('messages.buy') }}</td>
		@elseif($st->type == 'REDEEM')
		<td>{{ Lang::get('messages.redeem') }}</td>
		@elseif($st->type == 'EXPIRATION')
		<td>{{ Lang::get('messages.expiration') }}</td>
		@elseif($st->type == 'QUOTA')
		<td>{{ Lang::get('messages.quota') }}</td>
		@elseif($st->type == 'CHANGE')
		<td>{{ Lang::get('messages.change') }}</td>
		@else
		<td>{{ Lang::get('messages.unknown') }}</td>
		@endif
		<td><?php if ($st->official_money_amount > 0) {echo '+';} echo $st->official_money_amount; ?></td>
		<td><?php if ($st->local_money_amount > 0) {echo '+';} echo $st->local_money_amount; ?></td> 
	</tr>
	@endforeach
	</table>
	@endif
	
	@if (count($users) > 0)
	<p>{{ Lang::get('messages.users-between') }} {{$fromDate}} {{ Lang::get('messages.and') }} {{$toDate}}:</p>
	<table border="1" style="width: 100%;text-align: center;">
	<tr> <th>{{Lang::get('messages.date-time')}}</th> <th>{{ Lang::get('messages.transaction-kind') }}</th> <th>{{Lang::get('messages.official-money')}}</th> <th>{{Lang::get('messages.official-money')}}</th></tr>
	@endif
@else
	{{ Lang::get('adminview.no-transactions-to-show') }}
@endif

@stop