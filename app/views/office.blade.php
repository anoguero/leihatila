<?php
// Prepare the variables
if (!isset($lastFromDate)) {
	$lastFromDate = date('Y-m-d');
}

if (!isset($lastToDate)) {
	$lastToDate = date('Y-m-d');
}

if (isset($commerce)) {
	$pendingQuotas = Leihatila::getPendingQuotas($commerce->yearly_quota_paid_until);
}

$associations = Association::all();

$associations_opt = array();
$associations_opt[0] = '----------------------------';
foreach ($associations as $assoc) {
	$associations_opt[$assoc->id] = $assoc->name;
}

if (isset($user) && $user->supported_association_id != null)  {
	$selected_assoc = $user->supported_association_id;
} else if (isset($commerce) && $commerce->supported_association_id != null) {
	$selected_assoc = $commerce->supported_association_id;
} else if (isset($association) && $association->supported_association_id != null) {
	$selected_assoc = $association->supported_association_id;
} else {
	$selected_assoc = 0;
}

$operations = [
		'buy' => htmlentities(Lang::get('messages.buy-local-money')),
		'sell' => htmlentities(Lang::get('messages.redeem-local-money')),
		'exp' => htmlentities(Lang::get('messages.pay-expiration')),
		'change' => htmlentities(Lang::get('messages.change-local-money')),
];

$menu_items = array("tab1"=>htmlentities(Lang::get('messages.officestatus')),
		            "tab2"=>htmlentities(Lang::get('messages.partners')),
		            "tab3"=>htmlentities(Lang::get('messages.commerces')),
		            "tab4"=>htmlentities(Lang::get('messages.associations')),
		            "tab5"=>htmlentities(Lang::get('messages.operations')),
		            "tab6"=>htmlentities(Lang::get('messages.commerce-quotas')),
					"tab7"=>htmlentities(Lang::get('messages.accounting')),);


?>

@extends('layouts.master') 

@section('title') 
{{ htmlentities(Lang::get('messages.welcome')) }} {{ Auth::user()->username }}
@stop 

@section('header') 
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}

{{ HTML::script('js/menu.js') }} 
{{ HTML::script('js/controls.js') }} 
{{ HTML::script('js/officeview.js') }}

{{ HTML::style('css/leihatila.css') }}
{{ HTML::style('css/tabbedpanel.css') }}
{{ HTML::style('css/controls.css') }} 
{{ HTML::style('css/mainpage.css') }}  
@include('messages')

@stop 

@section('content')


<div id="tabbed_section" class="leihatila">
	<!-- *** MENU DIV ELEMENT *** -->
	@include('controls.tabbedpanel', ['items'=>$menu_items, 'selected'=>$active])
	<!-- *** END OF MENU DIV ELEMENT *** -->
	
	<!-- *** TAB1 VIEW *** -->
	<div id="tab1" class="tab_content">
    	<p style="text-align: center"><b>{{ htmlentities(Lang::get('messages.welcome-to-leihatila')) }} v{{ Leihatila::getVersion() }}</b></p>
    	<p style="text-align: center">{{ htmlentities(Lang::get('messages.leihatila-description')) }}</p>
    	<hr align="center" width="90%">
    	<table border="" style="width: 100%;border: none;">
    		<tr> <td class="form-left">{{ htmlentities(Lang::get('messages.connected-as')) }}</td> <td class="form-right"> {{ $office->name }}</td></tr>
    		<tr> <td class="form-left"></td> <td class="form-right"> {{ $office->address }}</td></tr>
    	</table>
    	<hr align="center" width="90%">
    	<table border="" style="width: 100%;border: none;">
    		<tr> <td class="form-left">{{ htmlentities(Lang::get('messages.official-money')) }}:</td> <td class="form-right"> {{ $office->total_official_money }}</td></tr>
    		<tr> <td class="form-left">{{ htmlentities(Lang::get('messages.local-money')) }}:</td> <td class="form-right"> {{ $office->total_local_money }}</td></tr>
    	</table>
    </div>
    <!-- *** END OF TAB1 VIEW *** -->
    
    <!-- *** TAB2 VIEW *** -->
    <div id="tab2" class="tab_content">
    	{{ Form::open() }}
    		{{ Form::hidden('action', 'tab2-getuser-form') }}
			{{ Form::hidden('active', 'tab2') }}
			<p>
				{{ Form::label('userbarcode', htmlentities(Lang::get('messages.insert-barcode'))) }}
				@if($user != null)
					{{ Form::text('userbarcode', $user->barcode) }}
				@else
					{{ Form::text('userbarcode') }}
				@endif
				{{ Form::submit(htmlentities(Lang::get('messages.submit'))) }}
			</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if($user != null)
			{{ Form::open(['onsubmit' => 'return confirmPartnerCreation(this);']) }}
				{{ Form::hidden('action', 'tab2-setuser-form') }}
				{{ Form::hidden('active', 'tab2') }}
				{{ Form::hidden('barcode', $user->barcode) }}
				@if ($user->created_at == null)
					{{ Form::hidden('create', true) }}
				@else
					{{ Form::hidden('create', false) }}
				@endif
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('name', $user->name, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('surname1', htmlentities(Lang::get('messages.surname1'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('surname1', $user->surname1, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('surname2', htmlentities(Lang::get('messages.surname2'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('surname2', $user->surname2, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('address', htmlentities(Lang::get('messages.address'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('address', $user->address, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('email', htmlentities(Lang::get('messages.email'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('email', $user->email, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('phone', htmlentities(Lang::get('messages.phone'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('phone', $user->phone, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('id_card', htmlentities(Lang::get('messages.id_card'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('id_card', $user->id_card, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr>
					<td class="form-left">
						{{ Form::label('association', htmlentities(Lang::get('messages.view-association'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::select('association', $associations_opt, $selected_assoc, ['class' => 'assoc_combo long-text']) }}
					</td> 
				</tr>
				</table>
				@if($user->created_at == null) 
				<hr align="center" width="90%">
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('initial_quota', htmlentities(Lang::get('messages.initial_quota'))) }}
					</td> 
					<td class="form-right"> 
						@include('controls.dualcurrencyreader', ['input_name'=>'quota'])
					</td> 
				</tr>
				@endif
				</table>
				<p style="text-align: right;padding-right: 10px;">
					@if($user->created_at == null)
						{{ Form::submit(htmlentities(Lang::get('messages.create-user'))) }}
					@else
						{{ Form::submit(htmlentities(Lang::get('messages.update-user'))) }}
					@endif
				</p>
			{{ Form::close() }}
		@endif
    </div>
    
	<!-- *** END OF TAB2 VIEW *** -->
	
	<!-- *** TAB3 VIEW *** -->
    <div id="tab3" class="tab_content">
    	{{ Form::open() }}
    		{{ Form::hidden('action', 'tab3-getuser-form') }}
			{{ Form::hidden('active', 'tab3') }}
			<p>
				{{ Form::label('userbarcode', htmlentities(Lang::get('messages.insert-barcode'))) }}
				@if($commerce != null)
					{{ Form::text('userbarcode', $commerce->barcode) }}
				@else
					{{ Form::text('userbarcode') }}
				@endif
				{{ Form::submit(htmlentities(Lang::get('messages.submit'))) }}
			</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if($commerce != null)
			{{ Form::open() }}
				{{ Form::hidden('action', 'tab3-setuser-form') }}
				{{ Form::hidden('active', 'tab3') }}
				{{ Form::hidden('barcode', $commerce->barcode) }}
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('name', $commerce->name, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('address', htmlentities(Lang::get('messages.address'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('address', $commerce->address, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('description', htmlentities(Lang::get('messages.description'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('description', $commerce->description, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('responsible', htmlentities(Lang::get('messages.responsible-person'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('responsible', $commerce->responsible, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('email', htmlentities(Lang::get('messages.email'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('email', $commerce->email, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('phone', htmlentities(Lang::get('messages.phone'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('phone', $commerce->phone, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('webpage', htmlentities(Lang::get('messages.webpage'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('webpage', $commerce->webpage, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('id_card', htmlentities(Lang::get('messages.id_card'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('id_card', $commerce->id_card, ['class' => 'long-text']) }}
					</td>
				</tr>
				<tr>
					<td class="form-left">
						{{ Form::label('association', htmlentities(Lang::get('messages.view-association'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::select('association', $associations_opt, $selected_assoc, ['class' => 'assoc_combo long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('quota_updated', htmlentities(Lang::get('messages.last-quota-paid'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('quota_updated', $commerce->yearly_quota_paid_until, ['class' => 'long-text', 'readonly']) }}
					</td>  
				</tr>
				</table>
				<p style="text-align: right;padding-right: 10px;">
					@if($commerce->created_at == null)
						{{ Form::submit(htmlentities(Lang::get('messages.create-commerce'))) }}
					@else
						{{ Form::submit(htmlentities(Lang::get('messages.update-commerce'))) }}
					@endif
				</p>
			{{ Form::close() }}
		@endif
    </div>
    
	<!-- *** END OF TAB3 VIEW *** -->
	
	<!-- *** TAB4 VIEW *** -->
    <div id="tab4" class="tab_content">
    	{{ Form::open() }}
    		{{ Form::hidden('action', 'tab4-getuser-form') }}
			{{ Form::hidden('active', 'tab4') }}
			<p>
				{{ Form::label('userbarcode', htmlentities(Lang::get('messages.insert-barcode'))) }}
				@if($association != null)
					{{ Form::text('userbarcode', $association->barcode) }}
				@else
					{{ Form::text('userbarcode') }}
				@endif
				{{ Form::submit(htmlentities(Lang::get('messages.submit'))) }}
			</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if($association != null)
			{{ Form::open() }}
				{{ Form::hidden('action', 'tab4-setuser-form') }}
				{{ Form::hidden('active', 'tab4') }}
				{{ Form::hidden('barcode', $association->barcode) }}
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('name', $association->name, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('address', htmlentities(Lang::get('messages.address'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('address', $association->address, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('description', htmlentities(Lang::get('messages.description'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('description', $association->description, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('responsible', htmlentities(Lang::get('messages.responsible-person'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('responsible', $association->responsible, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('email', htmlentities(Lang::get('messages.email'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('email', $association->email, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('phone', htmlentities(Lang::get('messages.phone'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('phone', $association->phone, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('webpage', htmlentities(Lang::get('messages.webpage'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('webpage', $association->webpage, ['class' => 'long-text']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('id_card', htmlentities(Lang::get('messages.id_card'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('id_card', $association->id_card, ['class' => 'long-text']) }}
					</td> 
				</tr>
				</table>
				@if($association->created_at == null) 
				<hr align="center" width="90%">
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('initial_quota', htmlentities(Lang::get('messages.initial_quota'))) }}
					</td> 
					<td class="form-right"> 
						@include('controls.dualcurrencyreader', ['input_name'=>'quota'])
					</td>
				</tr>
				@endif
				</table>
				<p style="text-align: right;padding-right: 10px;">
					@if($association->created_at == null)
						{{ Form::submit(htmlentities(Lang::get('messages.create-association'))) }}
					@else
						{{ Form::submit(htmlentities(Lang::get('messages.update-association'))) }}
					@endif
				</p>
			{{ Form::close() }}
		@endif
    </div>
    
	<!-- *** END OF TAB4 VIEW *** -->
	
	<!-- *** TAB5 VIEW *** -->
	<div id="tab5" class="tab_content">
		{{ Form::open() }}
			{{ Form::hidden('action', 'tab5-user-operation-form') }}
			{{ Form::hidden('active', 'tab5') }}
			<p>
			{{ Form::label('barcode', htmlentities(Lang::get('messages.insert-barcode')).':') }}
			@if(isset($opSel))
				{{ Form::text('userbarcode', $opSel['barcode']) }}
				{{ Form::hidden('last', $opSel['operation']) }}
				{{ Form::label('operation', htmlentities(Lang::get('messages.select-operation')).':') }}
				{{ Form::select('operation', $operations, $opSel['operation'], array('onchange' => 'testSelectChange(this.form);')) }}
			@else
				{{ Form::text('userbarcode') }}
				{{ Form::hidden('last', '') }}
				{{ Form::label('operation', htmlentities(Lang::get('messages.select-operation'))) }}
				{{ Form::select('operation', $operations, 'buy', array('onchange' => 'testSelectChange(this.form);')) }}
			@endif
			{{ Form::submit(htmlentities(Lang::get('messages.submit'))) }}
			</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if(isset($opSel) && $opSel['user'])
			{{ Form::open() }}
				{{ Form::hidden('action', 'tab5-perform-operation-form') }}
				{{ Form::hidden('active', 'tab5') }}
				{{ Form::hidden('operation', $opSel['operation']) }}
				{{ Form::hidden('barcode', $opSel['barcode']) }}
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('name', $opSel['name'], ['class' => 'long-text']) }}
					</td> 
				</tr>
				</table>
				@if($opSel['operation'] != 'change')
				<table style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('notes', htmlentities(Lang::get('messages.notes-to-'.$opSel['operation']))) }}
					</td> 
					<td class="form-right" style="text-align: right;"> 
						@if($opSel['operation'] == 'buy')
						@include('controls.notebarcode',['input_name'=>'notes','reader_kind'=>'stored_reader'])
						@elseif($opSel['operation'] == 'sell')
						@include('controls.notebarcode',['input_name'=>'notes','reader_kind'=>'non_expired_reader'])
						@elseif($opSel['operation'] == 'exp')
						@include('controls.notebarcode',['input_name'=>'notes','reader_kind'=>'expiration_reader'])
						@endif
					</td> 
				</tr>
				</table>
				@else
				<table style="width: 100%;border: none;">
				<tr>
					<td style="width: 50%;text-align: left;">
						{{ Form::label('received-notes', htmlentities(Lang::get('messages.received-notes'))) }}
					</td>
					<td style="width: 50%;text-align: left;">
						{{ Form::label('given-notes', htmlentities(Lang::get('messages.given-notes'))) }}
					</td>
				</tr>
				<tr>
					<td style="width: 50%;text-align: center;">
						@include('controls.notebarcode',['input_name'=>'received_notes','reader_kind'=>'non_expired_reader'])
					</td>
					<td style="width: 50%;text-align: center;">
						@include('controls.notebarcode',['input_name'=>'given_notes','reader_kind'=>'stored_reader'])
					</td>
				</tr>
				</table>
				@endif
				
				<div>
					{{ Form::button(Lang::get('messages.proceed'), ['class'=>'operation_proceed_button']) }}
				</div>
			{{ Form::close() }}
		@endif
	</div>
	<!-- *** END OF TAB5 VIEW *** -->
	
	<!-- *** TAB6 VIEW *** -->
    <div id="tab6" class="tab_content">
    	{{ Form::open() }}
    		{{ Form::hidden('action', 'tab6-getuser-form') }}
			{{ Form::hidden('active', 'tab6') }}
			<p>
				{{ Form::label('userbarcode', htmlentities(Lang::get('messages.insert-barcode'))) }}
				@if($commerce != null)
					{{ Form::text('userbarcode', $commerce->barcode) }}
				@else
					{{ Form::text('userbarcode') }}
				@endif
				{{ Form::submit(htmlentities(Lang::get('messages.submit'))) }}
			</p>
		{{ Form::close() }}
		<hr align="center" width="90%">
		@if($commerce != null && $commerce->created_at != null)
			{{ Form::open() }}
				{{ Form::hidden('action', 'tab6-pay-quota-form') }}
				{{ Form::hidden('active', 'tab6') }}
				{{ Form::hidden('barcode', $commerce->barcode) }}
				<table border="" style="width: 100%;border: none;">
				<tr> 
					<td class="form-left">
						{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('name', $commerce->name, ['class' => 'long-text', 'readonly']) }}
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('quota_updated', htmlentities(Lang::get('messages.last-quota-paid'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('quota_updated', $commerce->yearly_quota_paid_until, ['class' => 'long-text', 'readonly']) }}
					</td>  
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('pending_quotas', htmlentities(Lang::get('messages.pending-quotas'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::text('pending_quotas', $pendingQuotas, ['class' => 'long-text', 'readonly']) }}
					</td> 
				</tr>
				@if ($pendingQuotas > 0)
				<tr> 
					<td class="form-left">
						{{ Form::label('to_be_paid', htmlentities(Lang::get('messages.quota-to-be-paid'))) }}
					</td> 
					<td class="form-right"> 
						{{ Form::hidden('to_be_paid', $pendingQuotas * 20) }}
						{{ Form::hidden('pending_quotas', $pendingQuotas) }}
						<span>{{ $pendingQuotas * 20 }}</span>
						<span>{{ Lang::get('messages.local-money').' / '.Lang::get('messages.official-money') }}</span>
					</td> 
				</tr>
				<tr> 
					<td class="form-left">
						{{ Form::label('paid_local', htmlentities(Lang::get('messages.note-barcodes'))) }}
					</td> 
					<td class="form-right"> 
						@include('controls.dualcurrencyreader', ['input_name'=>'quota'])
					</td> 
				</tr>
				@endif
				</table>
				@if($pendingQuotas > 0)
				<p style="text-align: right;padding-right: 10px;">
					{{ Form::button(Lang::get('messages.pay-quota'), ['class'=>'pay_quota_button']) }}
				</p>
				@endif
			{{ Form::close() }}
		@elseif($commerce != null)
		<p>{{ Lang::get('messages.commerce-does-not-exist') }}</p>
		@endif
	</div>
	<!-- *** END OF TAB6 VIEW *** -->
	
	<!-- *** TAB7 VIEW *** -->
	<div id="tab7" class="tab_content">
    <p>{{ Lang::get('messages.calculate-office-transactions') }}</p>
    <p style="font-family: sans-serif;font-style: italic;font-size: smaller;">({{ Lang::get('messages.use-date-format') }})</p>
    {{ Form::open() }}
    	{{ Form::hidden('action', 'tab7-getdates-form') }}
		{{ Form::hidden('active', 'tab7') }}
		{{ Form::label('fromDate', Lang::get('messages.from')) }}
		{{ Form::text('fromDate', $lastFromDate) }}
		{{ Form::label('toDate', Lang::get('messages.to')) }}
		{{ Form::text('toDate', $lastToDate) }}
		{{ Form::submit(Lang::get('messages.calculate')) }}
    {{ Form::close() }}
    
    <hr align="center" width="90%">
    @if (isset($officialMoneyStatus) && isset($localMoneyStatus))
    	<table border="" style="width: 100%;border: none;">
			<tr> 
				<td class="form-left">{{ Lang::get('messages.official-money') }}:</td> 
				<td class="form-right"> {{ Form::text('official-money', $officialMoneyStatus, ['class' => 'long-text', 'readonly']) }}</td> 
			</tr>
			<tr> 
				<td class="form-left">{{ Lang::get('messages.local-money') }}:</td> 
				<td class="form-right"> {{ Form::text('local-money', $localMoneyStatus, ['class' => 'long-text', 'readonly']) }} </td> </tr>
		</table>
		<p style="text-align: right;padding-right: 10px;">
			{{ Form::button(Lang::get('messages.show-details'), ['onclick' => 'showResultsDetails(\'office_transaction_details?officeid='.$office->id.'&fromDate='.$lastFromDate.'&toDate='.$lastToDate.'\')']) }}
		</p>
    @endif
    </div>
	<!-- *** END OF TAB7 VIEW *** -->
	
	
</div>

@stop

@section('footer')
	<div class="logout_section">
	{{ Form::open(array('action' => 'LoginController@logout', 'method' => 'get')) }}
	{{ Form::submit(Lang::get('adminview.exit')) }}
	{{ Form::close() }}
	</div>
	@if(isset($mustConfirm) && $mustConfirm)
	<script type="text/javascript"> askConfirmationMessage('{{ $message or '' }}') </script>
	@else
	<script type="text/javascript"> showMessage('{{ $message or '' }}') </script>
	@endif
@stop