<?php
?>
<script type="text/javascript">
<!--
	// Note Barcode Control Messages
	var note_not_in_the_system_message = "{{ Lang::get('messages.not-in-system') }}";
	var note_not_stored_message = "{{ Lang::get('messages.not-stored') }}";
	var note_expired_message = "{{ Lang::get('messages.expired') }}";
	var note_not_circulating_message = "{{ Lang::get('messages.not-circulating') }}";
	var note_not_expired_message = "{{ Lang::get('messages.not-expired') }}";
	var note_repeated_note = "{{ Lang::get('messages.repeated-note-barcode') }}";
	var config_expiration_rate = {{ Leihatila::getSystemConfiguration()->expiration_rate }};
	var config_exchange_rate = {{ Leihatila::getSystemConfiguration()->exchange_rate }};
	var invalid_barcode = "{{ Lang::get('messages.invalid-barcode') }}";

	// Office view messages
	var office_confirm_partner_create_official_message = "{{ Lang::get('messages.confirm-partner-create-official') }}";
	var office_confirm_partner_create_local_message = "{{ Lang::get('messages.confirm-partner-create-local') }}";
	var office_lower_quota_warning = "{{ Lang::get('messages.lower-quota-warning') }}";
	var office_confirm_partner_update = "{{ Lang::get('messages.confirm-partner-update') }}";
	var office_missing_fields_error_message = "{{ Lang::get('messages.missing-fields-error') }}";
	var office_name_field = "{{ Lang::get('messages.name') }}";
	var office_surname1_field = "{{ Lang::get('messages.surname1') }}";
	var office_phone_or_email_field = "{{ Lang::get('messages.phone-or-email') }}";
	var office_supported_association_field = "{{ Lang::get('messages.supported-association') }}";
	var office_about_2_buy = "{{ Lang::get('messages.about-to-buy') }}";
	var office_about_2_sell = "{{ Lang::get('messages.about-to-sell') }}";
	var office_about_2_exp = "{{ Lang::get('messages.about-to-exp') }}";
	var office_about_2_change = "{{ Lang::get('messages.about-to-change') }}";
	var office_are_you_sure = "{{ Lang::get('messages.are-you-sure') }}";
	var office_different_change_values = "{{ Lang::get('messages.different-change-values-error') }}";
	var office_for = "{{ Lang::get('messages.for') }}";
	var office_quota_confirm = "{{ Lang::get('messages.quota-confirm') }}";
	var office_insufficient_quota = "{{ Lang::get('messages.insufficient-quota') }}";

	// Common messages
	var local_money = "{{ Lang::get('messages.local-money') }}";
	var official_money = "{{ Lang::get('messages.official-money') }}";
	
	// Admin view message
	var admin_config_confirm_message = "{{ Lang::get('adminview.config-change-confirm') }}";
	var admin_config_change_warning_message = "{{ Lang::get('adminview.config-change-warning') }}";
	var admin_transaction_create_confirm_message = "{{ Lang::get('adminview.create-transaction-confirm') }}";
	var admin_notesrange_create_confirm = "{{ Lang::get('adminview.create-notes-range-confirm') }}";
	var admin_notes_create_confirm = "{{ Lang::get('adminview.create-notes-confirm') }}";
	var admin_invalid_note_barcode = "{{ Lang::get('adminview.invalid-note-barcode') }}"; // "Los códigos introducidos no son código válidos de billetes. Deben tener 6 cifras."
	var admin_range_start_before_end = "{{ Lang::get('adminview.range-start-before-end') }}"; // "El código de inicio del rango debe ser menor que el código de final del rango."
	var admin_erroneous_barcodes = "{{ Lang::get('adminview.erroneous-barcodes') }}";
	var admin_send_email_confirm = "{{ Lang::get('adminview.send-email-confirm') }}";

	// Common messages
	var different_passwords_error_message = "{{ Lang::get('messages.different-passwords-error') }}";
	var missing_prev_password_error_message = "{{ Lang::get('messages.missing-prev-password-error') }}";
	var missing_new_password_error_message = "{{ Lang::get('messages.missing-new-password-error') }}";
	var user_incomplete_error_message = "{{ Lang::get('messages.user-incomplete-error') }}";
	var confirm_account_update_message = "{{ Lang::get('messages.confirm-account-update') }}";
	var change_supported_assoc_confirm = "{{ Lang::get('messages.change-supported-assoc-confirm') }}";
	var confirm_password_change_message = "{{ Lang::get('messages.confirm-password-update') }}";

	// Project messages
	var no_such_project_error = "{{ Lang::get('messages.no-such-project-error') }}";
//-->
</script>