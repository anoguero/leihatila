<?php 

$menu_items = array("tab1"=>"Tab 1","tab2"=>"Tab 2","tab3"=>"Tab 3","tab4"=>"Tab 4","tab5"=>"Tab 5","tab6"=>"Tab 6",);
$sel = "tab2";
?>

@extends('layouts.master') 

@section('title') 
Test 2
@stop 

@section('header') 
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}
{{ HTML::style('css/leihatila.css') }}
{{ HTML::style('css/tabbedpanel.css') }} 
{{ HTML::script('js/menu.js') }} 
@stop 

@section('content')
<div class="leihatila">
	<h1>This is test 2</h1>
	<hr/>
	@include('controls.tabbedpanel', ['items'=>$menu_items, 'selected'=>$sel])
	<div id="tab1" class="tab_content">
		<p>This is tab1</p>
	</div>
	<div id="tab2" class="tab_content">
		<p>This is tab2</p>
	</div>
	<div id="tab3" class="tab_content">
		<p>This is tab3</p>
	</div>
	<div id="tab4" class="tab_content">
		<p>This is tab4</p>
	</div>
	<div id="tab5" class="tab_content">
		<p>This is tab5</p>
	</div>
	<div id="tab6" class="tab_content">
		<p>This is tab6</p>
	</div>
	
</div>
@stop

@section('footer')

@stop