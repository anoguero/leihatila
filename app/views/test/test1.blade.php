@extends('layouts.master') 

@section('title') 
Test 1
@stop 

@section('header') 
{{ HTML::script('js/jquery.min.js') }}
{{ HTML::script('js/controls.js') }} 
{{ HTML::style('css/mainpage.css') }} 
{{ HTML::style('css/controls.css') }} 
{{ HTML::script('js/officeview.js') }} 
@include('messages')
@stop 

@section('content')
<div>
	<h1>This is test 1</h1>
	<hr/>

	{{ Form::open(['method'=>'get']) }}
	<div style="background-color: lime; width: 100%; min-height: 15em;">
		@include('controls.dualcurrencyreader', ['input_name'=>'test'])
	</div>
	{{ Form::submit() }}
	{{ Form::close() }}
</div>
@stop

@section('footer')

@stop