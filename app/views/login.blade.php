@extends('layouts.master')

@section('title')
{{ Lang::get('messages.login-message') }}
@stop

@section('content')

	<h1>Login</h1>
	{{ Form::open() }}
	<div>
		{{ Form::label('username', Lang::get('messages.username').': ') }}
		{{ Form::text('username') }}
	</div>
	<div>
		{{ Form::label('password', Lang::get('messages.password').': ') }}
		{{ Form::password('password') }}
	</div>
	<div>
		{{ Form::submit('Login') }}
	</div>
	{{ Form::close() }}
	<div>
		@if(isset($message))
			{{$message}}<br>
		@endif
	</div>

@stop