
@extends('layouts.master')

@section('title') 
{{ htmlentities(Lang::get('messages.system-transactions')) }} {{ Lang::get('messages.from') }} {{ $fromDate }} {{ Lang::get('messages.to') }} {{ $toDate }}
@stop 

@section('content')

@if(count($results) > 0)
	
	<table border="1" style="width: 100%;text-align: center;">
	<tr> <th>Fecha/Hora</th> <th>Oficina de cambio</th> <th>Euro</th> <th>Ekhi</th> <th>Descripci&oacute;n</th> </tr>
	@foreach($results as $st)
	<tr> 
	<td>{{ $st->created_at }}</td> 
	<td>{{ Leihatila::getOfficeName($st->office_id) }}</td> 
	<td><?php if ($st->official_money_amount > 0) {echo '+';} echo $st->official_money_amount; ?></td> 
	<td><?php if ($st->local_money_amount > 0) {echo '+';} echo $st->local_money_amount; ?></td> 
	<td>{{ $st->concept or Lang::get('adminview.no-transaction-description') }}</td>
	</tr>
	@endforeach
	</table>
@else
	{{ Lang::get('adminview.no-transactions-to-show') }}
@endif

@stop

