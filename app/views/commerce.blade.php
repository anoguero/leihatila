<?php
// Prepare the variables

$menu_items = array("tab1"=>htmlentities(Lang::get('messages.account-info')),
					"tab2"=>htmlentities(Lang::get('messages.change-password')),
					"tab3"=>htmlentities(Lang::get('messages.funding')),
					"tab4"=>htmlentities(Lang::get('messages.statistics')),
);

$associations = Association::all();

$associations_opt = array();
$associations_opt[0] = '----------------------------';
foreach ($associations as $assoc) {
	$associations_opt[$assoc->id] = $assoc->name;
}

if (isset($commerce->supported_association_id)) {
	$supported_assoc = Association::find($commerce->supported_association_id)->name;
} else {
	$supported_assoc = Lang::get('messages.none');
}

?>

@extends('layouts.master') 

@section('title') 
{{ htmlentities(Lang::get('messages.welcome')) }} {{ Auth::user()->username }}
@stop 

@section('header') 
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js') }}

{{ HTML::script('js/menu.js') }} 
{{ HTML::script('js/partnerview.js') }} 
{{ HTML::script('js/leihatila-common.js') }}

{{ HTML::style('css/leihatila.css') }}
{{ HTML::style('css/tabbedpanel.css') }}
{{ HTML::style('css/controls.css') }}   
{{ HTML::style('css/mainpage.css') }}  
@include('messages')

@stop 

@section('content')


<div id="tabbed_section" class="leihatila">
	<!-- *** MENU DIV ELEMENT *** -->
	@include('controls.tabbedpanel', ['items'=>$menu_items, 'selected'=>$active])
	<!-- *** END OF MENU DIV ELEMENT *** -->
	
	<!-- *** TAB1 VIEW *** -->
	<div id="tab1" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.account-data')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.account-data-explanation')) }}</p>
    	<hr/>
    	{{ Form::open(['id'=>'update_account_form']) }}
    	{{ Form::hidden('action', 'tab1-updateuser-form') }}
		{{ Form::hidden('active', 'tab1') }}
		<table border="" style="width: 100%;border: none;">
		<tr> 
			<td class="form-left">
				{{ Form::label('name', htmlentities(Lang::get('messages.name'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('name', $commerce->name, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('address', htmlentities(Lang::get('messages.address'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('address', $commerce->address, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('description', htmlentities(Lang::get('messages.description'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('description', $commerce->description, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('responsible', htmlentities(Lang::get('messages.responsible-person'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('responsible', $commerce->responsible, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('email', htmlentities(Lang::get('messages.email'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('email', $commerce->email, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('phone', htmlentities(Lang::get('messages.phone'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('phone', $commerce->phone, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('webpage', htmlentities(Lang::get('messages.webpage'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('webpage', $commerce->webpage, ['class' => 'long-text']) }}
			</td> 
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('id_card', htmlentities(Lang::get('messages.id_card'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('id_card', $commerce->id_card, ['class' => 'long-text']) }}
			</td>
		</tr>
		<tr> 
			<td class="form-left">
				{{ Form::label('quota_updated', htmlentities(Lang::get('messages.last-quota-paid'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('quota_updated', $commerce->yearly_quota_paid_until, ['class' => 'long-text', 'readonly']) }}
			</td>  
		</tr>
		</table>
    	{{ Form::close() }}
    	{{ Form::open(['id'=>'delete_account_form']) }}
	    	{{ Form::hidden('action', 'tab1-updateuser-form') }}
			{{ Form::hidden('active', 'tab1') }}
		{{ Form::close() }}
    	<p style="text-align: right;padding-right: 10px;">
			{{ Form::button(Lang::get('messages.update-account'), ['class'=>'update_commerce_button']) }}
			<!-- {{ Form::button(Lang::get('messages.delete-account'), ['class'=>'delete_account_button']) }} -->
		</p>
    </div>
    <!-- *** END OF TAB1 VIEW *** -->
    
    <!-- *** TAB2 VIEW *** -->
    <div id="tab2" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.change-password')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.change-pass-explanation')) }}</p>
    	<hr/>
    	{{ Form::open() }}
    	{{ Form::hidden('action', 'tab2-updatepass-form') }}
		{{ Form::hidden('active', 'tab2') }}
		<table border="" style="width: 100%;border: none;">
			<tr> 
				<td class="form-left">
					{{ Form::label('prev_pass', htmlentities(Lang::get('messages.previous-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('prev_pass', ['class' => 'long-text']) }}
				</td> 
			</tr>
			<tr> 
				<td class="form-left">
					{{ Form::label('new_pass1', htmlentities(Lang::get('messages.new-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('new_pass1', ['class' => 'long-text']) }}
				</td> 
			</tr>
			<tr> 
				<td class="form-left">
					{{ Form::label('new_pass2', htmlentities(Lang::get('messages.repeat-password'))) }}
				</td> 
				<td class="form-right"> 
					{{ Form::password('new_pass2', ['class' => 'long-text']) }}
				</td> 
			</tr>
		</table>
		<p style="text-align: right;padding-right: 10px;">
		{{ Form::button(Lang::get('messages.update-password'), ['class'=>'update_pass_button']) }}
		</p>
		{{ Form::close() }}
    </div>
	<!-- *** END OF TAB2 VIEW *** -->
    
	
	<!-- *** TAB3 VIEW *** -->
    <div id="tab3" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.funding')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.funding-explanation')) }}</p>
    	<hr/>
    	{{ Form::open() }}
    	{{ Form::hidden('action', 'tab3-updateassoc-form') }}
		{{ Form::hidden('active', 'tab3') }}
    	<table border="" style="width: 100%;border: none;">
		<tr> 
			<td class="form-left">
				{{ Form::label('supported_assoc', htmlentities(Lang::get('messages.selected-association'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::text('supported_assoc', $supported_assoc, ['class' => 'long-text', 'readonly'=>'readonly']) }}
			</td> 
		</tr>
		<tr>
			<td class="form-left">
				{{ Form::label('association', htmlentities(Lang::get('messages.view-association'))) }}
			</td> 
			<td class="form-right"> 
				{{ Form::select('association', $associations_opt, 0, ['class' => 'assoc_combo long-text']) }}
			</td> 
		</tr>
		</table>
		<div>
		</div>
		<p style="text-align: right;padding-right: 10px;">
			{{ Form::button(Lang::get('messages.change-assoc'), ['class'=>'update_assoc_button','disabled'=>'disabled']) }}
		</p>
    	{{ Form::close() }}
    </div>
    
	<!-- *** END OF TAB3 VIEW *** -->
	
	<!-- *** TAB4 VIEW *** -->
    <div id="tab4" class="tab_content">
    	<h1>{{ htmlentities(Lang::get('messages.account-statistics')) }}:</h1>
    	<p>{{ htmlentities(Lang::get('messages.coming-soon')) }}</p>
    </div>
	<!-- *** END OF TAB4 VIEW *** -->
	
</div>

@stop

@section('footer')
	<div class="logout_section">
	{{ Form::open(array('action' => 'LoginController@logout', 'method' => 'get')) }}
	{{ Form::submit(Lang::get('adminview.exit')) }}
	{{ Form::close() }}
	</div>
	@if(isset($mustConfirm) && $mustConfirm)
	<script type="text/javascript"> askConfirmationMessage('{{ $message or '' }}') </script>
	@else
	<script type="text/javascript"> showMessage('{{ $message or '' }}') </script>
	@endif
@stop