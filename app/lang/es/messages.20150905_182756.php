<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2015/06/14 20:22:34 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-buy' => 'Está a punto de vender',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-change' => 'Está a punto de cambiar',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-exp' => 'Está a punto de cobrar por la caducidad',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-sell' => 'Está a punto de descambiar',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data' => 'Datos de la Cuenta',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data-explanation' => 'Utilice este formulario para actualizar su información personal.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-info' => 'Información de la cuenta',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-statistics' => 'Estadísticas de la Cuenta',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'accounting' => 'Contabilidad',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'active' => 'Activo',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'address' => 'Dirección',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'and' => 'y',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'are-you-sure' => '¿Está seguro?',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'association' => 'Asociación',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-create-success' => 'La nueva asociación se creó con éxito.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-set-error' => 'Se produjo un error al crear un nuevo usuario para la asociación. Por favor, contacte con el administrador.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-update-success' => 'La asociación se actualizó con éxito.',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'associations' => 'Asociaciones',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'barcode' => 'Código de Barras',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'buy' => 'Comprar',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'buy-local-money' => 'Comprar Moneda Local',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate' => 'Calcular',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate-office-transactions' => 'Calcular transacciones de la oficina',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'change' => 'Cambiar',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-assoc' => 'Cambiar Asociación',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'change-local-money' => 'Cambiar Moneda Local',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-pass-explanation' => 'Por favor, introduzca su contraseña anterior, y la nueva contraseña dos veces para realizar el cambio.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-password' => 'Cambiar Contraseña',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'change-supported-assoc-confirm' => '¿Está seguro de cambiar la asociación a la que está apoyando?',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'coming-soon' => 'Esta zona está en desarrollo',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'commerce' => 'Comercio',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-create-success' => 'El nuevo comercio se ha creado con éxito.',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerce-does-not-exist' => 'El comercio seleccionado no existe',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerce-quotas' => 'Coutas de Comercio',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-set-error' => 'Se produjo un error al crear el nuevo usuario comercio. Por favor, contacte con un administrador.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-update-success' => 'La información del comercio ha sido actualizada con éxito.',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerces' => 'Comercios',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-account-update' => '¿Está seguro de actualizar la información de su cuenta?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-local' => 'Se creará un nuevo usuario con una couta de :value: Ekhis. ¿Quiere continuar?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-official' => 'Ce creará un nuevo usuario con una cuota de :value: Euros. ¿Quiere continuar?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-update' => '¿Está seguro de actualizar la información de esta socia?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-password-update' => 'Se va a proceder a cambiar su contraseña, ¿desea continuar?',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'connected-as' => 'Conectado como',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-association' => 'Crear Asociación',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-commerce' => 'Crear Comercio',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-user' => 'Crear Socio',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'date-time' => 'Fecha y Hora',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'db-save-error' => 'Se produjo un error al almacenar información en la base de datos. Por favor, contacte con un administrador.',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'delete' => 'Borrar',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'delete-account' => 'Borrar Cuenta',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'description' => 'Descripción',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'different-amounts-detected' => 'El valor total de moneda local es diferente del total de moneda oficial',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'different-change-values-error' => 'Los valores de moneda local a cambiar no coinciden',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'different-passwords-error' => 'La dos contraseñas no coinciden',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'draft' => 'Borrador',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'edit' => 'Editar',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'email' => 'Email',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'expirated-note' => 'Billete Caducado',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'expiration' => 'Caducidad',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'expiration-total' => 'Total de Caducidad',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'expired' => 'Caducado',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'for' => 'Por',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'from' => 'Desde',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding' => 'Ekhifunding',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding-explanation' => 'Aquí podrá seleccionar qué asociación recibirá el dinero generado por sus cambios y cuotas de caducidad.',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'funding-status' => 'Estado del Ekhifunding',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'geocoding-failed' => 'Leihatila no pudo localizar en el mapa la dirección introducida',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'given-notes' => 'Billetes entregados',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'granted-budget' => 'Financiación concedida',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'id_card' => 'CIF/NIF',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'initial_quota' => 'Cuota Inicial',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'insert-barcode' => 'Introduzca el código de barras',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'insufficient-quota' => 'La cuota introducida no es suficiente',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-association-id' => 'No se pudo localizar a la asociación seleccionada.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcode' => 'El código de barras introducido :number no es válido',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcodes-detected' => 'La aplicación ha detectado algunos códigos de barras no válidos',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-expected-local-amount' => 'Se produjo un error. La aplicación esperaba recibir :expected Ekhis, pero ha detectado :actual Ekhis',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'invalid-note-barcodes' => 'El grupo de códigos de billete introducidos contiene alguno que no es válido.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-operation' => 'Se detectó un tipo de transacción no válida. Por favor, contacte con un administrador',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-status' => 'El billete de código :number debía estar en :expected pero su estado es :actual',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-user-barcode' => 'El código de usuario introducido no es válido',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'last-quota-paid' => 'Cuota Pagada En',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'leihatila-description' => 'La aplicación de contabilidad para monedas alternativas desarrollada por Ekhi Txanpona',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'local-money' => 'Ekhi',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'login-failed' => 'No se pudo acceder a la aplicación. Por favor, compruebe sus credenciales.',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'logout-success' => 'La desconexión se ha realizado con éxito',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'lower-quota-warning' => 'El usuario se creará con una cuota menor a la esperada.',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-fields-error' => 'Algunos campos no se han completado:',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-new-password-error' => 'No se ha proporcionado la nueva contraseña.',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-prev-password-error' => 'No se ha proporcionado la contraseña antigua.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'name' => 'Nombre',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'new' => 'Nuevo',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'new-password' => 'Nueva Contraseña',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'no-such-project-error' => 'No se pudo localizar el proyecto seleccionado',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'none' => 'Ninguno',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-circulating' => 'El billete no se en encuentra en circulación',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'not-expirated-note' => 'El billete :number no está caducado',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-expired' => 'El billete introducido no está caducado',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-in-system' => 'El billete introducido no está registrado en el sistema',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'not-logged' => 'No estás conectado',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-stored' => 'El billete introducido no está en caja',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'note-barcodes' => 'Códigos de Billetes',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'notes-expiration-date' => 'Los billetes caducarán el :date',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'office-id' => 'ID de la Oficina',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'office-transactions' => 'Transacciones de la Oficina',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'officestatus' => 'Estado de la Oficina',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'official-money' => 'Euro',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'official_money' => 'Euro',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'operation-completed-sucessfully' => 'La operación se realizó con éxito',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'operations' => 'Operaciones',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'partner' => 'Socia',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'partner-create-success' => 'Nueva socia creada con éxito',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'partner-update-success' => 'La información de la persona socia se actualizó con éxito',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'partners' => 'Socias',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'pass-change-confirm' => '¿Está seguro de querer cambiar su contraseña?',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-expiration' => 'Pagar Caducidad',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-quota' => 'Pagar Cuota de Socio',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pending-quotas' => 'Cuotas Pendientes',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'phone' => 'Teléfono',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'phone-or-email' => 'Debes introducir un teléfono o un email',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'photo' => 'Fotografía',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'previous-password' => 'Contraseña Anterior',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'proceed' => 'Proceder',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-create-confirm' => '¿Está seguro de querer crear el proyecto?',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-edit-confirm' => '¿Está seguro de querer guardar los cambios en el proyecto?',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'project-status' => 'Estado del Proyecto',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'projects' => 'Proyectos',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'quota' => 'Cuota',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'quota-confirm' => 'Has seleccionado una cuota de :value: :curr:, ¿quieres continuar?',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'quota-outdated' => 'Su cuota de comercio no está actualizada y la operación se cancelará. Por favor, actualice sus cuotas pendientes antes de realizar operaciones.',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'quota-to-be-paid' => 'Cuota a Pagar',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'ready' => 'Listo',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'received-notes' => 'Billetes Recibidos',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'redeem' => 'Descambio',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'redeem-local-money' => 'Descambiar Moneda Local',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'remove-note' => 'Eliminar Billete',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'repeat-password' => 'Repita la Contraseña',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'repeated-note-barcode' => 'El código de billete introducido está repetido',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'requested-budget' => 'Presupuesto Total',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'responsible-person' => 'Persona Responsable',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'select-operation' => 'Selecciona una Operación',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'selected-association' => 'Asociación Elegida',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'show-details' => 'Mostrar Detalles',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'statistics' => 'Estadísticas',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'status-stored' => 'En Caja',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'submit' => 'Enviar',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'supported-association' => 'Asociación Apoyada',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'supported-association-update-success' => 'Su asociación apoyada se ha actualizado correctamente.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'surname1' => 'Primer Apellido',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'surname2' => 'Segundo Apellido',
  // Defined in file /home/adrian/leihatila2/app/views/transactions.blade.php
  'system-transactions' => 'Transacciones de Sistema',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'to' => 'A',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'total' => 'Total',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-bought' => 'Se han cambiado un total de :amount Ekhi',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-sold' => 'Se han descambiado un total de :amount Ekhi por :official Euro',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transaction-kind' => 'Tipo de Transacción',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transactions-between' => 'Transacciones entre',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'unknown' => 'Desconocido',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-account' => 'Actualizar Cuenta',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-association' => 'Actualizar Asociación',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-commerce' => 'Actualizar Comercio',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-password' => 'Actualizar Contraseña',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-user' => 'Actualizar Socia',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'use-date-format' => 'Utiliza fechas con el formato AAAA-MM-DD',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'user-incomplete-error' => 'La información de usuario introducida no está completa',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'users-between' => 'Comprueba los usuarios creados entre',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'view-association' => 'Ver Asociación',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'view-projects' => 'Ver Proyectos',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'webpage' => 'Página Web',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'welcome-to-leihatila' => 'Bienvenida a Leihatila',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'wrong-password' => 'La contraseña introducida no es correcta',
  //==================================== Translations ====================================//
  'changeconfig' => 'Configuración del Sistema',
  'mailing' => 'Mails a Socias',
  'notallowed' => 'No tiene permisos para ver esta página.',
  'notemanagement' => 'Gestión de Billetes',
  'partnermanagement' => 'Gestión de Socias',
  'systemstatus' => 'Estado del Sistema',
  'systemtransactions' => 'Transacciones de Sistema',
  'welcome' => 'Bienvenido',
  //================================== Obsolete strings ==================================//
  'internalerror' => 'La aplicación ha experimentado un error interno, por favor contacte con un administrador.',
);