<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2015/09/05 18:23:28 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /home/adrian/leihatila2/app/views/login.blade.php
  'login-message' => 'login-message',
  // Defined in file /home/adrian/leihatila2/app/views/login.blade.php
  'password' => 'password',
  // Defined in file /home/adrian/leihatila2/app/views/login.blade.php
  'username' => 'username',
  //==================================== Translations ====================================//
  'about-to-buy' => 'about-to-buy',
  'about-to-change' => 'about-to-change',
  'about-to-exp' => 'about-to-exp',
  'about-to-sell' => 'about-to-sell',
  'account-data' => 'account-data',
  'account-data-explanation' => 'account-data-explanation',
  'account-info' => 'account-info',
  'account-statistics' => 'account-statistics',
  'accounting' => 'accounting',
  'active' => 'active',
  'address' => 'address',
  'and' => 'and',
  'are-you-sure' => 'are-you-sure',
  'association' => 'association',
  'association-create-success' => 'association-create-success',
  'association-set-error' => 'association-set-error',
  'association-update-success' => 'association-update-success',
  'associations' => 'associations',
  'barcode' => 'barcode',
  'buy' => 'buy',
  'buy-local-money' => 'buy-local-money',
  'calculate' => 'calculate',
  'calculate-office-transactions' => 'calculate-office-transactions',
  'change' => 'change',
  'change-assoc' => 'change-assoc',
  'change-local-money' => 'change-local-money',
  'change-pass-explanation' => 'change-pass-explanation',
  'change-password' => 'change-password',
  'change-supported-assoc-confirm' => 'change-supported-assoc-confirm',
  'changeconfig' => 'Sistemaren Konfigurazioa',
  'coming-soon' => 'coming-soon',
  'commerce' => 'commerce',
  'commerce-create-success' => 'commerce-create-success',
  'commerce-does-not-exist' => 'commerce-does-not-exist',
  'commerce-quotas' => 'commerce-quotas',
  'commerce-set-error' => 'commerce-set-error',
  'commerce-update-success' => 'commerce-update-success',
  'commerces' => 'commerces',
  'confirm-account-update' => 'confirm-account-update',
  'confirm-partner-create-local' => 'confirm-partner-create-local',
  'confirm-partner-create-official' => 'confirm-partner-create-official',
  'confirm-partner-update' => 'confirm-partner-update',
  'confirm-password-update' => 'confirm-password-update',
  'connected-as' => 'connected-as',
  'create-association' => 'create-association',
  'create-commerce' => 'create-commerce',
  'create-user' => 'create-user',
  'date-time' => 'date-time',
  'db-save-error' => 'db-save-error',
  'delete' => 'delete',
  'delete-account' => 'delete-account',
  'description' => 'description',
  'different-amounts-detected' => 'different-amounts-detected',
  'different-change-values-error' => 'different-change-values-error',
  'different-passwords-error' => 'different-passwords-error',
  'draft' => 'draft',
  'edit' => 'edit',
  'email' => 'email',
  'expirated-note' => 'expirated-note',
  'expiration' => 'expiration',
  'expiration-total' => 'expiration-total',
  'expired' => 'expired',
  'for' => 'for',
  'from' => 'from',
  'funding' => 'funding',
  'funding-explanation' => 'funding-explanation',
  'funding-status' => 'funding-status',
  'geocoding-failed' => 'geocoding-failed',
  'given-notes' => 'given-notes',
  'granted-budget' => 'granted-budget',
  'id_card' => 'id_card',
  'initial_quota' => 'initial_quota',
  'insert-barcode' => 'insert-barcode',
  'insufficient-quota' => 'insufficient-quota',
  'invalid-association-id' => 'invalid-association-id',
  'invalid-barcode' => 'invalid-barcode',
  'invalid-barcodes-detected' => 'invalid-barcodes-detected',
  'invalid-expected-local-amount' => 'invalid-expected-local-amount',
  'invalid-note-barcodes' => 'invalid-note-barcodes',
  'invalid-operation' => 'invalid-operation',
  'invalid-status' => 'invalid-status',
  'invalid-user-barcode' => 'invalid-user-barcode',
  'last-quota-paid' => 'last-quota-paid',
  'leihatila-description' => 'leihatila-description',
  'local-money' => 'local-money',
  'login-failed' => 'login-failed',
  'logout-success' => 'logout-success',
  'lower-quota-warning' => 'lower-quota-warning',
  'mailing' => 'Kideentzako Emailak',
  'missing-fields-error' => 'missing-fields-error',
  'missing-new-password-error' => 'missing-new-password-error',
  'missing-prev-password-error' => 'missing-prev-password-error',
  'name' => 'name',
  'new' => 'new',
  'new-password' => 'new-password',
  'no-such-project-error' => 'no-such-project-error',
  'none' => 'none',
  'not-circulating' => 'not-circulating',
  'not-expirated-note' => 'not-expirated-note',
  'not-expired' => 'not-expired',
  'not-in-system' => 'not-in-system',
  'not-logged' => 'not-logged',
  'not-stored' => 'not-stored',
  'notallowed' => 'Ez daukazu permisurik orrialde hau ikusteko.',
  'note-barcodes' => 'note-barcodes',
  'notemanagement' => 'Billeteen Kudeaketa',
  'notes-expiration-date' => 'notes-expiration-date',
  'office-id' => 'office-id',
  'office-transactions' => 'office-transactions',
  'officestatus' => 'officestatus',
  'official-money' => 'official-money',
  'operation-completed-sucessfully' => 'operation-completed-sucessfully',
  'operations' => 'operations',
  'partner' => 'partner',
  'partner-create-success' => 'partner-create-success',
  'partner-update-success' => 'partner-update-success',
  'partnermanagement' => 'Kideen Kudeaketa',
  'partners' => 'partners',
  'pass-change-confirm' => 'pass-change-confirm',
  'pay-expiration' => 'pay-expiration',
  'pay-quota' => 'pay-quota',
  'pending-quotas' => 'pending-quotas',
  'phone' => 'phone',
  'phone-or-email' => 'phone-or-email',
  'photo' => 'photo',
  'previous-password' => 'previous-password',
  'proceed' => 'proceed',
  'project-create-confirm' => 'project-create-confirm',
  'project-edit-confirm' => 'project-edit-confirm',
  'project-status' => 'project-status',
  'projects' => 'projects',
  'quota' => 'quota',
  'quota-confirm' => 'quota-confirm',
  'quota-outdated' => 'quota-outdated',
  'quota-to-be-paid' => 'quota-to-be-paid',
  'ready' => 'ready',
  'received-notes' => 'received-notes',
  'redeem' => 'redeem',
  'redeem-local-money' => 'redeem-local-money',
  'remove-note' => 'remove-note',
  'repeat-password' => 'repeat-password',
  'repeated-note-barcode' => 'repeated-note-barcode',
  'requested-budget' => 'requested-budget',
  'responsible-person' => 'responsible-person',
  'select-operation' => 'select-operation',
  'selected-association' => 'selected-association',
  'show-details' => 'show-details',
  'statistics' => 'statistics',
  'status-stored' => 'status-stored',
  'submit' => 'submit',
  'supported-association' => 'supported-association',
  'supported-association-update-success' => 'supported-association-update-success',
  'surname1' => 'surname1',
  'surname2' => 'surname2',
  'system-transactions' => 'system-transactions',
  'systemstatus' => 'Sistemaren Egoera',
  'systemtransactions' => 'Sistema Transakzioak',
  'to' => 'to',
  'total' => 'total',
  'total-local-bought' => 'total-local-bought',
  'total-local-sold' => 'total-local-sold',
  'transaction-kind' => 'transaction-kind',
  'transactions-between' => 'transactions-between',
  'unknown' => 'unknown',
  'update-account' => 'update-account',
  'update-association' => 'update-association',
  'update-commerce' => 'update-commerce',
  'update-password' => 'update-password',
  'update-user' => 'update-user',
  'use-date-format' => 'use-date-format',
  'user-incomplete-error' => 'user-incomplete-error',
  'users-between' => 'users-between',
  'view-association' => 'view-association',
  'view-projects' => 'view-projects',
  'webpage' => 'webpage',
  'welcome' => 'Ongi Etorri',
  'welcome-to-leihatila' => 'welcome-to-leihatila',
  'wrong-password' => 'wrong-password',
  //================================== Obsolete strings ==================================//
  'internalerror' => 'Arazo bat izan da aplikazioarekin. Mesedez jar zaitez harremanean administradorearekin.',
  'official_money' => 'official_money',
);