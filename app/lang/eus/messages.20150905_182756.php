<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2015/06/14 20:22:34 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-buy' => 'about-to-buy',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-change' => 'about-to-change',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-exp' => 'about-to-exp',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-sell' => 'about-to-sell',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data' => 'account-data',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data-explanation' => 'account-data-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-info' => 'account-info',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-statistics' => 'account-statistics',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'accounting' => 'accounting',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'active' => 'active',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'address' => 'address',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'and' => 'and',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'are-you-sure' => 'are-you-sure',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'association' => 'association',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-create-success' => 'association-create-success',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-set-error' => 'association-set-error',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'association-update-success' => 'association-update-success',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'associations' => 'associations',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'barcode' => 'barcode',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'buy' => 'buy',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'buy-local-money' => 'buy-local-money',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate' => 'calculate',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate-office-transactions' => 'calculate-office-transactions',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'change' => 'change',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-assoc' => 'change-assoc',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'change-local-money' => 'change-local-money',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-pass-explanation' => 'change-pass-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-password' => 'change-password',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'change-supported-assoc-confirm' => 'change-supported-assoc-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'coming-soon' => 'coming-soon',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'commerce' => 'commerce',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-create-success' => 'commerce-create-success',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerce-does-not-exist' => 'commerce-does-not-exist',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerce-quotas' => 'commerce-quotas',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-set-error' => 'commerce-set-error',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'commerce-update-success' => 'commerce-update-success',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerces' => 'commerces',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-account-update' => 'confirm-account-update',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-local' => 'confirm-partner-create-local',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-official' => 'confirm-partner-create-official',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-update' => 'confirm-partner-update',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-password-update' => 'confirm-password-update',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'connected-as' => 'connected-as',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-association' => 'create-association',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-commerce' => 'create-commerce',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'create-user' => 'create-user',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'date-time' => 'date-time',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'db-save-error' => 'db-save-error',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'delete' => 'delete',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'delete-account' => 'delete-account',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'description' => 'description',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'different-amounts-detected' => 'different-amounts-detected',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'different-change-values-error' => 'different-change-values-error',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'different-passwords-error' => 'different-passwords-error',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'draft' => 'draft',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'edit' => 'edit',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'email' => 'email',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'expirated-note' => 'expirated-note',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'expiration' => 'expiration',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'expiration-total' => 'expiration-total',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'expired' => 'expired',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'for' => 'for',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'from' => 'from',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding' => 'funding',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding-explanation' => 'funding-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'funding-status' => 'funding-status',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'geocoding-failed' => 'geocoding-failed',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'given-notes' => 'given-notes',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'granted-budget' => 'granted-budget',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'id_card' => 'id_card',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'initial_quota' => 'initial_quota',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'insert-barcode' => 'insert-barcode',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'insufficient-quota' => 'insufficient-quota',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-association-id' => 'invalid-association-id',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcode' => 'invalid-barcode',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcodes-detected' => 'invalid-barcodes-detected',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-expected-local-amount' => 'invalid-expected-local-amount',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'invalid-note-barcodes' => 'invalid-note-barcodes',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-operation' => 'invalid-operation',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-status' => 'invalid-status',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-user-barcode' => 'invalid-user-barcode',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'last-quota-paid' => 'last-quota-paid',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'leihatila-description' => 'leihatila-description',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'local-money' => 'local-money',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'login-failed' => 'login-failed',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'logout-success' => 'logout-success',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'lower-quota-warning' => 'lower-quota-warning',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-fields-error' => 'missing-fields-error',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-new-password-error' => 'missing-new-password-error',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-prev-password-error' => 'missing-prev-password-error',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'name' => 'name',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'new' => 'new',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'new-password' => 'new-password',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'no-such-project-error' => 'no-such-project-error',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'none' => 'none',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-circulating' => 'not-circulating',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'not-expirated-note' => 'not-expirated-note',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-expired' => 'not-expired',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-in-system' => 'not-in-system',
  // Defined in file /home/adrian/leihatila2/app/controllers/LoginController.php
  'not-logged' => 'not-logged',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-stored' => 'not-stored',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'note-barcodes' => 'note-barcodes',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'notes-expiration-date' => 'notes-expiration-date',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'office-id' => 'office-id',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'office-transactions' => 'office-transactions',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'officestatus' => 'officestatus',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'official-money' => 'official-money',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'official_money' => 'official_money',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'operation-completed-sucessfully' => 'operation-completed-sucessfully',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'operations' => 'operations',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'partner' => 'partner',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'partner-create-success' => 'partner-create-success',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'partner-update-success' => 'partner-update-success',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'partners' => 'partners',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'pass-change-confirm' => 'pass-change-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-expiration' => 'pay-expiration',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-quota' => 'pay-quota',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pending-quotas' => 'pending-quotas',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'phone' => 'phone',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'phone-or-email' => 'phone-or-email',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'photo' => 'photo',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'previous-password' => 'previous-password',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'proceed' => 'proceed',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-create-confirm' => 'project-create-confirm',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-edit-confirm' => 'project-edit-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'project-status' => 'project-status',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'projects' => 'projects',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'quota' => 'quota',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'quota-confirm' => 'quota-confirm',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'quota-outdated' => 'quota-outdated',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'quota-to-be-paid' => 'quota-to-be-paid',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'ready' => 'ready',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'received-notes' => 'received-notes',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'redeem' => 'redeem',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'redeem-local-money' => 'redeem-local-money',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'remove-note' => 'remove-note',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'repeat-password' => 'repeat-password',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'repeated-note-barcode' => 'repeated-note-barcode',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'requested-budget' => 'requested-budget',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'responsible-person' => 'responsible-person',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'select-operation' => 'select-operation',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'selected-association' => 'selected-association',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'show-details' => 'show-details',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'statistics' => 'statistics',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'status-stored' => 'status-stored',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'submit' => 'submit',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'supported-association' => 'supported-association',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'supported-association-update-success' => 'supported-association-update-success',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'surname1' => 'surname1',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'surname2' => 'surname2',
  // Defined in file /home/adrian/leihatila2/app/views/transactions.blade.php
  'system-transactions' => 'system-transactions',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'to' => 'to',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'total' => 'total',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-bought' => 'total-local-bought',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-sold' => 'total-local-sold',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transaction-kind' => 'transaction-kind',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transactions-between' => 'transactions-between',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'unknown' => 'unknown',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-account' => 'update-account',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-association' => 'update-association',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-commerce' => 'update-commerce',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-password' => 'update-password',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'update-user' => 'update-user',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'use-date-format' => 'use-date-format',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'user-incomplete-error' => 'user-incomplete-error',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'users-between' => 'users-between',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'view-association' => 'view-association',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'view-projects' => 'view-projects',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'webpage' => 'webpage',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'welcome-to-leihatila' => 'welcome-to-leihatila',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'wrong-password' => 'wrong-password',
  //==================================== Translations ====================================//
  'changeconfig' => 'Sistemaren Konfigurazioa',
  'mailing' => 'Kideentzako Emailak',
  'notallowed' => 'Ez daukazu permisurik orrialde hau ikusteko.',
  'notemanagement' => 'Billeteen Kudeaketa',
  'partnermanagement' => 'Kideen Kudeaketa',
  'systemstatus' => 'Sistemaren Egoera',
  'systemtransactions' => 'Sistema Transakzioak',
  'welcome' => 'Ongi Etorri',
  //================================== Obsolete strings ==================================//
  'internalerror' => 'Arazo bat izan da aplikazioarekin. Mesedez jar zaitez harremanean administradorearekin.',
);