<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2015/06/14 20:22:34 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'and' => 'and',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'associations-only' => 'associations-only',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'backing' => 'backing',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'calculate' => 'calculate',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'central-local' => 'central-local',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'central-official' => 'central-official',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'change-config' => 'change-config',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'circulating' => 'circulating',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'commerces-only' => 'commerces-only',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'config-change-confirm' => 'config-change-confirm',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'config-change-failed' => 'config-change-failed',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'config-change-success' => 'config-change-success',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'config-change-warning' => 'config-change-warning',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'create-notes' => 'create-notes',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'create-notes-confirm' => 'create-notes-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'create-notes-range-confirm' => 'create-notes-range-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'create-transaction' => 'create-transaction',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'create-transaction-confirm' => 'create-transaction-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'description' => 'description',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'details' => 'details',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'email-to-explanation' => 'email-to-explanation',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'empty-transaction-error' => 'empty-transaction-error',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'erroneous-barcodes' => 'erroneous-barcodes',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'exchange-rate' => 'exchange-rate',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'exit' => 'exit',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'expiration-day' => 'expiration-day',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'expiration-months' => 'expiration-months',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'expiration-rate' => 'expiration-rate',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'find-users-explanation' => 'find-users-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'first-code' => 'first-code',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'free-days' => 'free-days',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'from' => 'from',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'funding' => 'funding',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'invalid-configuration' => 'invalid-configuration',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'invalid-note-barcode' => 'invalid-note-barcode',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'invalid-transaction-error' => 'invalid-transaction-error',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'last-code' => 'last-code',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'local-money' => 'local-money',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'money-create-success' => 'money-create-success',
  // Defined in file /home/adrian/leihatila2/app/views/transactions.blade.php
  'no-transaction-description' => 'no-transaction-description',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'no-transaction-warning' => 'no-transaction-warning',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'no-transactions-to-show' => 'no-transactions-to-show',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'nodescription' => 'nodescription',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'none' => 'none',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'office-find-error' => 'office-find-error',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'office-update-error' => 'office-update-error',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'official-money' => 'official-money',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'partners-only' => 'partners-only',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'quotas' => 'quotas',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'range-explanation' => 'range-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'range-start-before-end' => 'range-start-before-end',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'refresh' => 'refresh',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'removed-duplicates-warning' => 'removed-duplicates-warning',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'scan-explanation' => 'scan-explanation',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'select-office' => 'select-office',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'send' => 'send',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'send-email-confirm' => 'send-email-confirm',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'show-transactions-between' => 'show-transactions-between',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'status-update-error' => 'status-update-error',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'system-config' => 'system-config',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'system-status' => 'system-status',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'to' => 'to',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'topic' => 'topic',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'total-local' => 'total-local',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'total-official' => 'total-official',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'transaction-create-success' => 'transaction-create-success',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'transaction-creation-error' => 'transaction-creation-error',
  // Defined in file /home/adrian/leihatila2/app/controllers/AdminController.php
  'transaction-description' => 'transaction-description',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'use-date-format' => 'use-date-format',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'warning' => 'warning',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'warning-message' => 'warning-message',
);