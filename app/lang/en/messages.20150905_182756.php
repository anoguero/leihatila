<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2015/06/14 20:22:34 
*************************************************************************/

return array (
  //============================== New strings to translate ==============================//
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-buy' => 'You are about to buy',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-change' => 'You are about to change',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-exp' => 'You are about to pay for the expiration',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'about-to-sell' => 'You are about to exchange',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data' => 'Account Data',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-data-explanation' => 'With the following form you may change your personal information.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-info' => 'Account Information',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'account-statistics' => 'Account Statistics',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'active' => 'Active',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'and' => 'and',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'are-you-sure' => 'Are you sure?',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'association' => 'Association',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'barcode' => 'Barcode',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'buy' => 'Buy',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'buy-local-money' => 'Buy Local Money',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate' => 'Calculate',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'calculate-office-transactions' => 'Calculate office transactions',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'change' => 'Change',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-assoc' => 'Change Association',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'change-local-money' => 'Change Local Money',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-pass-explanation' => 'Please enter your previous password and the new password you want to use two times.',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'change-password' => 'Change Password',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'change-supported-assoc-confirm' => 'Are you sure you want to change your supported association?',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'coming-soon' => 'Coming Soon',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'commerce' => 'Commerce',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'commerce-does-not-exist' => 'No commerce exists with the given barcode',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-account-update' => 'Are you sure you want to update your account information?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-local' => 'The new partner will be created. The selected quota is :value: Ekhi. Do you want to continue?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-create-official' => 'The new partner will be created. The selected quota is :value: Euro. Do you want to continue?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-partner-update' => 'Are you sure you want to update the information of this partner?',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'confirm-password-update' => 'Are you sure to change your password?',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'date-time' => 'Date-Time',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'db-save-error' => 'An error occurred while storing information in the database. Please, contact an administrator.',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'delete' => 'Delete',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'delete-account' => 'Delete Account',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'different-amounts-detected' => 'Total local money and official money amounts are different',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'different-change-values-error' => 'Changed local money amounts are different',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'different-passwords-error' => 'Provided two passwords are different',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'draft' => 'Draft',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'edit' => 'Edit',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'expirated-note' => 'Expirated Note',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'expiration' => 'Expiration',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'expiration-total' => 'Expiration Total',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'expired' => 'Expired',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'for' => 'For',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'from' => 'From',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding' => 'Ekhifunding',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'funding-explanation' => 'Here you can define what association will receive the money associated to your exchange and expiration quotas.',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'funding-status' => 'Ekhifunding Status',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'geocoding-failed' => 'Leihatila failed to place the provided address in the map',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'given-notes' => 'Given Notes',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'granted-budget' => 'Granted Budget',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'insufficient-quota' => 'Given quota is not sufficient',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-association-id' => 'Could not find the supported association',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcode' => 'Provided barcode :number is not valid',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-barcodes-detected' => 'The application detected some invalid barcodes',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-expected-local-amount' => 'An error ocurred. The application expected to receive :expected Ekhi, but detected :actual Ekhi',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-operation' => 'Leihatila detected an invalid transaction operation. Please, contact an administrator.',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-status' => 'Note with barcode :number was expected to be :expected however it is :actual',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'invalid-user-barcode' => 'Provided user barcode is not valid',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'last-quota-paid' => 'Last Quota Paid',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'lower-quota-warning' => 'The user will be created with a quota lower than expected.',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'mailing' => 'Mailing',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-fields-error' => 'Some required fields are missing:',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-new-password-error' => 'New password was not provided',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'missing-prev-password-error' => 'Previous password was not provided',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'new' => 'New',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'new-password' => 'New Password',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'no-such-project-error' => 'Failed to locate the selected project',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'none' => 'None',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-circulating' => 'Provided note is not circulating',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'not-expirated-note' => 'Provided note :number is not expired',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-expired' => 'Provided note is not expired',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-in-system' => 'Provided note is not in the system',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'not-stored' => 'Provided note is not stored',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'note-barcodes' => 'Note Barcodes',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'notes-expiration-date' => 'Provided notes will expire on :date',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'office-id' => 'Office Identifier',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'office-transactions' => 'Office Transactions',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'operation-completed-sucessfully' => 'Operation completed sucessfully',
  // Defined in file /home/adrian/leihatila2/app/views/admin.blade.php
  'partner' => 'Partner',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'pass-change-confirm' => 'Are you sure you want to update your password?',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-expiration' => 'Pay Expiration',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pay-quota' => 'Pay Quota',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'pending-quotas' => 'Pending Quotas',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'phone-or-email' => 'You must provide a phone or email ',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'photo' => 'Photo',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'previous-password' => 'Previous Password',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'proceed' => 'Proceed',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-create-confirm' => 'Are you sure you want to create the new project?',
  // Defined in file /home/adrian/leihatila2/app/controllers/AssociationController.php
  'project-edit-confirm' => 'Are you sure you want to save the changes of the current project?',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'project-status' => 'Project Status',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'projects' => 'Projects',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'quota' => 'Quota',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'quota-confirm' => 'You have selected a quota of :value: :curr:, do you want to continue?',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'quota-outdated' => 'Your commerce quota is not up to date and the operation will be cancelled. Please update your quota to perform the operation.',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'quota-to-be-paid' => 'Quota to be paid',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'ready' => 'Ready',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'received-notes' => 'Received Notes',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'redeem' => 'Redeem',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'redeem-local-money' => 'Redeem Local Money',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'remove-note' => 'Remove Note',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'repeat-password' => 'Repeat Password',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'repeated-note-barcode' => 'Provided note barcode is repeated',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'requested-budget' => 'Requested Budget',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'select-operation' => 'Select an operation',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'selected-association' => 'Selected Association',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'show-details' => 'Show Details',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'statistics' => 'Statistics',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'status-stored' => 'Stored',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'supported-association' => 'Supported Association',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'supported-association-update-success' => 'Your supported association has been changed sucessfully',
  // Defined in file /home/adrian/leihatila2/app/views/transactions.blade.php
  'system-transactions' => 'System Transactions',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'to' => 'to',
  // Defined in file /home/adrian/leihatila2/app/views/controls/notebarcode.blade.php
  'total' => 'Total',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-bought' => 'A total of :amount Ekhi have been bought',
  // Defined in file /home/adrian/leihatila2/app/controllers/OfficeController.php
  'total-local-sold' => 'A total of :amount Ekhi have been redeemed by :official Euro',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transaction-kind' => 'Transaction Kind',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'transactions-between' => 'Transactions between',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'unknown' => 'Unknown',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-account' => 'Update Account',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'update-password' => 'Update Password',
  // Defined in file /home/adrian/leihatila2/app/views/office.blade.php
  'use-date-format' => 'Use dates with the format YYYY-MM-DD',
  // Defined in file /home/adrian/leihatila2/app/views/messages.blade.php
  'user-incomplete-error' => 'Provided user information is not complete',
  // Defined in file /home/adrian/leihatila2/app/views/notetransactions.blade.php
  'users-between' => 'Check the user created between',
  // Defined in file /home/adrian/leihatila2/app/views/partner.blade.php
  'view-association' => 'View Association',
  // Defined in file /home/adrian/leihatila2/app/views/association.blade.php
  'view-projects' => 'View Projects',
  // Defined in file /home/adrian/leihatila2/app/controllers/UserController.php
  'wrong-password' => 'Provided password in not correct',
  //==================================== Translations ====================================//
  'accounting' => 'Accounting',
  'address' => 'Address',
  'association-create-success' => 'New association user created successfully.',
  'association-set-error' => 'Failed to create a new association user. Please contact an administrator.',
  'association-update-success' => 'Association user updated successfully.',
  'associations' => 'Associations',
  'changeconfig' => 'System Configuration',
  'commerce-create-success' => 'New commerce user created successfully.',
  'commerce-quotas' => 'Commerce Quotas',
  'commerce-set-error' => 'Failed to create a new commerce user. Please contact an administrator.',
  'commerce-update-success' => 'Commerce user updated successfully.',
  'commerces' => 'Commerces',
  'connected-as' => 'Connected as',
  'create-association' => 'Create Association',
  'create-commerce' => 'Create Commerce',
  'create-user' => 'Create User',
  'description' => 'Description',
  'email' => 'E-mail',
  'id_card' => 'ID Number',
  'initial_quota' => 'Initial Quota',
  'insert-barcode' => 'Insert Barcode',
  'invalid-note-barcodes' => 'The provided note barcodes set contains non-valid barcodes.',
  'leihatila-description' => 'The accounting application for local currencies developed by Ekhi Txanpona',
  'local-money' => 'Ekhi',
  'login-failed' => 'Failed to log in. Please check your credentials',
  'logout-success' => 'Sucessfully logged out',
  'name' => 'Name',
  'not-logged' => 'You are not logged in',
  'notallowed' => 'You are not allowed to see this page.',
  'notemanagement' => 'Note Management',
  'officestatus' => 'Office Status',
  'official-money' => 'Euro',
  'operations' => 'Operations',
  'partner-create-success' => 'New partner user created successfully.',
  'partner-update-success' => 'Partner user updated successfully.',
  'partnermanagement' => 'Partner Management',
  'partners' => 'Partners',
  'phone' => 'Phone',
  'responsible-person' => 'Responsible Person',
  'submit' => 'Submit',
  'surname1' => 'Surname 1',
  'surname2' => 'Surname 2',
  'systemstatus' => 'System Status',
  'systemtransactions' => 'System Transactions',
  'update-association' => 'Update Association',
  'update-commerce' => 'Update Commerce',
  'update-user' => 'Update User',
  'webpage' => 'Webpage',
  'welcome' => 'Welcome',
  'welcome-to-leihatila' => 'Welcome to Leihatila',
  //================================== Obsolete strings ==================================//
  'initial-quota-confirm' => 'The selected initial quota is lower than the recommended one. Are you sure you want to continue?',
  'internalerror' => 'An internal error of the application occurred. Please contact an administrator.',
  'no-description-error' => 'You must define a description',
  'no-name-error' => 'You must define a name',
  'no-phone-email-error' => 'You must define at least a contact method: phone and/or email',
  'no-responsible-error' => 'You must define a responsible person',
  'no-surname-error' => 'You must define a surname',
  'partner-set-error' => 'Failed to create a new partner user. Please contact an administrator.',
);